package why.java.demo.jvm.reflect;

import com.sun.deploy.util.ReflectionUtil;
import sun.reflect.misc.ReflectUtil;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.Arrays;

/*
    关于Proxy动态代理的实现原理

 */
public class ReflectTest2 {

    public static void main(String[] args) {

        //控制参数，将代理生成的类保存到本地磁盘
        System.getProperties().put("sun.misc.ProxyGenerator.saveGeneratedFiles", "true");

        //定义 InvocationHandler，handler中是对实际要执行的逻辑的包装
        //对于 Proxy.newProxyInstance 方法所传入的interfaces 参数，要保证 InvocationHandler 中实际执行代码的target也要实现相同的接口，否则就无法调用
        Object target = new Class2_1();
        InvocationHandler2_1 h = new InvocationHandler2_1(target);

        //这里的需要传入一个类加载器，类加载器要保证能访问加载接口类的类加载的命名空间中的类
        ClassLoader loader = ReflectTest2.class.getClassLoader();
        //如果被代理类实现了多个接口，接口中存在同名且参数列表相同的方法，生成的代理对象只会实现第一个接口的方法，
        //如果方法的返回值不同，在java中，被代理类是不能同时实现这样的接口的，编译通不过（方法不能重载）
        //如果方法的返回值相同，被代理对象如果也实现了这些接口，只能保留一个同名且参数列表相同的方法，这样就不存在问题
        //如果代理对象实现了被代理类没有实现的接口，这时可能出现代理类有这个方法，但被代理类没有该方法，在方法调用时可能会发生异常
        //


        Iface2_3 proxyObj = (Iface2_3) Proxy.newProxyInstance(loader, new Class[]{Iface2_2.class, Iface2_1.class, Iface2_3.class}, h);
        h.setProxy(proxyObj);
        //这里创建生成的对象类型和 Class2_1 没有任何关系，它只是 所传接口的子类，和 Class2_1实现了相同的接口
        // Iface2_1 有一个构造函数，参数是 InvocationHandler
        System.out.println(proxyObj.getClass());
        Constructor<?>[] cc = proxyObj.getClass().getConstructors();
        for (Constructor<?> c : cc) {
            System.out.println(Arrays.toString(c.getParameterTypes()));
        }
        proxyObj.test2();
    }
}

class InvocationHandler2_1 implements InvocationHandler {
    //执行的目标类
    private final Object target;
    private Object proxy;
    public InvocationHandler2_1(Object target) {
        this.target = target;
    }
    public void setProxy(Object proxy) {
        this.proxy = proxy;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println(method.getDeclaringClass());
        Method m = Iface2_2.class.getDeclaredMethod("test");
        System.out.println(m == method);
        System.out.println(proxy == this.proxy);
        System.out.println("---before---");
        Object ret = method.invoke(target, args);
        System.out.println("---after---");
        return ret;
    }
}

class Class2_1 implements Iface2_1, Iface2_2 {
    @Override
    public void test() {
        System.out.println("test invoke");
    }
}

interface Iface2_1 {
    void test();
    default void test(String s) {
        System.out.println(s);
    }
}

interface Iface2_2 {
    void test();
}

interface Iface2_3 {
    default String test2() {
        return "Iface2_3 test";
    }
}

/*
    这个类由 ProxyGenerator 类生成的动态代理类
 */
final class $Proxy0 extends Proxy implements Iface2_1 {
    private static Method m1;
    private static Method m3;
    private static Method m2;
    private static Method m0;
    //构造函数，接受一个 InvocationHandler
    public $Proxy0(InvocationHandler var1) {
        super(var1);
    }
    @Override
    public final boolean equals(Object var1)  {
        try {
            return (Boolean)super.h.invoke(this, m1, new Object[]{var1});
        } catch (RuntimeException | Error var3) {
            throw var3;
        } catch (Throwable var4) {
            throw new UndeclaredThrowableException(var4);
        }
    }

    /*
     对接口方法的实现，实际调用的是 InvocationHandler 的 invoke方法
     */
    @Override
    public final void test() {
        try {
            super.h.invoke(this, m3, (Object[])null);
        } catch (RuntimeException | Error var2) {
            throw var2;
        } catch (Throwable var3) {
            throw new UndeclaredThrowableException(var3);
        }
    }
    @Override
    public final String toString() {
        try {
            return (String)super.h.invoke(this, m2, (Object[])null);
        } catch (RuntimeException | Error var2) {
            throw var2;
        } catch (Throwable var3) {
            throw new UndeclaredThrowableException(var3);
        }
    }
    @Override
    public final int hashCode() {
        try {
            return (Integer)super.h.invoke(this, m0, (Object[])null);
        } catch (RuntimeException | Error var2) {
            throw var2;
        } catch (Throwable var3) {
            throw new UndeclaredThrowableException(var3);
        }
    }

    static {
        try {
            //这里会得到所有需要实现的方法，包括接口的方法和 Object类的equals、toString和hashCode方法
            //接口的方法是工具 interfaces参数决定的，包括default方法，也会有对应的实现
            m1 = Class.forName("java.lang.Object").getMethod("equals", Class.forName("java.lang.Object"));
            m3 = Class.forName("why.java.demo.jvm.reflect.Iface2_1").getMethod("test");
            m2 = Class.forName("java.lang.Object").getMethod("toString");
            m0 = Class.forName("java.lang.Object").getMethod("hashCode");
        } catch (NoSuchMethodException var2) {
            throw new NoSuchMethodError(var2.getMessage());
        } catch (ClassNotFoundException var3) {
            throw new NoClassDefFoundError(var3.getMessage());
        }
    }
}
