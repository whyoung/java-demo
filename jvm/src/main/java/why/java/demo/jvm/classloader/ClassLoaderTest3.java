package why.java.demo.jvm.classloader;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ClassLoaderTest3 {

    public static void main(String[] args) throws Exception {

        String path = args[0];
        FilePathClassLoader loader = new FilePathClassLoader( path);

        System.out.println(Class.forName("why.java.demo.jvm.classloader.Class3_3", true, loader));
        //FilePathClassLoader 类加载器的 父类加载器是 sun.misc.Launcher$AppClassLoader
//        System.out.println(loader.getParent());

        //FilePathClassLoader 加载器并没有破坏委托模型，还是会先委托给父类加载器加载，如果父类加载器加载不了，才会使用当前类加载器加载
        //默认情况下，编译之后，类路径下是会存在 Class3_1.class 文件
        //AppClassLoader 会从classpath下加载类，如果能够加载，就直接返回类的Class对象，否则才是 loader 加载
        //如果要测试自定义的类加载器，需要把类路径下对应的类删除
        Class<?> clazz = Class.forName("why.java.demo.jvm.classloader.Class3_1", true, loader);


        System.out.println(clazz.getClassLoader());
//        System.out.println(clazz.getDeclaredField("c").get(clazz));

//        ClassLoader loader2 = new NonDelegateFilePathClassLoader("/Users/whyoung/Desktop");
//        Class<?> clazz2 = Class.forName("why.java.demo.jvm.classloader.Class3_1", false, loader2);
//        System.out.println(clazz2.getClassLoader());

        System.out.println(getClassFromLoadedCache(loader, "why.java.demo.jvm.classloader.Class3_2") != null);

        //只有被loadClass加载过的类才会放到缓存中
        System.out.println(getClassFromLoadedCache(loader, "java.lang.Object") != null);

        System.out.println(getClassFromLoadedCache(loader, "java.lang.String") != null);

        //loader.loadClass("why.java.demo.jvm.classloader.Class3_2", true);
        //Class.forName("why.java.demo.jvm.classloader.Class3_2", true, loader);

        System.out.println(getClassFromLoadedCache(loader.getParent(), "java.lang.Object") != null);
        ClassLoader appClassLoader = loader.getParent();
        ClassLoader extClassLoader = appClassLoader.getParent();
        System.out.println(getClassFromLoadedCache(appClassLoader, "java.lang.Object") != null);
        System.out.println(getClassFromLoadedCache(extClassLoader, "java.lang.Object") != null);
    }

    static Class<?> getClassFromLoadedCache(ClassLoader loader, String name) throws Exception {
        if (loader == null || name == null) {
            return null;
        }
        Method m = ClassLoader.class.getDeclaredMethod("findLoadedClass", String.class);
        m.setAccessible(true);
        return (Class<?>) m.invoke(loader, name);
    }
}

class Class3_1 extends Class3_2 implements Iface3_1 {
    public static int x = 1;

    public static Class3_3 c = new Class3_3();
    @Override
    public boolean equals(Object o) {
        return o instanceof Class3_1;
    }
}

class Class3_2 {
}

interface Iface3_1 {

}

class Class3_3 {

}