package why.java.demo.jvm.classloader;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

 /**
 * 自定义类加载器，不遵循委托模型
 * 因为破坏了委托模型，所以类加载器的
 */
public class NonDelegateFilePathClassLoader extends FilePathClassLoader {

    public NonDelegateFilePathClassLoader(String classFileLocation) {
        super(classFileLocation);
    }

    public NonDelegateFilePathClassLoader(ClassLoader parent) {
        super(parent);
    }

    public NonDelegateFilePathClassLoader(ClassLoader parent, String classFileLocation) {
        super(parent, classFileLocation);
    }

    @Override
    protected synchronized Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
        System.out.println("loading class " + name + " by " + this.getClass().getSimpleName());
        Class<?> c = findLoadedClass(name);
        if (c != null) {
            System.out.println("class " + name + " already loaded");
        } else {
            try {
                Method m = ClassLoader.class.getDeclaredMethod("findBootstrapClassOrNull", String.class);
                m.setAccessible(true);
                c = (Class<?>) m.invoke(this, name);
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException ignore) {
            }
            if (c == null) {
                c = findClass(name);
                System.out.println("loaded class " + name + " by " + this.getClass().getSimpleName());
            }
        }
        if (resolve) {
            resolveClass(c);
        }
        return c;
    }

    @Override
    public String toString() {
        return "NonDelegateFilePathClassLoader#" + getClassFileLocation();
    }
}