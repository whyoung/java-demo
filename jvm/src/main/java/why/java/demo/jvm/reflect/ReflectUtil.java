package why.java.demo.jvm.reflect;

public class ReflectUtil {

    public static String getCaller() {
        try {
            StackTraceElement e = getCallerStackTrace();
            return e.getClassName() + "." + e.getMethodName();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public static Class<?> getCallerClass() {
        try {
            StackTraceElement e = getCallerStackTrace();
            return Class.forName(e.getClassName());
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public static ClassLoader getCallerClassLoader() {
        try {
            StackTraceElement e = getCallerStackTrace();
            return Class.forName(e.getClassName()).getClassLoader();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    private static StackTraceElement getCallerStackTrace() {
        StackTraceElement[] trace = Thread.currentThread().getStackTrace();
        return trace[trace.length - 1];
    }
}
