package why.java.demo.jvm.bytecode;


public class ConstantPoolTest2 {

    private static String x = "hello world";
    private static String y = "hello world";
    public static void main(String[] args) {
        //true
        System.out.println(x == Class2_1.x);
        //true
        System.out.println(x == y);
    }
}

class Class2_1 {
    static String x = "hello world";
}
