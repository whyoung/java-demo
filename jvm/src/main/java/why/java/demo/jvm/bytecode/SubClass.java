package why.java.demo.jvm.bytecode;

public class SubClass extends SuperClass implements Iface {

    static String x;
    static {
        x = "x";
    }

    private final RefClass ref;

    public SubClass() {
        super(0);
        this.ref = new RefClass();
    }

    @Override
    public int m() {
        return super.m();
    }

    @Override
    public int m(String s) {
        return 0;
    }

    @Override
    public int m1(String s) throws ExClass1 {
        if (s == null) {
            throw new ExClass1();
        }
        return Integer.parseInt(s);
    }

    @Override
    public int m2(String s) throws ExClass1, ExClass2 {
        if (s == null) {
            throw new ExClass1();
        }
        try {
            return Integer.parseInt(s);
        } catch (Exception e) {
            throw new ExClass2();
        }
    }

    public static void main(String[] args) {
        SubClass sub = new SubClass();
        System.out.println(sub.m2("22s2"));
    }
}

class ExClass1 extends RuntimeException {

}

class ExClass2 extends RuntimeException {

}

interface Iface {

    default int m() {
        return 0;
    }

    int m(String s);

    int m1(String s) throws ExClass1;

    int m2(String s) throws ExClass1, ExClass2;
}

class RefClass {
}

class SuperClass {

    static int x = 10;

    int y;

    public SuperClass() {
        this(0);
    }

    public SuperClass(int y) {
        this.y = y;
    }

    int m() {
        return x;
    }
}