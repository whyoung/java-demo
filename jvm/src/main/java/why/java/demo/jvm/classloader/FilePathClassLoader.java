package why.java.demo.jvm.classloader;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * 自定义类加载器，从类路径
 * 保持委托加载模型
 */
public class FilePathClassLoader extends ClassLoader {

    /**
     * 存在class文件的路径
     */
    private String classFileLocation;

    public FilePathClassLoader(String classFileLocation) {
        super();
        this.classFileLocation = classFileLocation;
    }

    public FilePathClassLoader(ClassLoader parent) {
        super(parent);
        this.classFileLocation = ".";
    }

    public FilePathClassLoader(ClassLoader parent, String classFileLocation) {
        super(parent);
        this.classFileLocation = classFileLocation;
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        if (name == null) {
            throw new IllegalArgumentException();
        }
        String classFilePath = classFileLocation + "/" + name.replace(".", "/") + ".class";
        File f = new File(classFilePath);
        if (!f.exists() || !f.isFile() || !f.canRead()) {
            System.out.println("can't load class " + name + " by " + this.getClass().getSimpleName());
            throw new ClassNotFoundException();
        }
        try (FileInputStream in = new FileInputStream(f); ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            byte[] buf = new byte[1024];
            int len;
            while((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            return defineClass(name, out.toByteArray(), 0, out.size());
        } catch (IOException e) {
            System.out.println("can't load class " + name + " by " + this.getClass().getSimpleName());
            throw new ClassNotFoundException();
        }
    }

    @Override
    protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
        System.out.println("loading class " + name + " by " + this.getClass().getSimpleName());
        //这个方法在 loadClass 也调用了，这里为了测试能否使用已加载的类
        Class<?> c = findLoadedClass(name);
        if (c != null) {
            System.out.println("class " + name + " already loaded");
            if (resolve) {
                resolveClass(c);
            }
            return c;
        }
        return super.loadClass(name, resolve);
    }

    String getClassFileLocation() {
        return classFileLocation;
    }

    public void setClassFileLocation(String classFileLocation) {
        this.classFileLocation = classFileLocation;
    }

    @Override
    public String toString() {
        return "MyClassLoader#" + classFileLocation;
    }
}

