package why.java.demo.jvm.gc;

public class ObjectMemoryLayout {

    public static void main(String[] args) {
        System.gc();

        /*
        young generation 0K
        6471

         */
        MyObj obj1 = new MyObj();
        MyObj obj2 = new MyObj();
        MyObj obj3 = new MyObj();
        MyObj obj4 = new MyObj();
        MyObj obj5 = new MyObj();
        MyObj obj6 = new MyObj();
        System.gc();

    }
}

class MyObj {
    public byte[] buf;

    public MyObj() {
        this.buf = new byte[1024 * 1024];
    }
}
