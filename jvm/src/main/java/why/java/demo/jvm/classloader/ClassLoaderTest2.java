package why.java.demo.jvm.classloader;

import java.lang.reflect.Field;

/*
    测试 NoClassDefFoundError 和 ClassNotFoundException
    正常情况下，编译代码之后，路路径下有 Class2_1.class 和 Class2_2.class 文件，执行程序，catch中的代码不会被执行
    删除类路径下的 Class2_1.class 和 Class2_2.class 文件
 */
public class ClassLoaderTest2 {

    public static void main(String[] args) throws Exception {
        /*
        直接引用类Class2_1
         */
        try {
            System.out.println(Class2_1.x);
        } catch (NoClassDefFoundError err) {
            //类Class2_1 找不到，会抛出 NoClassDefFoundError 错误
            /*
            NoClassDefFoundError 继承自 LinkageError 错误，而 LinkageError 继承自 Error
            java虚拟机在运行时，依赖 Class2_1 类，如果类加载器没有成功加载 Class2_1 类，会抛出这个错误，
            错误的cause：Caused by: java.lang.ClassNotFoundException: why.java.demo.jvm.classloader.Class2_1
            可以看见造成错误的原因是抛出了 ClassNotFoundException 异常
            java 虚拟机规范中定义：
            java虚拟机的实现允许类加载器在预料某个类将要被使用时就预先加载它，如果在预先加载的过程中遇到了.class文件缺失或者存在错误，
            类加载器必须在程序主动使用该类时才报告错误（LinkageError错误）。
            如果这个类一直没有被程序主动使用，那么类加载器就不会报告错误（也就是不会抛出错误）。
            这种情况要求被依赖的类在当前类编译期一定存在
             */
            err.printStackTrace();
        }

        /*
        通过Class.forName 加载类Class2_2
         */
        try {
            Class<?> clazz = Class.forName("why.java.demo.jvm.classloader.Class2_2", false, ClassLoaderTest2.class.getClassLoader());
            Field f = clazz.getDeclaredField("x");
            f.setAccessible(true);
            System.out.println(f.get(clazz));
        } catch (ClassNotFoundException ex) {
            //类Class2_2 找不到，会抛出 ClassNotFoundException 异常
            /*
            ClassNotFoundException 继承自 ReflectiveOperationException，而 ReflectiveOperationException 继承自 Exception
            当应用程序尝试通过两个字符串类名加载类时
            1.Class类的forName方法
            2.ClassLoader的findSystemClass方法和loadClass方法
            这种情况不一定要求要查找的类在编译期一定存在，可能是通过类的二进制名称字符串查找类
             */
            ex.printStackTrace();
        }

    }
}

class Class2_1 {
    public static int x = 1;
    static {
        System.out.println("Class2_1 static invoked");
    }
}

class Class2_2 {
    public static int x = 2;
    static {
        System.out.println("Class2_2 static invoked");
    }
}

class Class2_3 {
    public static final int x = 1;
    static {
        System.out.println("Class2_3 static invoked");
    }
}