package why.java.demo.jvm.sync;

public class VolatileTest {

    private volatile static boolean flag = true;
    public static void main(String[] args) {
        new Thread(() -> {
            try {
                Thread.sleep(20);
            } catch (InterruptedException ignore) {
            }
            flag = false;
        }).start();

        while (flag) {
            System.out.println("run...");
        }
    }
}
