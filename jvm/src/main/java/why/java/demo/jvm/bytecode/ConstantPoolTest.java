package why.java.demo.jvm.bytecode;

import java.lang.reflect.Field;

public class ConstantPoolTest {

    public static void main(String[] args) throws Exception {
        //args[0] = "why.java.demo.jvm.bytecode.Class1_1"
        String x = args[0];
        internByClassloader(x);
//        internByManual(x);
    }

    /*
    手动intern
     */
    private static void internByManual(String x) throws Exception {
        //先手动intern
        String s = x.intern();
        //true，第一次加载，将对象放到 字符串常量池中
        System.out.println(s == x);
        Class<?> clazz = Class.forName(x);
        Field field = clazz.getDeclaredField("x");
        //true，对于于常量池中的同一个对象
        System.out.println(field.get(clazz) == x);
    }

    /*
    类加载器intern
     */
    private static void internByClassloader(String x) throws Exception {
        Class<?> clazz = Class.forName(x);
        Field field = clazz.getDeclaredField("x");
        //false，类加载器会将x的字面量放到 字符串常量池中
        System.out.println(field.get(clazz) == x);
        String s = x.intern();
        //true
        System.out.println(field.get(clazz) == s);
        //false
        System.out.println(s == x);
    }
}

class Class1_1 {
    static String x = "why.java.demo.jvm.bytecode.Class1_1";
}