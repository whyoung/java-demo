package why.java.demo.jvm.bytecode;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ConstantPoolTest3 {

    public static void main(String[] args) throws Exception {
        /*
        测试字符串常量池的gc
        -Xms5m -Xmx5m -XX:+PrintStringTableStatistics -XX:+PrintGCDetails
        jstat -gcutil $pid 5
         */
        Random r = new Random();
        final int max = 1000;
        List<String> ref = new ArrayList<>(max);
        for (int i = 0;; i++) {
            String x = String.valueOf(r.nextInt()).intern();
            if (ref.size() < max) {
                ref.add(x);
            } else {
                ref.set(r.nextInt(max), x);
            }
        }
    }
}