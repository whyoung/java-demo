package why.java.demo.jvm.reflect;

public class ReflectTest3 {

    public static void main(String[] args) {
        Class3_1.getCaller();
        Class3_1.getCallerClass();
        Class3_1.getCallerClassLoader();
    }
}

class Class3_1 {
    public static void getCaller() {
        System.out.println(ReflectUtil.getCaller());
    }

    public static void getCallerClass() {
        System.out.println(ReflectUtil.getCallerClass() == ReflectTest3.class);
    }

    public static void getCallerClassLoader() {
        System.out.println(ReflectUtil.getCallerClassLoader());
    }
}
