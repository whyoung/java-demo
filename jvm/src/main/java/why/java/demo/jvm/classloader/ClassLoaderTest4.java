package why.java.demo.jvm.classloader;

/*
注意，这个代码是运行在 jdk8版本下，对于高版本的jdk，某些配置已经被弃用了
java classpath
 */
public class ClassLoaderTest4 {

    public static void main(String[] args) {
        String classpath = System.getProperty("java.class.path");
        String bootClassPath  = System.getProperty("sun.boot.class.path");
        String extClassPath  = System.getProperty("java.ext.dirs");
        System.out.println(bootClassPath);
        System.out.println(extClassPath);
        System.out.println(classpath);

        System.out.println(ClassLoaderTest4.class.getClassLoader());

        System.out.println(System.getProperty("java.system.class.loader"));

        ClassLoader loader = new ClassLoader() {
            @Override
            public Class<?> loadClass(String name) throws ClassNotFoundException {
                return super.loadClass(name);
            }
        };
        System.out.println(loader.getParent());
    }
}
