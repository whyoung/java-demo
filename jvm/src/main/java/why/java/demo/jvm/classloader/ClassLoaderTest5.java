package why.java.demo.jvm.classloader;

import java.lang.reflect.Method;
import java.sql.Driver;
import java.util.ServiceLoader;

public class ClassLoaderTest5 {

    public static void main(String[] args) throws Exception {

        System.out.println(ClassLoader.getSystemClassLoader());

        System.out.println(Thread.currentThread().getContextClassLoader());


        Class<?> clazz = Class.forName("why.java.demo.jvm.classloader.Class5_1");
        Method m = clazz.getDeclaredMethod("init", ClassLoader.class);
        m.setAccessible(true);
        m.invoke(clazz, ClassLoader.getSystemClassLoader());
    }
}

class Class5_1 {
    public static void init(ClassLoader loader) {
        System.out.println("---------------------------------");
        ServiceLoader<Driver> drivers = ServiceLoader.load(Driver.class, loader);
        for (Driver d : drivers) {
            System.out.println(d.getClass().getClassLoader());
        }
    }
}

