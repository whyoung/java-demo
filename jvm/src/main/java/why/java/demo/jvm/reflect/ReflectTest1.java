package why.java.demo.jvm.reflect;

import java.lang.reflect.Field;

public class ReflectTest1 {

    public static void main(String[] args) throws Exception {
        ReflectTest1 test = new ReflectTest1();
        test.testSynthetic();
    }

    /*
    测试 反射的isSynthetic，如果是由编译器生成的，返回true
     */
    public void testSynthetic() throws Exception {
        Class1_1 obj = new Class1_1() {
        };
        Class<?> clazz = obj.getClass();
        System.out.println(clazz);
        Field[] fields = clazz.getFields();
        for (Field f : fields) {
            //this$0, ReflectTest1 类型
            System.out.println(f.isSynthetic());
            System.out.println(f.getName() + ":" + f.getType().getName());
            System.out.println(f.get(obj) == this);
        }
        fields = clazz.getDeclaredFields();
        for (Field f : fields) {
            //this$0, ReflectTest1 类型
            System.out.println(f.isSynthetic());
            System.out.println(f.getName() + ":" + f.getType().getName());
            System.out.println(f.get(obj) == this);
        }
    }
}

class Class1_1 {
    public int x;
    private String y;
}
