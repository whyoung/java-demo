package why.java.demo.jvm.classloader;

import java.lang.reflect.Field;
import java.util.Random;

/*
    测试类的加载和初始化
    这里只包含和Class相关的，无关对象，创建对象意味着类一定被加载并且被初始化了
    这个测试类包含了比较常见的类加载与初始化的场景
 */
public class ClassLoaderTest1 {

    /**
     * 类的主动加载
     * 可以通过 -XX:+TraceClassLoading 参数观察类的加载
     */
    public static void main(String[] args) throws Exception {

        /*
        引用类的静态变量，触发类的加载，Class1_1 类被加载，并且静态代码块被执行
         */
        System.out.println(Class1_1.x);

        /*
        引用类的静态常量，Class1_2类未被加载，删掉classpath下的Class1_2.class，程序能正常运行
         */
        System.out.println(Class1_2.x);

        /*
        引用类的静态常量，但这个常量不是编译期间能够确定，Class1_3 会被加载，并且静态代码块被执行
         */
        System.out.println(Class1_3.x);

        /*
        直接引用类的Class对象，Class1_4 类被加载，但静态代码没有执行，意味着类没有被初始化
         */
        System.out.println(Class1_4.class);

        /*
        引用类的父类中定义的静态变量，Class1_5 类没有被加载，Class1_4 被加载，并且静态代码块执行
         */
        System.out.println(Class1_5.x);

        /*
        引用类的静态变量，Class1_5被加载并且静态方法被执行，Class1_4也被加载并且静态方法被执行
         */
        System.out.println(Class1_5.x1);

        /*
        Class.forName 加载类，initialize参数设置成false，Class1_6 被加载，但静态方法没有被执行
         */
        Class<?> clazz = Class.forName("why.java.demo.jvm.classloader.Class1_6", false, ClassLoaderTest1.class.getClassLoader());
        //下面这两行代码不会导致类的初始化
        Field field = clazz.getDeclaredField("x");
        field.setAccessible(true);
        System.out.println(field.get(clazz));

        /*
         引用类的静态变量，Class1_8类被加载且被初始化，其父类 Class1_6 被加载并且被初始化
         父接口Iface1_1 被加载，但未被初始化
         父接口Iface1_2 被加载，并且被初始化，因为 Iface1_2中定义了 default方法，符合类必须初始化的6种场景
         */
        System.out.println(Class1_8.x);
    }
}

class Class1_1 {
    public static int x = 1;
    static {
        System.out.println("Class1_1 static invoked");
    }
}

class Class1_2 {
    public static final int x = 2;
    static {
        System.out.println("Class1_2 static invoked");
    }
}

class Class1_3 {
    public static final int x = new Random().nextInt(4);
    static {
        System.out.println("Class1_3 static invoked");
    }
}

class Class1_4 {
    public static int x = 4;
    static {
        System.out.println("Class1_4 static invoked");
    }
}

class Class1_5 extends Class1_4 {
    public static int x1 = 5;
    static {
        System.out.println("Class1_5 static invoked");
    }
}

class Class1_6 {
    public static int x = 6;
    static {
        System.out.println("Class1_6 static invoked");
    }
}

interface Iface1_1 {
    public static final Object obj1 = new Object() {
        {
            System.out.println("Iface1_7 init");
        }
    };
}

interface Iface1_2 {
    public static final Object obj2 = new Object() {
        {
            System.out.println("Iface1_2 init");
        }
    };

    default void m() {
        //default method
    }
}

class Class1_8 extends Class1_6 implements Iface1_1, Iface1_2  {
    public static int x = 8;
    static {
        System.out.println("Class1_8 static invoked");
    }
}

