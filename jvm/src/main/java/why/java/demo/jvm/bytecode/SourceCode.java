package why.java.demo.jvm.bytecode;

public class SourceCode extends SuperClass {

    private int x;
    private static String sx = "static";

    public SourceCode(int x) {
        this.x = x;
    }

    static {
        System.out.println("out");
    }

    synchronized void test() {
    }

    void test2() {
        synchronized (this) {
            test();
        }
    }

    public static void main(String[] args) {

    }
}
