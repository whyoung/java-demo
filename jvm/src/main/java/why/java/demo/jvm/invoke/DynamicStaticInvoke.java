package why.java.demo.jvm.invoke;

public class DynamicStaticInvoke {

    public void test(Fruit f) {
        System.out.println("invoke fruit");
        System.out.println(f.desc());
    }

    public void test(Apple a) {
        System.out.println("invoke apple");
        System.out.println(a.desc());
    }

    public void test(Orange o) {
        System.out.println("invoke orange");
        System.out.println(o.desc());
    }

    public static void main(String[] args) {
        DynamicStaticInvoke invoke = new DynamicStaticInvoke();
        Fruit apple = new Apple();
        Fruit orange = new Orange();
        //invoke
        invoke.test(apple);
        invoke.test(orange);

        apple = new Orange();
        invoke.test(apple);
    }
}


class Fruit {

    public String desc() {
        return "fruit";
    }
}

class Apple extends Fruit {
    @Override
    public String desc() {
        return "apple";
    }
}

class Orange extends Fruit {
    @Override
    public String desc() {
        return "orange";
    }
}