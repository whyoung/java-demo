## java常量池

在java中，关于常量池，通常分为三类
1.class字节码文件的常量池
2.字符串常量池
3.jvm运行时常量池

对于 class字节码常量池，这是个静态的概念，java源码被编译成字节码之后，字节码结构中有一个部分存储的是常量池
以下面的代码为例
```java
package why.java.demo.jvm.bytecode;
public class ConstantPoolTest {
    public static void main(String[] args) {
        // args[0] = Lwhy/java/demo/jvm/bytecode/ConstantPoolTest;
        String x = args[0];
        String y = x.intern();
        System.out.println(x == y);
    }
}
```
对应的字节码常量池部分

```
Constant pool:
   #1 = Methodref          #6.#27         // java/lang/Object."<init>":()V
   #2 = Methodref          #28.#29        // java/lang/String.intern:()Ljava/lang/String;
   #3 = Fieldref           #30.#31        // java/lang/System.out:Ljava/io/PrintStream;
   #4 = Methodref          #32.#33        // java/io/PrintStream.println:(Z)V
   #5 = Class              #34            // why/java/demo/jvm/bytecode/ConstantPoolTest
   #6 = Class              #35            // java/lang/Object
   #7 = Utf8               <init>
   #8 = Utf8               ()V
   #9 = Utf8               Code
  #10 = Utf8               LineNumberTable
  #11 = Utf8               LocalVariableTable
  #12 = Utf8               this
  #13 = Utf8               Lwhy/java/demo/jvm/bytecode/ConstantPoolTest;
  #14 = Utf8               main
  #15 = Utf8               ([Ljava/lang/String;)V
  #16 = Utf8               args
  #17 = Utf8               [Ljava/lang/String;
  #18 = Utf8               x
  #19 = Utf8               Ljava/lang/String;
  #20 = Utf8               y
  #21 = Utf8               StackMapTable
  #22 = Class              #17            // "[Ljava/lang/String;"
  #23 = Class              #36            // java/lang/String
  #24 = Class              #37            // java/io/PrintStream
  #25 = Utf8               SourceFile
  #26 = Utf8               ConstantPoolTest.java
  #27 = NameAndType        #7:#8          // "<init>":()V
  #28 = Class              #36            // java/lang/String
  #29 = NameAndType        #38:#39        // intern:()Ljava/lang/String;
  #30 = Class              #40            // java/lang/System
  #31 = NameAndType        #41:#42        // out:Ljava/io/PrintStream;
  #32 = Class              #37            // java/io/PrintStream
  #33 = NameAndType        #43:#44        // println:(Z)V
  #34 = Utf8               why/java/demo/jvm/bytecode/ConstantPoolTest
  #35 = Utf8               java/lang/Object
  #36 = Utf8               java/lang/String
  #37 = Utf8               java/io/PrintStream
  #38 = Utf8               intern
  #39 = Utf8               ()Ljava/lang/String;
  #40 = Utf8               java/lang/System
  #41 = Utf8               out
  #42 = Utf8               Ljava/io/PrintStream;
  #43 = Utf8               println
  #44 = Utf8               (Z)V
```

可以看到，常量池共有44项，class字节码常量池主要存储哪些内容呢？

> 常量池中主要存放两大类常量：字面量（Literal）和符号引用（SymbolicReferences）。
> 字面量比较接近于Java语言层面的常量概念，如文本字符串、被声明为final的常量值等。
> 而符号引用则属于编译原理方面的概念，主要包括下面几类常量：
> 
>· 被模块导出或者开放的包（Package）
>
>· 类和接口的全限定名（Fully QualifiedName）
>
>· 字段的名称和描述符（Descriptor）
> 
>· 方法的名称和描述符·方法句柄和方法类型（MethodHandle、Method Type、InvokeDynamic）
> 
>· 动态调用点和动态常量（Dynamically-Computed Call Site、Dynamically-Computed Constant）
>
>Java代码在进行Javac编译的时候，并不像C和C++那样有“连接”这一步骤，而是在虚拟机加载Class文件的时候进行动态连接。
>也就是说，在Class文件中不会保存各个方法、字段最终在内存中的布局信息，这些字段、方法的符号引用不经过虚拟机在运行期转换的话是无法得到真正的内存入口地址，也就无法直接被虚拟机使用的。
>当虚拟机做类加载时，将会从常量池获得对应的符号引用，再在类创建时或运行时解析、翻译到具体的内存地址之中。

字符串常量池
简单来说，HotSpot VM里StringTable是个哈希表，里面存的是驻留字符串的引用（而不是驻留字符串实例自身）。
也就是说某些普通的字符串实例被这个StringTable引用之后就等同被赋予了“驻留字符串”的身份。
这个StringTable在每个HotSpot VM的实例里只有一份，被所有的类共享。类的运行时常量池里的CONSTANT_String类型的常量，经过解析（resolve）之后，同样存的是字符串的引用；解析的过程会去查询StringTable，以保证运行时常量池所引用的字符串与StringTable所引用的是一致的。

运行时常量池
java是动态连接的，在连接过程中，需要将class文件中常量池中的引用转换成实际的对象引用或方法入口
当类加载到内存中后，jvm就会将class常量池中的内容存放到运行时常量池中，由此可知，运行时常量池也是每个类都有一个。
class常量池中存的是字面量和符号引用，也就是说他们存的并不是对象的实例，而是对象的符号引用值。而经过解析（resolve）之后，也就是把符号引用替换为直接引用，解析的过程会去查询全局字符串池，也就是我们上面所说的StringTable，以保证运行时常量池所引用的字符串与全局字符串池中所引用的是一致的。


