## java的classpath 

classpath 是java 虚拟机在加载类时查找类的路径

在`com.sun.org.apache.bcel.internal.util.ClassPath` 类中定义了三个关于classpath的java启动参数

注意，以下代码都运行在java8 版本下，对于高版本，某些配置已经过时

```java
String boot_path  = System.getProperty("sun.boot.class.path");
String ext_path  = System.getProperty("java.ext.dirs");
String class_path = System.getProperty("java.class.path");
```
boot_path 是bootstrap 查找类的路径，主要是 `rt.jar` 等java核心类库
ext_path 是扩展类加载器查找类的路径
class_path 是关于应用程序所涉及的类的路径

```
$JAVA_HOME/jre/lib/resources.jar:$JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/jre/lib/sunrsasign.jar:$JAVA_HOME/jre/lib/jsse.jar:$JAVA_HOME/jre/lib/jce.jar:$JAVA_HOME/jre/lib/charsets.jar:$JAVA_HOME/jre/lib/jfr.jar:$JAVA_HOME/jre/classes
/Users/whyoung/Library/Java/Extensions:$JAVA_HOME/jre/lib/ext:/Library/Java/Extensions:/Network/Library/Java/Extensions:/System/Library/Java/Extensions:/usr/lib/java
.
```

默认的启动参数是根据jdk的安装路径确定的，但可以通过java的启动参数指定这个路径

对于 `sun.boot.class.path` 参数
如果直接指定用户自定义的路径
`java -Dsun.boot.class.path=. Test`
可能会得的如下的输出：
```
Error occurred during initialization of VM
java/lang/NoClassDefFoundError: java/lang/Object
```

虚拟机启动所依赖的核心类（比如`java.lang`包下的类）找不到，虚拟机就不能启动的
正确的做法是 将自定义的类路径加到这个原路径中，使用`-Xbootclasspath/a`参数，例如`java -Xbootclasspath/a:. Test`
然后看 `Test.class.getClassLoader()`，输出的null
注意，这种方式是不被提倡的，可能会破会虚拟机运行异常。

`java.ext.dirs` 是java的一些扩展类路径
执行 `java -Djava.ext.dirs=. Test`，输出Test的类加载器，类型却是`sun.misc.Launcher$AppClassLoader`，因为扩展类加载有些特殊，只能从jar包里加载类
将类打包成jar之后，再指定 `java.ext.dirs`，就能让应用程序的类被扩展类加载器加载了


`java.class.path` 是应用程序所用到的类，默认就是执行java 命令所要启动的包含main方法的类所在的路径
可以通过cp 命令，将其他的依赖的类加入到这个类路径下



