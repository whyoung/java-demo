### java类的加载时机

类的加载时机在java虚拟机规范中并没有严格规定

除了类必被初始化的6种情形、直接引用类的Class对象和手动加载类

下面的几种方式不会导致类被加载
1.引用类的编译期可以确定的常量

```java
class Test {
    public static final int x = 2;
    static {
        System.out.println("Test static invoked");
    }
}

//在其他类中已有 Test.x 时，不会导致 Test类的加载，运行时不需要 Test.class
//编译之后，引用Test.x 变量的代码会被编译成 iconst_2 指令，不依赖于Test类了
```
但如果常量是编译期间不能确定的
```java
public static final int x = new Random(),nextInt(2);
```
在这种情况下，常量x的值只能在运行期间（类初始化之后）确定，对 `Test.x` 的引用必须要在Test类加载并初始化之后
`Test.x` 编译之后的字节码 变成了 `getstatic`, 属于必须初始化类的6种情况之一，就必须要加载并初始化类

通过子类引用了父类的静态变量
```java
class Parent {
    public static int x = 1;
}

class Child extends Parent {
}

System.out.println(Child.x);
```
这种情况下，只会导致`Parent.class`类被加载，不会加载 `Child`类


