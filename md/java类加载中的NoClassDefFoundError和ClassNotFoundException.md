### java类加载中的NoClassDefFoundError 和 NoClassDefFoundException 

java程序在运行时，有时候会因为类加载器未能成功加载类，抛出`NoClassDefFoundError` 错误或 `NoClassDefFoundException`异常

对于 `NoClassDefFoundError` 比较常见的是maven项目，两个不同版本的jar包中的类不一样，项目启动的时候加载了错误版本的jar包导致找不到类
常见的错误还可能是 `java.lang.NoSuchMethodError`，`java.lang.NoClassDefFoundError` 和 `java.lang.LinkageError`等

对于 `NoClassDefFoundError` 错误
![NoClassDefFoundError类结构](https://i.vgy.me/8StQdf.png)

`NoClassDefFoundError` 是属于java异常体系的 `Error` 分支

java虚拟机在运行时，对于其依赖的类，如果类加载器没有成功加载类，会抛出这个错误
            
java 虚拟机规范中定义：
java虚拟机的实现允许类加载器在预料某个类将要被使用时就预先加载它，如果在预先加载的过程中遇到了.class文件缺失或者存在错误，类加载器必须在程序主动使用该类时才报告错误（LinkageError错误）。
如果这个类一直没有被程序主动使用，那么类加载器就不会报告错误（也就是不会抛出错误）。
这要求被依赖的类在当前类的编译期一定存在

对于 `ClassNotFoundException` 异常
![ClassNotFoundException类结构](https://i.vgy.me/mKWWoJ.png)
`ClassNotFoundException` 是属于java异常体系的 `Exception` 分支

当应用程序尝试通过两个字符串类名加载类时
1.Class类的forName方法
2.ClassLoader的findSystemClass方法和loadClass方法
如果类不存在，会抛出 `ClassNotFoundException`异常，这种情况不一定要求要查找的类在编译期一定存在，可能是通过类的二进制名称字符串查找类

测试代码：
```java
public class ClassLoaderTest {

    public static void main(String[] args) throws Exception {
        /*
        直接引用类Class2_1
         */
        try {
            System.out.println(Class2_1.x);
        } catch (NoClassDefFoundError err) {
            err.printStackTrace();
        }

        /*
        通过Class.forName 加载类Class_2
         */
        try {
            Class<?> clazz = Class.forName("Class_2", false, ClassLoaderTest.class.getClassLoader());
            Field f = clazz.getDeclaredField("x");
            f.setAccessible(true);
            System.out.println(f.get(clazz));
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }

    }
}

class Class_1 {
    public static int x = 1;
    static {
        System.out.println("Class_1 static invoked");
    }
}

class Class_2 {
    public static int x = 2;
    static {
        System.out.println("Class_2 static invoked");
    }
}
```
代码编译成功之后，删除类路径下的 `Class_1.class` 和 `Class_2.class` 文件，再运行程序
对于第一个 `try...catch` 方法，`catch`代码块中捕获到了`NoClassDefFoundError`错误
错误的cause：
```
Caused by: java.lang.ClassNotFoundException: Class_1
```
可以看见造成错误的原因是抛出了`ClassNotFoundException` 异常导致虚拟机抛出了 `NoClassDefFoundError`错误

对于第二个`try...catch`代码块，捕获到了`ClassNotFoundException` 异常，异常原因是类加载器没能成功加载`Class_2`类
