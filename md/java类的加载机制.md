### java类的加载机制

委托模型（Delegate Model）
> 对于类加载器来说，类加载器是有一个父类加载器的引用 parent，在委托模型中，类加载器在需要加载类时，并不会直接使用指定的类加载器去尝试加载类，
> 而是尝试使用其父类加载器加载，一直到启动类加载器（BootstrapClassLoader），如果父类加载器能够加载（或者已经加载）会直接将加载得到的Class对象
> 返回给当前类加载器，否则将由当前类加载器加载类。

java中的 `ClassLoader` 就是对类加载器的定义，这是一个抽象类，不能被直接实例化，如果需要定义自定义的类加载器，需要继承`ClassLoader`及其子类

内置的类加载器
Bootstrap ClassLoader
启动加载器，是Java类中加载层次最顶层的类加载器，负责加载JDK中的核心类库，如rt.jar,resources.jar,charsets.jar等；另外，在命令行中用-Xbootclasspath选项指定的jar包也是通过Bootstrap ClassLoader加载。
`ClassLoader.findBootstrapClassOrNull` 方法中就是通过 启动类加载器来加载类


关于 `ClassLoader`类中和类加载相关的方法

- findClass    查找类
- defineClass  定义类，将字节数字转换成Class对象，这是个final方法
- loadClass    加载类

类加载的默认类加载实现代码如下（去掉了注释和一些代码）
```java
protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException
{
    synchronized (getClassLoadingLock(name)) {
        //检查类是否被加载过
        Class<?> c = findLoadedClass(name);
        if (c == null) {
            //尝试委托给父类加载器加载
            try {
                if (parent != null) {
                    c = parent.loadClass(name, false);
                } else {
                    c = findBootstrapClassOrNull(name);
                }
            } catch (ClassNotFoundException e) {
            }
            if (c == null) {
                //父类加载器加载不了，尝试用当前类加载器加载
                c = findClass(name);
            }
        }
        if (resolve) {
            //解析类
            resolveClass(c);
        }
        return c;
    }
}
```
从这个方法的流程可以看到，这个是使用委托模型来加载类。



对于`findClass`方法，`ClassLoader`中默认实现是抛出异常，因此，要实现自己的类加载器必须要实现这个方法

`sun.rmi.rmic.iiop.ClassPathLoader` 实现实现了加载 classpath路径下的类的类加载器
```java
protected Class findClass(String var1) throws ClassNotFoundException {
    //读取构成类的class信息加载到字节数组中
    byte[] var2 = this.loadClassData(var1);
    //调用 defineClass方法将字节数组转换成Class对象
    return this.defineClass(var1, var2, 0, var2.length);
}
```
从上面的实现可以看出，我们只需要将定义类的class文件加载到内存中，再通过 `defineClass` 方法将内存中的数据转换成一个可用的Class对象。

定义自己的类加载器，从指定的目录下加载文件，这个类加载器不破坏委托模型
```java
/**
 * 自定义类加载器，从类路径
 * 保持委托加载模型
 */
class FilePathClassLoader extends ClassLoader {

    /**
     * 存在class文件的路径
     */
    private final String classFileLocation;

    public FilePathClassLoader(String classFileLocation) {
        super();
        this.classFileLocation = classFileLocation;
    }

    public FilePathClassLoader(ClassLoader parent, String classFileLocation) {
        super(parent);
        this.classFileLocation = classFileLocation;
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        if (name == null) {
            throw new IllegalArgumentException();
        }
        String classFilePath = classFileLocation + "/" + name.replace(".", "/") + ".class";
        File f = new File(classFilePath);
        if (!f.exists() || !f.isFile() || !f.canRead()) {
            throw new ClassNotFoundException();
        }
        try (FileInputStream in = new FileInputStream(f); ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            byte[] buf = new byte[1024];
            int len;
            while((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            return defineClass(name, out.toByteArray(), 0, out.size());
        } catch (IOException e) {
            throw new ClassNotFoundException();
        }
    }

    @Override
    protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
        System.out.println("loading class " + name + " by " + this.getClass().getSimpleName());
        return super.loadClass(name, resolve);
    }

    String getClassFileLocation() {
        return classFileLocation;
    }

    @Override
    public String toString() {
        return "FilePathClassLoader#" + classFileLocation;
    }
}
```

测试代码
```java
public class ClassLoaderTest {
    public static void main(String[] args) throws Exception {
        ClassLoader loader = new FilePathClassLoader("xxx");
        System.out.println(loader.getParent());
        Class<?> clazz = Class.forName("TestClass", false, loader);
        System.out.println(clazz.getClassLoader());
    }
}

//测试类
class TestClass {
    public static int x = 1;

    @Override
    public boolean equals(Object o) {
        return o instanceof TestClass;
    }
}
```
程序运行结果
```
当前类加载器的父类加载器是sun.misc.Launcher$AppClassLoader
sun.misc.Launcher$AppClassLoader@18b4aac2

调用当前类加载的loadClass加载 TestClass 类
loading TestClass by FilePathClassLoader

TestClass 类的实际类加载器是 sun.misc.Launcher$AppClassLoader
sun.misc.Launcher$AppClassLoader@18b4aac2
```
通过运行结果可以看到，`FilePathClassLoader` 类加载器的`findClass` 方法并没有执行，但 `loadClass`方法是执行了的，`loadClass`方法本质上只是调用`ClassLoader.loadClass`方法。

从上面对 `ClassLoader.loadClass`   方法的分析可以知道，这个方法会先委托其父类加载器加载类，父类加载器是 `sun.misc.Launcher$AppClassLoader` 

再看一下`sun.misc.Launcher$AppClassLoader`的`loadClass`的源码

```java
public Class<?> loadClass(String var1, boolean var2) throws ClassNotFoundException {
    //安全检查
    if (this.ucp.knownToNotExist(var1)) {
        Class var5 = this.findLoadedClass(var1);
        if (var5 != null) {
            if (var2) {
                this.resolveClass(var5);
            }

            return var5;
        } else {
            throw new ClassNotFoundException(var1);
        }
    } else {
        //调用父类的 loadClass
        return super.loadClass(var1, var2);
    }
}
```
可以看到，这里最终也是调用了其父类的 `loadClass`方法，`sun.misc.Launcher$AppClassLoader` 的父类是 `java.net.URLClassLoader` 类

然后发现最终还是调用的 `ClassLoader.loadClass`方法, `java.net.URLClassLoader`的父类加载器并没有加载成功，最终代码会回到`java.net.URLClassLoader`的`findClass`中

```java
protected Class<?> findClass(final String name) throws ClassNotFoundException
{
    final Class<?> result;
    try {
        result = AccessController.doPrivileged(
            new PrivilegedExceptionAction<Class<?>>() {
                public Class<?> run() throws ClassNotFoundException {
                    //将类名转换成文件路径，再从resource路径下查找clss文件
                    String path = name.replace('.', '/').concat(".class");
                    Resource res = ucp.getResource(path, false);
                    if (res != null) {
                        try {
                            return defineClass(name, res);
                        } catch (IOException e) {
                            throw new ClassNotFoundException(name, e);
                        }
                    } else {
                        return null;
                    }
                }
            }, acc);
    } catch (java.security.PrivilegedActionException pae) {
        throw (ClassNotFoundException) pae.getException();
    }
    if (result == null) {
        throw new ClassNotFoundException(name);
    }
    return result;
}
```
这个方法最终会从resource目录下查找`TestClass.class`文件。

因为我们编译之后，class文件是放到resource路径下的，因此`sun.misc.Launcher$AppClassLoader`的`findClass`会找到对应的class文件并转换成`TestClass`类

再将这个`Class`对象返回给 `sun.misc.Launcher$AppClassLoader`，表示`sun.misc.Launcher$AppClassLoader`类成功加载了类，`TestClass`的类加载器就是`sun.misc.Launcher$AppClassLoader`

最后，这个方法将返回给`FilePathClassLoader` 类的`loadClass`方法，所以这个调用并不会触发`FilePathClassLoader.findClass`方法。

但如果我们将 `resource`目录下的 `TestClass.class`文件删除，`sun.misc.Launcher$AppClassLoader` 的`loadClass`将会抛出异常，被`FilePathClassLoader`捕获并忽略，然后会触发
`FilePathClassLoader.findClass`方法调用

删除`resource`中的`TestClass.class`文件再执行的结果

```
//调用了FilePathClassLoader.loadClass
loading class TestClass by FilePathClassLoader

//调用了FilePathClassLoader.loadClass 加载 java.lang.Object,这个放到后面，可以先忽略
loading class java.lang.Object by FilePathClassLoader

TestClass 的类加载器是 FilePathClassLoader 类加载器
FilePathClassLoader#xxx
```
至此，我们完成了由一个自定义类加载器来加载类。



对于`FilePathClassLoader` 类加载器来说，我们并没有实际去修改它的`loadClass` 流程，整个过程还是遵循委托模型来加载的。

如果想打破这种委托模型，需要覆写`loadClass`方法
```java
/**
 * 自定义类加载器，不遵循委托模型
 * 因为破坏了委托模型，所以类加载器的
 */
class NonDelegateFilePathClassLoader extends FilePathClassLoader {

    public NonDelegateFilePathClassLoader(String classFileLocation) {
        super(classFileLocation);
    }

    public NonDelegateFilePathClassLoader(ClassLoader parent, String classFileLocation) {
        super(parent, classFileLocation);
    }

    @Override
    protected synchronized Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
        System.out.println("loading class " + name + " by " + this.getClass().getSimpleName());
        Class<?> c = findLoadedClass(name);
        if (c == null) {
            c = findClass(name);
            System.out.println("loaded class " + name + " by " + this.getClass().getSimpleName());
            if (resolve) {
                resolveClass(c);
            }
        }
        return c;
    }

    @Override
    public String toString() {
        return "NonDelegateFilePathClassLoader#" + getClassFileLocation();
    }
}
```
测试代码
```java
ClassLoader loader2 = new NonDelegateFilePathClassLoader(null,"xxx");
Class<?> clazz2 = Class.forName("TestClass", false, loader2);
System.out.println(clazz2.getClassLoader());
```
执行结果
```
调用NonDelegateFilePathClassLoader.loadClass 加载 TestClass
loading class TestClass by NonDelegateFilePathClassLoader

调用NonDelegateFilePathClassLoader.loadClass 加载 Object
loading class java.lang.Object by NonDelegateFilePathClassLoader

NonDelegateFilePathClassLoader 不能加载 java.lang.Object
Exception in thread "main" java.lang.NoClassDefFoundError: java/lang/Object
```

程序的第二个输出是 `loading class java.lang.Object by NonDelegateFilePathClassLoader`
回到前面`FilePathClassLoader`类的测试代码的第二行输出`loading class java.lang.Object by FilePathClassLoader`

这两个类加载器都去尝试加载了 `java.lang.Object`类？

在java中，`Object`类是所有类的父类，所以`TestClass`在加载前，必须先加载其父类或父接口，从这里的输出可以看到，会先使用 当前的类加载器去加载`java.lang.Object`
在默认实现这，Object类是由启动类加载器加载，因为我们覆写了`loadClass`代码，如果方法中不能加载 `Object`，将抛出异常


因为我们覆写了`loadClass`方法，这里不会将`java.lang.Object`的加载委托给其父类加载器加载，而且我们将`NonDelegateFilePathClassLoader`的父类加载器设置成了 null，导致只能通过当前类加载器去加载`java.lang.Object`，``NonDelegateFilePathClassLoader``很明显加载不了`java.lang.Object`,因为在该目录下不存在`Object.class`， 在`FilePathClassLoader`的例子中是因为委托给父类加载器去加载了，所以能加载`java.lang.Object`。



但现在内存里肯定是存在 `Object.class` 对象的（因为包含main方法的类加载了必然会导致Object类的加载），为什么这个类加载器不能使用这个Class对象呢？这个问题先留到后面。



我们修改一下`loadClass`方法，因为在 `TestClass`中，除了`Object`没有其他的直接类引用了，虽然`Object`类引用了其他类，这些类都可以使用启动类加载器（`Bootstrap`）加载

```java
@Override
protected synchronized Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
    System.out.println("loading class " + name + " by " + this.getClass().getSimpleName());
    Class<?> c = findLoadedClass(name);
    if (c == null) {
        try {
            Method m = ClassLoader.class.getDeclaredMethod("findBootstrapClassOrNull", String.class);
            m.setAccessible(true);
            c = (Class<?>) m.invoke(this, name);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException ignore) {
        }
        if (c == null) {
            c = findClass(name);
            System.out.println("loaded class " + name + " by " + this.getClass().getSimpleName());
        }
        if (resolve) {
            resolveClass(c);
        }
    }
    return c;
}
```
`ClassLoader`中定义了一个使用启动类加载器加载的方法`findBootstrapClassOrNull`，因为是私有的，我们不能直接使用，这里使用反射的方式来调用，保证之前由系统类加载器加载的类能被找到，再次运行

```
调用NonDelegateFilePathClassLoader.loadClass 加载 TestClass
loading class TestClass by NonDelegateFilePathClassLoader

调用NonDelegateFilePathClassLoader.loadClass 加载 Object
loading class java.lang.Object by NonDelegateFilePathClassLoader

成功通过NonDelegateFilePathClassLoader.loadClass 加载了 TestClass
loaded class TestClass by NonDelegateFilePathClassLoader

NonDelegateFilePathClassLoader#xxx
```



遗留问题：

1.为什么`NonDelegateFilePathClassLoader` 不能引用已有的`java.lang.Object`，即`ClassLoader.findLoadedClass`
针对每个类加载器而言，可以通过`findLoadedClass`方法查找已经加载的类，这个是每个`ClassLoader`对象都是独立的，即使是具有层级关系的类加载器，这个方法只有当调用了该类的loadClass之后才能被查出来

2.Object 类的加载和 ClassLoader类的加载

`Object` 类是如何类的父类，包括`Launcher`类（定义`ExtClassLoader` 和 `AppClassLoader`类的类）
所有bootstrap classloader 一定不是从java类实例化的（否则就造成了一个循环依赖）

bootstrap 类加载器会先加载了jvm运行所必须的类，再实例化 `ExtClassLoader` 和 `AppClassLoader`类加载器


