## java字节码解析

系统信息和java版本信息

mac os 15.7
java version "1.8.0_261"

以 SubClass.java 为例，以十六进制格式打开class文件

展示如下，第一行是偏移量，右侧是对于的字符串
```
  Offset: 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F 	
00000000: CA FE BA BE 00 00 00 34 00 50 0A 00 14 00 34 07    J~:>...4.P....4.
00000010: 00 35 0A 00 02 00 36 09 00 0C 00 37 0A 00 14 00    .5....6....7....
00000020: 38 07 00 39 0A 00 06 00 36 0A 00 3A 00 3B 07 00    8..9....6..:.;..
00000030: 3C 07 00 3D 0A 00 0A 00 36 07 00 3E 0A 00 0C 00    <..=....6..>....
00000040: 36 09 00 3F 00 40 08 00 41 0A 00 0C 00 42 0A 00    6..?.@..A....B..
00000050: 43 00 44 08 00 16 09 00 0C 00 45 07 00 46 07 00    C.D.......E..F..
00000060: 47 01 00 01 78 01 00 12 4C 6A 61 76 61 2F 6C 61    G...x...Ljava/la
00000070: 6E 67 2F 53 74 72 69 6E 67 3B 01 00 03 72 65 66    ng/String;...ref
00000080: 01 00 25 4C 77 68 79 2F 6A 61 76 61 2F 64 65 6D    ..%Lwhy/java/dem
00000090: 6F 2F 6A 76 6D 2F 62 79 74 65 63 6F 64 65 2F 52    o/jvm/bytecode/R
000000a0: 65 66 43 6C 61 73 73 3B 01 00 06 3C 69 6E 69 74    efClass;...<init
000000b0: 3E 01 00 03 28 29 56 01 00 04 43 6F 64 65 01 00    >...()V...Code..
000000c0: 0F 4C 69 6E 65 4E 75 6D 62 65 72 54 61 62 6C 65    .LineNumberTable
000000d0: 01 00 12 4C 6F 63 61 6C 56 61 72 69 61 62 6C 65    ...LocalVariable
000000e0: 54 61 62 6C 65 01 00 04 74 68 69 73 01 00 25 4C    Table...this..%L
000000f0: 77 68 79 2F 6A 61 76 61 2F 64 65 6D 6F 2F 6A 76    why/java/demo/jv
00000100: 6D 2F 62 79 74 65 63 6F 64 65 2F 53 75 62 43 6C    m/bytecode/SubCl
00000110: 61 73 73 3B 01 00 01 6D 01 00 03 28 29 49 01 00    ass;...m...()I..
00000120: 15 28 4C 6A 61 76 61 2F 6C 61 6E 67 2F 53 74 72    .(Ljava/lang/Str
00000130: 69 6E 67 3B 29 49 01 00 01 73 01 00 02 6D 31 01    ing;)I...s...m1.
00000140: 00 0D 53 74 61 63 6B 4D 61 70 54 61 62 6C 65 01    ..StackMapTable.
00000150: 00 0A 45 78 63 65 70 74 69 6F 6E 73 01 00 02 6D    ..Exceptions...m
00000160: 32 01 00 01 65 01 00 15 4C 6A 61 76 61 2F 6C 61    2...e...Ljava/la
00000170: 6E 67 2F 45 78 63 65 70 74 69 6F 6E 3B 07 00 3C    ng/Exception;..<
00000180: 01 00 04 6D 61 69 6E 01 00 16 28 5B 4C 6A 61 76    ...main...([Ljav
00000190: 61 2F 6C 61 6E 67 2F 53 74 72 69 6E 67 3B 29 56    a/lang/String;)V
000001a0: 01 00 04 61 72 67 73 01 00 13 5B 4C 6A 61 76 61    ...args...[Ljava
000001b0: 2F 6C 61 6E 67 2F 53 74 72 69 6E 67 3B 01 00 03    /lang/String;...
000001c0: 73 75 62 01 00 08 3C 63 6C 69 6E 69 74 3E 01 00    sub...<clinit>..
000001d0: 0A 53 6F 75 72 63 65 46 69 6C 65 01 00 0D 53 75    .SourceFile...Su
000001e0: 62 43 6C 61 73 73 2E 6A 61 76 61 0C 00 1A 00 48    bClass.java....H
000001f0: 01 00 23 77 68 79 2F 6A 61 76 61 2F 64 65 6D 6F    ..#why/java/demo
00000200: 2F 6A 76 6D 2F 62 79 74 65 63 6F 64 65 2F 52 65    /jvm/bytecode/Re
00000210: 66 43 6C 61 73 73 0C 00 1A 00 1B 0C 00 18 00 19    fClass..........
00000220: 0C 00 21 00 22 01 00 23 77 68 79 2F 6A 61 76 61    ..!."..#why/java
00000230: 2F 64 65 6D 6F 2F 6A 76 6D 2F 62 79 74 65 63 6F    /demo/jvm/byteco
00000240: 64 65 2F 45 78 43 6C 61 73 73 31 07 00 49 0C 00    de/ExClass1..I..
00000250: 4A 00 23 01 00 13 6A 61 76 61 2F 6C 61 6E 67 2F    J.#...java/lang/
00000260: 45 78 63 65 70 74 69 6F 6E 01 00 23 77 68 79 2F    Exception..#why/
00000270: 6A 61 76 61 2F 64 65 6D 6F 2F 6A 76 6D 2F 62 79    java/demo/jvm/by
00000280: 74 65 63 6F 64 65 2F 45 78 43 6C 61 73 73 32 01    tecode/ExClass2.
00000290: 00 23 77 68 79 2F 6A 61 76 61 2F 64 65 6D 6F 2F    .#why/java/demo/
000002a0: 6A 76 6D 2F 62 79 74 65 63 6F 64 65 2F 53 75 62    jvm/bytecode/Sub
000002b0: 43 6C 61 73 73 07 00 4B 0C 00 4C 00 4D 01 00 04    Class..K..L.M...
000002c0: 32 32 73 32 0C 00 28 00 23 07 00 4E 0C 00 4F 00    22s2..(.#..N..O.
000002d0: 48 0C 00 16 00 17 01 00 25 77 68 79 2F 6A 61 76    H.......%why/jav
000002e0: 61 2F 64 65 6D 6F 2F 6A 76 6D 2F 62 79 74 65 63    a/demo/jvm/bytec
000002f0: 6F 64 65 2F 53 75 70 65 72 43 6C 61 73 73 01 00    ode/SuperClass..
00000300: 20 77 68 79 2F 6A 61 76 61 2F 64 65 6D 6F 2F 6A    .why/java/demo/j
00000310: 76 6D 2F 62 79 74 65 63 6F 64 65 2F 49 66 61 63    vm/bytecode/Ifac
00000320: 65 01 00 04 28 49 29 56 01 00 11 6A 61 76 61 2F    e...(I)V...java/
00000330: 6C 61 6E 67 2F 49 6E 74 65 67 65 72 01 00 08 70    lang/Integer...p
00000340: 61 72 73 65 49 6E 74 01 00 10 6A 61 76 61 2F 6C    arseInt...java/l
00000350: 61 6E 67 2F 53 79 73 74 65 6D 01 00 03 6F 75 74    ang/System...out
00000360: 01 00 15 4C 6A 61 76 61 2F 69 6F 2F 50 72 69 6E    ...Ljava/io/Prin
00000370: 74 53 74 72 65 61 6D 3B 01 00 13 6A 61 76 61 2F    tStream;...java/
00000380: 69 6F 2F 50 72 69 6E 74 53 74 72 65 61 6D 01 00    io/PrintStream..
00000390: 07 70 72 69 6E 74 6C 6E 00 21 00 0C 00 14 00 01    .println.!......
000003a0: 00 15 00 02 00 08 00 16 00 17 00 00 00 12 00 18    ................
000003b0: 00 19 00 00 00 07 00 01 00 1A 00 1B 00 01 00 1C    ................
000003c0: 00 00 00 43 00 03 00 01 00 00 00 11 2A 03 B7 00    ...C........*.7.
000003d0: 01 2A BB 00 02 59 B7 00 03 B5 00 04 B1 00 00 00    .*;..Y7..5..1...
000003e0: 02 00 1D 00 00 00 0E 00 03 00 00 00 0D 00 05 00    ................
000003f0: 0E 00 10 00 0F 00 1E 00 00 00 0C 00 01 00 00 00    ................
00000400: 11 00 1F 00 20 00 00 00 01 00 21 00 22 00 01 00    ..........!."...
00000410: 1C 00 00 00 2F 00 01 00 01 00 00 00 05 2A B7 00    ..../........*7.
00000420: 05 AC 00 00 00 02 00 1D 00 00 00 06 00 01 00 00    .,..............
00000430: 00 13 00 1E 00 00 00 0C 00 01 00 00 00 05 00 1F    ................
00000440: 00 20 00 00 00 01 00 21 00 23 00 01 00 1C 00 00    .......!.#......
00000450: 00 36 00 01 00 02 00 00 00 02 03 AC 00 00 00 02    .6.........,....
00000460: 00 1D 00 00 00 06 00 01 00 00 00 18 00 1E 00 00    ................
00000470: 00 16 00 02 00 00 00 02 00 1F 00 20 00 00 00 00    ................
00000480: 00 02 00 24 00 17 00 01 00 01 00 25 00 23 00 02    ...$.......%.#..
00000490: 00 1C 00 00 00 56 00 02 00 02 00 00 00 11 2B C7    .....V........+G
000004a0: 00 0B BB 00 06 59 B7 00 07 BF 2B B8 00 08 AC 00    ..;..Y7..?+8..,.
000004b0: 00 00 03 00 1D 00 00 00 0E 00 03 00 00 00 1D 00    ................
000004c0: 04 00 1E 00 0C 00 20 00 1E 00 00 00 16 00 02 00    ................
000004d0: 00 00 11 00 1F 00 20 00 00 00 00 00 11 00 24 00    ..............$.
000004e0: 17 00 01 00 26 00 00 00 03 00 01 0C 00 27 00 00    ....&........'..
000004f0: 00 04 00 01 00 06 00 01 00 28 00 23 00 02 00 1C    .........(.#....
00000500: 00 00 00 7D 00 02 00 03 00 00 00 1A 2B C7 00 0B    ...}........+G..
00000510: BB 00 06 59 B7 00 07 BF 2B B8 00 08 AC 4D BB 00    ;..Y7..?+8..,M;.
00000520: 0A 59 B7 00 0B BF 00 01 00 0C 00 10 00 11 00 09    .Y7..?..........
00000530: 00 03 00 1D 00 00 00 16 00 05 00 00 00 25 00 04    .............%..
00000540: 00 26 00 0C 00 29 00 11 00 2A 00 12 00 2B 00 1E    .&...)...*...+..
00000550: 00 00 00 20 00 03 00 12 00 08 00 29 00 2A 00 02    ...........).*..
00000560: 00 00 00 1A 00 1F 00 20 00 00 00 00 00 1A 00 24    ...............$
00000570: 00 17 00 01 00 26 00 00 00 07 00 02 0C 44 07 00    .....&.......D..
00000580: 2B 00 27 00 00 00 06 00 02 00 06 00 0A 00 09 00    +.'.............
00000590: 2C 00 2D 00 01 00 1C 00 00 00 51 00 03 00 02 00    ,.-.......Q.....
000005a0: 00 00 15 BB 00 0C 59 B7 00 0D 4C B2 00 0E 2B 12    ...;..Y7..L2..+.
000005b0: 0F B6 00 10 B6 00 11 B1 00 00 00 02 00 1D 00 00    .6..6..1........
000005c0: 00 0E 00 03 00 00 00 30 00 08 00 31 00 14 00 32    .......0...1...2
000005d0: 00 1E 00 00 00 16 00 02 00 00 00 15 00 2E 00 2F    .............../
000005e0: 00 00 00 08 00 0D 00 30 00 20 00 01 00 08 00 31    .......0.......1
000005f0: 00 1B 00 01 00 1C 00 00 00 22 00 01 00 00 00 00    ........."......
00000600: 00 06 12 12 B3 00 13 B1 00 00 00 01 00 1D 00 00    ....3..1........
00000610: 00 0A 00 02 00 00 00 07 00 05 00 08 00 01 00 32    ...............2
00000620: 00 00 00 02 00 33                                  .....3
```

使用javap -verbose 命令查看对于的字节码信息
```
警告: 二进制文件SubClass包含why.java.demo.jvm.bytecode.SubClass
Classfile /Users/apple/Project/java-demo/jvm/target/classes/why/java/demo/jvm/bytecode/SubClass.class
  Last modified 2020-11-24; size 1574 bytes
  MD5 checksum 0b25ca2799bcf887dafd178cf7539c35
  Compiled from "SubClass.java"
public class why.java.demo.jvm.bytecode.SubClass extends why.java.demo.jvm.bytecode.SuperClass implements why.java.demo.jvm.bytecode.Iface
  minor version: 0
  major version: 52
  flags: ACC_PUBLIC, ACC_SUPER
Constant pool:
   #1 = Methodref          #20.#52        // why/java/demo/jvm/bytecode/SuperClass."<init>":(I)V
   #2 = Class              #53            // why/java/demo/jvm/bytecode/RefClass
   #3 = Methodref          #2.#54         // why/java/demo/jvm/bytecode/RefClass."<init>":()V
   #4 = Fieldref           #12.#55        // why/java/demo/jvm/bytecode/SubClass.ref:Lwhy/java/demo/jvm/bytecode/RefClass;
   #5 = Methodref          #20.#56        // why/java/demo/jvm/bytecode/SuperClass.m:()I
   #6 = Class              #57            // why/java/demo/jvm/bytecode/ExClass1
   #7 = Methodref          #6.#54         // why/java/demo/jvm/bytecode/ExClass1."<init>":()V
   #8 = Methodref          #58.#59        // java/lang/Integer.parseInt:(Ljava/lang/String;)I
   #9 = Class              #60            // java/lang/Exception
  #10 = Class              #61            // why/java/demo/jvm/bytecode/ExClass2
  #11 = Methodref          #10.#54        // why/java/demo/jvm/bytecode/ExClass2."<init>":()V
  #12 = Class              #62            // why/java/demo/jvm/bytecode/SubClass
  #13 = Methodref          #12.#54        // why/java/demo/jvm/bytecode/SubClass."<init>":()V
  #14 = Fieldref           #63.#64        // java/lang/System.out:Ljava/io/PrintStream;
  #15 = String             #65            // 22s2
  #16 = Methodref          #12.#66        // why/java/demo/jvm/bytecode/SubClass.m2:(Ljava/lang/String;)I
  #17 = Methodref          #67.#68        // java/io/PrintStream.println:(I)V
  #18 = String             #22            // x
  #19 = Fieldref           #12.#69        // why/java/demo/jvm/bytecode/SubClass.x:Ljava/lang/String;
  #20 = Class              #70            // why/java/demo/jvm/bytecode/SuperClass
  #21 = Class              #71            // why/java/demo/jvm/bytecode/Iface
  #22 = Utf8               x
  #23 = Utf8               Ljava/lang/String;
  #24 = Utf8               ref
  #25 = Utf8               Lwhy/java/demo/jvm/bytecode/RefClass;
  #26 = Utf8               <init>
  #27 = Utf8               ()V
  #28 = Utf8               Code
  #29 = Utf8               LineNumberTable
  #30 = Utf8               LocalVariableTable
  #31 = Utf8               this
  #32 = Utf8               Lwhy/java/demo/jvm/bytecode/SubClass;
  #33 = Utf8               m
  #34 = Utf8               ()I
  #35 = Utf8               (Ljava/lang/String;)I
  #36 = Utf8               s
  #37 = Utf8               m1
  #38 = Utf8               StackMapTable
  #39 = Utf8               Exceptions
  #40 = Utf8               m2
  #41 = Utf8               e
  #42 = Utf8               Ljava/lang/Exception;
  #43 = Class              #60            // java/lang/Exception
  #44 = Utf8               main
  #45 = Utf8               ([Ljava/lang/String;)V
  #46 = Utf8               args
  #47 = Utf8               [Ljava/lang/String;
  #48 = Utf8               sub
  #49 = Utf8               <clinit>
  #50 = Utf8               SourceFile
  #51 = Utf8               SubClass.java
  #52 = NameAndType        #26:#72        // "<init>":(I)V
  #53 = Utf8               why/java/demo/jvm/bytecode/RefClass
  #54 = NameAndType        #26:#27        // "<init>":()V
  #55 = NameAndType        #24:#25        // ref:Lwhy/java/demo/jvm/bytecode/RefClass;
  #56 = NameAndType        #33:#34        // m:()I
  #57 = Utf8               why/java/demo/jvm/bytecode/ExClass1
  #58 = Class              #73            // java/lang/Integer
  #59 = NameAndType        #74:#35        // parseInt:(Ljava/lang/String;)I
  #60 = Utf8               java/lang/Exception
  #61 = Utf8               why/java/demo/jvm/bytecode/ExClass2
  #62 = Utf8               why/java/demo/jvm/bytecode/SubClass
  #63 = Class              #75            // java/lang/System
  #64 = NameAndType        #76:#77        // out:Ljava/io/PrintStream;
  #65 = Utf8               22s2
  #66 = NameAndType        #40:#35        // m2:(Ljava/lang/String;)I
  #67 = Class              #78            // java/io/PrintStream
  #68 = NameAndType        #79:#72        // println:(I)V
  #69 = NameAndType        #22:#23        // x:Ljava/lang/String;
  #70 = Utf8               why/java/demo/jvm/bytecode/SuperClass
  #71 = Utf8               why/java/demo/jvm/bytecode/Iface
  #72 = Utf8               (I)V
  #73 = Utf8               java/lang/Integer
  #74 = Utf8               parseInt
  #75 = Utf8               java/lang/System
  #76 = Utf8               out
  #77 = Utf8               Ljava/io/PrintStream;
  #78 = Utf8               java/io/PrintStream
  #79 = Utf8               println
{
  static java.lang.String x;
    descriptor: Ljava/lang/String;
    flags: ACC_STATIC

  public why.java.demo.jvm.bytecode.SubClass();
    descriptor: ()V
    flags: ACC_PUBLIC
    Code:
      stack=3, locals=1, args_size=1
         0: aload_0
         1: iconst_0
         2: invokespecial #1                  // Method why/java/demo/jvm/bytecode/SuperClass."<init>":(I)V
         5: aload_0
         6: new           #2                  // class why/java/demo/jvm/bytecode/RefClass
         9: dup
        10: invokespecial #3                  // Method why/java/demo/jvm/bytecode/RefClass."<init>":()V
        13: putfield      #4                  // Field ref:Lwhy/java/demo/jvm/bytecode/RefClass;
        16: return
      LineNumberTable:
        line 13: 0
        line 14: 5
        line 15: 16
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            0      17     0  this   Lwhy/java/demo/jvm/bytecode/SubClass;

  public int m();
    descriptor: ()I
    flags: ACC_PUBLIC
    Code:
      stack=1, locals=1, args_size=1
         0: aload_0
         1: invokespecial #5                  // Method why/java/demo/jvm/bytecode/SuperClass.m:()I
         4: ireturn
      LineNumberTable:
        line 19: 0
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            0       5     0  this   Lwhy/java/demo/jvm/bytecode/SubClass;

  public int m(java.lang.String);
    descriptor: (Ljava/lang/String;)I
    flags: ACC_PUBLIC
    Code:
      stack=1, locals=2, args_size=2
         0: iconst_0
         1: ireturn
      LineNumberTable:
        line 24: 0
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            0       2     0  this   Lwhy/java/demo/jvm/bytecode/SubClass;
            0       2     1     s   Ljava/lang/String;

  public int m1(java.lang.String) throws why.java.demo.jvm.bytecode.ExClass1;
    descriptor: (Ljava/lang/String;)I
    flags: ACC_PUBLIC
    Code:
      stack=2, locals=2, args_size=2
         0: aload_1
         1: ifnonnull     12
         4: new           #6                  // class why/java/demo/jvm/bytecode/ExClass1
         7: dup
         8: invokespecial #7                  // Method why/java/demo/jvm/bytecode/ExClass1."<init>":()V
        11: athrow
        12: aload_1
        13: invokestatic  #8                  // Method java/lang/Integer.parseInt:(Ljava/lang/String;)I
        16: ireturn
      LineNumberTable:
        line 29: 0
        line 30: 4
        line 32: 12
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            0      17     0  this   Lwhy/java/demo/jvm/bytecode/SubClass;
            0      17     1     s   Ljava/lang/String;
      StackMapTable: number_of_entries = 1
        frame_type = 12 /* same */
    Exceptions:
      throws why.java.demo.jvm.bytecode.ExClass1

  public int m2(java.lang.String) throws why.java.demo.jvm.bytecode.ExClass1, why.java.demo.jvm.bytecode.ExClass2;
    descriptor: (Ljava/lang/String;)I
    flags: ACC_PUBLIC
    Code:
      stack=2, locals=3, args_size=2
         0: aload_1
         1: ifnonnull     12
         4: new           #6                  // class why/java/demo/jvm/bytecode/ExClass1
         7: dup
         8: invokespecial #7                  // Method why/java/demo/jvm/bytecode/ExClass1."<init>":()V
        11: athrow
        12: aload_1
        13: invokestatic  #8                  // Method java/lang/Integer.parseInt:(Ljava/lang/String;)I
        16: ireturn
        17: astore_2
        18: new           #10                 // class why/java/demo/jvm/bytecode/ExClass2
        21: dup
        22: invokespecial #11                 // Method why/java/demo/jvm/bytecode/ExClass2."<init>":()V
        25: athrow
      Exception table:
         from    to  target type
            12    16    17   Class java/lang/Exception
      LineNumberTable:
        line 37: 0
        line 38: 4
        line 41: 12
        line 42: 17
        line 43: 18
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
           18       8     2     e   Ljava/lang/Exception;
            0      26     0  this   Lwhy/java/demo/jvm/bytecode/SubClass;
            0      26     1     s   Ljava/lang/String;
      StackMapTable: number_of_entries = 2
        frame_type = 12 /* same */
        frame_type = 68 /* same_locals_1_stack_item */
          stack = [ class java/lang/Exception ]
    Exceptions:
      throws why.java.demo.jvm.bytecode.ExClass1, why.java.demo.jvm.bytecode.ExClass2

  public static void main(java.lang.String[]);
    descriptor: ([Ljava/lang/String;)V
    flags: ACC_PUBLIC, ACC_STATIC
    Code:
      stack=3, locals=2, args_size=1
         0: new           #12                 // class why/java/demo/jvm/bytecode/SubClass
         3: dup
         4: invokespecial #13                 // Method "<init>":()V
         7: astore_1
         8: getstatic     #14                 // Field java/lang/System.out:Ljava/io/PrintStream;
        11: aload_1
        12: ldc           #15                 // String 22s2
        14: invokevirtual #16                 // Method m2:(Ljava/lang/String;)I
        17: invokevirtual #17                 // Method java/io/PrintStream.println:(I)V
        20: return
      LineNumberTable:
        line 48: 0
        line 49: 8
        line 50: 20
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            0      21     0  args   [Ljava/lang/String;
            8      13     1   sub   Lwhy/java/demo/jvm/bytecode/SubClass;

  static {};
    descriptor: ()V
    flags: ACC_STATIC
    Code:
      stack=1, locals=0, args_size=0
         0: ldc           #18                 // String x
         2: putstatic     #19                 // Field x:Ljava/lang/String;
         5: return
      LineNumberTable:
        line 7: 0
        line 8: 5
}
SourceFile: "SubClass.java"
```

我们将依据这两个，做因此完整的class文件解析

在解析之前，我们需要解析class文件的格式，在java虚拟机规范中，合法的class字节码文件应该是按照如下格式组成的：
```
ClassFile {
  u4              magic;
  u2              minor_version;
  u2              major_version;
  u2              constant_pool_count;
  cp_info         constant_pool[constant_pool_count-1];
  u2              access_flags;
  u2              this_class;
  u2              super_class;
  u2              interfaces_count;
  u2              interfaces[interfaces_count];
  u2              fields_count;
  field_info      fields[fields_count];
  u2              methods_count;
  method_info     methods[methods_count];
  u2              attributes_count;
  attribute_info  attributes[attributes_count];
}
```
其中，u1，u2，u4，u8分别表示占1个字节，2个字节，4个以及8个字节
xxx_info 则表示这是一个复合结构，这个结构也是有多种其他数据结构组成

首先是4个字节的魔数，这个值默认是 `CAFEBABE`

第二部分是2个字节的次版本号，这里是 `00 00`，对应于jdk版本的`1.8.0_261`的第三位，后面的三位是更新版本号
jdk1.2之后到jdk12之前，次版本号都固定是0，这里一般没有太多的作用

第三部分是2个字节的主版本号，这里是`00 34`，对应于10进制的52，jdk的版本是这样的
java的版本号是从45开始，jdk1.1之后每个大版本版本号加1
java能实现向前兼容，对于小于等于当前jdk版本的合法class文件，java都是可以执行的

第四、第五部分是常量池

常量池中主要存放两大类常量：字面量（Literal）和符号引用（Symbolic References）。

字面量比较接近于Java语言层面的常量概念，如文本字符串、被声明为final的常量值等。

而符号引用则属于编译原理方面的概念，主要包括下面几类常量：

- 被模块导出或者开放的包（Package）
- 类和接口的全限定名（Fully Qualified Name）
- 字段的名称和描述符（Descriptor）
- 方法的名称和描述符·方法句柄和方法类型（Method Handle、Method Type、InvokeDynamic）
- 动态调用点和动态常量（Dynamically-Computed CallSite、Dynamically-Computed Constant）



第四部分是占2个字节，字节码中是`00 50`，表示常量池的大小，转换成十进制是80，表示常量池中元素的个数
这里需要注意，常量池的个数比常量池的元素格式多1，第0个表示不引用常量池中的任何项，因此，常量池项总共有79个
从javap 命令得到的 字节码指令可以清楚地看到，常量池项编号是从#1~#79

常量池项的类型目前17几种，当然这些类型是随着jdk的升级不断扩展的，例如在jdk加入了动态语言支持之后加入的`CONSTANT_Dynamic_info`

和 `CONSTANT_InvokeDynamic_info`，在jdk9中引入模块化之后加入的 `CONSTANT_Module_info` 和 `CONSTANT_Package_info`，常量池的类型是在不断变化的，但解析的方式都是相似的。



类型如下表：

| 类 型                            | 标 志 | 描 述                    |
| -------------------------------- | ----- | ------------------------ |
| CONSTANT_Utf8_info               | 1     | UTF-8 编码的字符串       |
| CONSTANT_Integer_info            | 3     | 整型字面量               |
| CONSTANT_Float_info              | 4     | 浮点型字面量             |
| CONSTANT_Long_info               | 5     | 长整型字面量             |
| CONSTANT_Double_info             | 6     | 双精度浮点型字面量       |
| CONSTANT_Class_info              | 7     | 类或接口的符号引用       |
| CONSTANT_String_info             | 8     | 字符串类型字面量         |
| CONSTANT_Fieldref_info           | 9     | 字段的符号引用           |
| CONSTANT_Methodref_info          | 10    | 类中方法的符号引用       |
| CONSTANT_InterfaceMethodref_info | 11    | 接口中方法的符号引用     |
| CONSTANT_NameAndType_info        | 12    | 字段或方法的部分符号引用 |
| CONSTANT_MethodHandle_info       | 15    | 表示方法句柄             |
| CONSTANT_MethodType_info         | 16    | 标识方法类型             |
| CONSTANT_Dynamic_info      | 17    | 表示一个动态计算常量   |
| CONSTANT_InvokeDynamic_info      | 18    | 表示一个动态方法调用点   |
| CONSTANT_Module_info             | 19    | 表示一个模块             |
| CONSTANT_Package_info            | 20    | 表示模块中开放或导出的包 |

每种类型有其独立的数据结构，但每种类型都有一个相同的属性，tag，类型为u1，对应于上面表中的标志列
遇到每种类型，再单独解析

常量池的第一项 tag值是 `0A`，十进制10，对应于类型中的`CONSTANT_Methodref_info`，
`CONSTANT_Methodref_info`类型的结构：
```
CONSTANT_Methodref_info {
    u1 tag;   //10
    u2 class_index; 
    u2 name_and_type_index;
}
```
紧接着的是一个u2类型的类名称索引，只是`00 14`，即20，这里值是，再下一个也是一个索引，值是`00 34`，即52，这里的索引是对于常量池中的引用
我们暂时忽略这部分索引所指代的内容

常量池的第二项 tag值是`07`，十进制7，对应于类型中的`CONSTANT_Class_info`
`CONSTANT_Class_info`类型的结构
```
CONSTANT_Class_info {
    u1 tag;//7
    u2 name_index;
}
```
下一个是一个u2类型的索引，值是`00 35`，即53

常量池的第三项 tag值是 `0A`，十进制10，对应于类型中的`CONSTANT_Methodref_info`
后面两个索引的值分别是 `00 02`（2）和`00 36`（54），常量池的第2项索引已经解析了，对应于class的信息

常量池的第四项 tag值是 `09`，十进制9，对应于类型中的`CONSTANT_Fieldref_info`
`CONSTANT_Fieldref_info`类型的结构
```
CONSTANT_Fieldref_info {
    u1 tag;     //9
    u2 class_index; 
    u2 name_and_type_index;
}
```
后面两个是u2类型的索引，值分别是`00 0C`（12） 和 `00 37`（55）

常量池的第五项 tag值是 `0A`，十进制10，对应于类型中的`CONSTANT_Methodref_info`，两个索引的值分别是 `00 14`（20）和`00 38`（56）
常量池的第六项 tag值是 `07`，十进制7，对应于类型中的`CONSTANT_Class_info`，对应的索引值是 `00 39`（57）
常量池的第七项 tag值是 `0A`，十进制10，对应于类型中的`CONSTANT_Methodref_info`，两个索引的值分别是 `00 06`（6）和`00 36`（54）
常量池的第八项 tag值是 `0A`，十进制10，对应于类型中的`CONSTANT_Methodref_info`，两个索引的值分别是 `00 3A`（58）和`00 3B`（59）
常量池的第九项 tag值是 `07`，十进制7，对应于类型中的`CONSTANT_Class_info`，对应的索引值是 `00 3C`（60）
常量池的第十项 tag值是 `07`，十进制7，对应于类型中的`CONSTANT_Class_info`，对应的索引值是 `00 3D`（61）

...

常量池的第十五项 tag值是 `08`，十进制8，对应于类型中的`CONSTANT_String_info`
`CONSTANT_String_info`类型的结构
```
CONSTANT_String_info {
    u1 tag;    //8
    u2 string_index;
}
```
后面一个是表示这个字符串字面量所表示的字符串的内容的引用，索引是`00 41`（65）

...

常量池的第二十二项 tag值是 `01`，十进制1，对应于类型中的`CONSTANT_Utf8_info`
`CONSTANT_Utf8_info`类型的结构
```
CONSTANT_Utf8_info {
    u1 tag;  //1
    u2 length;
    u1 bytes[length];
}
```
这个表示 utf-8编码的字符串，我们从十六进制编辑器右侧看到的那部分能看懂的数据，大部分都是表示这部分数据
tag后面是标识字符串长度的u2类型，值是`00 01`（1），然后是一个长度为1的u1类型的数组，对于字符串"x"
java 变量名和方法名这些都是编译之后存储于常量池中的，可以知道，这些名称的最大长度是u2类型的数值，最大值是65535，因此java中
变量名称，类名称这些都是有长度限制的


常量池的第五十二项 tag值是 `0C`，十进制12，对应于类型中的`CONSTANT_NameAndType_info`
`CONSTANT_NameAndType_info`类型的结构
```
CONSTANT_NameAndType_info {
    u1 tag;  //12
    u2 name_index;
    u2 descriptor_index;
}
```
这个类型表示名字和类型，根据名字和类型，可以唯一定位一个属性或方法，后面跟两个表示索引的u2类型，这里分别是 `00 1A` 和 `00 48`

常量池占据了很大的篇幅，当所有的常量池都解析完成之后，再对应于字节码的常量池部分，不同的类型的索引是通过不同方式组合在一起



第六部分是 access_flags，类的访问标志

访问标志列表：

| Flag Name      | Value  | Remarks                                                      |
| -------------- | ------ | ------------------------------------------------------------ |
| ACC_PUBLIC     | 0x0001 | public                                                       |
| ACC_PRIVATE    | 0x0002 | private                                                      |
| ACC_PROTECTED  | 0x0004 | protected                                                    |
| ACC_STATIC     | 0x0008 | static                                                       |
| ACC_FINAL      | 0x0010 | final                                                        |
| ACC_SUPER      | 0x0020 | 用于兼容早期编译器，新编译器都设置该标记，以在使用 *invokespecial* 指令时对子类方法做特定处理。 |
| ACC_INTERFACE  | 0x0200 | 接口，同时需要设置：ACC_ABSTRACT。不可同时设置：ACC_FINAL、ACC_SUPER、ACC_ENUM |
| ACC_ABSTRACT   | 0x0400 | 抽象类，无法实例化。不可与ACC_FINAL同时设置。                |
| ACC_SYNTHETIC  | 0x1000 | synthetic，由编译器产生，不存在于源代码中。                  |
| ACC_ANNOTATION | 0x2000 | 注解类型（annotation），需同时设置：ACC_INTERFACE、ACC_ABSTRACT |
| ACC_ENUM       | 0x4000 | 枚举类型                                                     |

不同的标志通过 & 计算最终得到 类的访问标志值



第七部分是 常量池中当前类的索引 `00 0C`  （12）
第八部分是 常量池中当前类的父类的索引`00 14` （20）
第九部分是 当前类实现的接口数量，`SubClass`只实现了一个接口，因此值是1
第十部分是 当前类实现的接口的信息索引
每个索引的类型是u2，这里只有一个，值是`00 15`（21）

第十一、十二部分是 类的字段信息
第十一部分是字段个数，类型是u2，值是`00 02`（2），表示`SubClass`类中共有两个字段
第十二部分是每个字段的信息，字段的结构定义如下
```
field_info {
    u2 access_flags;
    u2 name_index;
    u2 descriptor_index;
    u2 attributes_count;
    attribute_info attributes[attributes_count];
}
```

access_flags：访问标志，u2类型
字段的访问标志表

| 标志名称      | 标 志 值 | 含 义                |
| ------------- | -------- | -------------------- |
| ACC_PUBIC     | 0x0001   | 是否为 public        |
| ACC_PRIVATE   | 0x0002   | 是否为 private       |
| ACC_PROTECTED | 0x0004   | 是否为 protected     |
| ACC_STATIC    | 0x0008   | 是否为 static        |
| ACC_FINAL     | 0x0010   | 是否为 final         |
| ACC_VOLATILE  | 0x0040   | 是否为 volatile      |
| ACC_TRANSIENT | 0x0080   | 是否为 transient     |
| ACC_SYNTHETIC | 0x1000   | 是否由编译器自动生成 |
| ACC_ENUM      | 0x4000   | 是否为 enum          |

name_index：  字段的简单名称索引，u2类型
descriptor_index：字段的描述符索引，u2类型
attributes_count：字段属性个数
attribute_info：字段属性列表
关于属性，我们先放到后面再分析

我们以第一个字段为例
access_flags是`00 08`（8），对应于 static
name_index  是`00 16`（22），对应于常量池中的第22项，x
descriptor_index 是`00 17`（23）对应于 Ljava/lang/String;
attribute_info 是`00 00`（0），表示没有 attribute_info 属性

第十三、十四部分是 类的方法个数和方法信息
第十三部分是方法的数量，u2类型，值是`00 07`（7），表示有7个方法
第十四部分是方法的信息，方法的结构定义如下：
```
method_info {
    u2 access_flags;
    u2 name_index;
    u2 descriptor_index;
    u2 attributes_count;
    attribute_info attributes[attributes_count];
}
```
access_flags：访问标志，u2类型
方法的访问标志表：

| 标志名称         | 标 志 值 | 含 义                      |
| ---------------- | -------- | -------------------------- |
| ACC_PUBIC        | 0x0001   | 是否为 public              |
| ACC_PRIVATE      | 0x0002   | 是否为 private             |
| ACC_PROTECTED    | 0x0004   | 是否为 protected           |
| ACC_STATIC       | 0x0008   | 是否为 static              |
| ACC_FINAL        | 0x0010   | 是否为 final               |
| ACC_SYNCHRONIZED | 0x0020   | 是否为 sychronized         |
| ACC_BRIDGE       | 0x0040   | 是否由编译器产生的桥接方法 |
| ACC_VARARGS      | 0x0080   | 是否接受不定参数           |
| ACC_NATIVE       | 0x0100   | 是否为 native              |
| ACC_ABSTRACT     | 0x0400   | 是否为 abstract            |
| ACC_STRICTFP     | 0x0800   | 是否为 strictfp            |
| ACC_SYNTHETIC    | 0x1000   | 是否由编译器自动产生       |

name_index：  方法的简单名称索引，u2类型
descriptor_index：方法的描述符索引，u2类型
attributes_count：方法属性个数
attribute_info：方法属性列表

我们也以第一个方法为例
access_flags是`00 01`（1），对应于 `public`
name_index  是`00 1A`（26），对应于常量池第26项，<init>
descriptor_index 是`00 1B`（27），对应于 `()V`
attributes_count 是`00 01`（1），表示有一个属性

`attribute_info`的结构定义如下：
```
attribute_info {
     u2 attribute_name_index;
     u4 attribute_length;
     u1 info[attribute_length];
}
```
一个u2类型的 attribute_name_index 索引，值是`00 1C`（28），对应于常量池中的 `Code`
一个u4类型的 attribute_length，值是`00 00 00 43`（67）
一个长度为67的u1类型的数组，表示attribute_info 的内容
```
                      00 03 00 01 00 00 00 11 2A 03 B7 00    ...C........*.7.
000003d0: 01 2A BB 00 02 59 B7 00 03 B5 00 04 B1 00 00 00    .*;..Y7..5..1...
000003e0: 02 00 1D 00 00 00 0E 00 03 00 00 00 0D 00 05 00    ................
000003f0: 0E 00 10 00 0F 00 1E 00 00 00 0C 00 01 00 00 00    ................
00000400: 11 00 1F 00 20 00 00
```
这部分数据，我们需要根据属性的类型来解析，属性类型是code。
Code属性是method_info属性表中一种可变长度的属性，该属性中包括JVM指令及方法的辅助信息，如实例初始化方法或者类或接口的初始化方法。如果一个方法被声明为native或者abstract，那么其method_info结构中的属性表中一定不包含Code属性。否则，其属性表中必定包含一个Code属性。

Code属性的格式定义如下：
```
Code_attribute {
  u2 attribute_name_index;
  u4 attribute_length;
  u2 max_stack;
  u2 max_locals;
  u4 code_length;
  u1 code[code_length];
  u2 exception_table_length;
  { 
       u2 start_pc;
       u2 end_pc;
       u2 handler_pc;
       u2 catch_type;
  } exception_table[exception_table_length];
  u2 attributes_count;
  attribute_info attributes[attributes_count];
}
```
u2类型的最大栈深度，值是`00 03`（3）
u2类型的最大局部变量表长度，值是`00 01`（1）
u4类型的代码长度，值是`00 00 00 11`（17）
u1类型数组，代码的内容，值是`2A 03 B7 00 01 2A BB 00 02 59 B7 00 03 B5 00 04 B1`
code的解析
对于code的解析（也就是我们看到的通过助记符展示出来的内容），需要借助java虚拟机规范中的指令表
类似如下的表
每个指令对应于一个字节码，指令可能会包含参数，我们需要对照这个表来解析

| 字节码 | 助记符        | 指令含义                                        |
| ------ | ------------- | ----------------------------------------------- |
| 0x2a   | aload_0       | 将第一个引用类型本地变量推送至栈顶              |
| 0xb7   | invokespecial | 调用超类构建方法, 实例初始化方法, 私有方法      |
| 0x04   | iconst_1      | 将int型1推送至栈顶                              |
| 0xb5   | putfield      | 为指定类的实例域赋值                            |
| 0x12   | ldc           | 将int,float或String型常量值从常量池中推送至栈顶 |
| 0xb1   | return        | 从当前方法返回void                              |
| 0x02   | iconst_m1        |                               |
| 0x03   | iconst_0        |                               |
| 0x04   | iconst_1        |                               |
| 0x05   | iconst_2        |                               |
| 0x06   | iconst_3        |                               |
| 0xbb   | new        |                               |
| 0x59   | dup        |                               |

以这段代码为例：
code对应的内容是`2A 03 B7 00 01 2A BB 00 02 59 B7 00 03 B5 00 04 B1`

`2A`字节码对应的助记符是 aload_0
`03`字节码对应的助记符是 iconst_0
`B7`字节码对应的助记符是 invokespecial，这个指令后面带一个参数，指向常量池中的索引，`00 01`（1）
`2A`字节码对应的助记符是 aload_0
`BB`字节码对应的助记符是 new，也跟一个参数，`00 02`（2）
`59`字节码对应的助记符是 dup
`B7`字节码对应的助记符是 invokespecial，参数是 `00 03`（3）
`B5`字节码对应的助记符是 putfield，后面带一个参数，指向filed的索引，值是`00 04`（#4）
`B1`字节码对应的助记符是 return

自此，这个方法的字节码全部解析完成，结果如下：

```
aload_0
iconst_0
invokespecial #1
aload_0
new #2
dup
invokespecial #3
putfield #4
return
```

和javap命令生成的代码对比
```
stack=3, locals=1, args_size=1
 0: aload_0
 1: iconst_0
 2: invokespecial #1                  // Method why/java/demo/jvm/bytecode/SuperClass."<init>":(I)V
 5: aload_0
 6: new           #2                  // class why/java/demo/jvm/bytecode/RefClass
 9: dup
10: invokespecial #3                  // Method why/java/demo/jvm/bytecode/RefClass."<init>":()V
13: putfield      #4                  // Field ref:Lwhy/java/demo/jvm/bytecode/RefClass;
16: return
```

u2类型的异常表长度，值是`00 00`（0）
异常表长度为0，因此，没有异常表内容项
u2类型的属性长度，值是`00 02`（2），因此再递归解析这部分属性数据

第一个子属性：
    u2类型的 attribute_name_index 索引，值是`00 1D`（29），对应于常量池中的 `LineNumberTable`
    u4类型的 attribute_length，值是`00 00 00 0E`（14）
    属性内容对应的数据是 `00 03 00 00 00 0D 00 05 00 0E 00 10 00 0F`

`LineNumberTable` 结构定义：
    
```
LineNumberTable_attribute {
    u2 attribute_name_index;
    u4 attribute_length;
    u2 line_number_table_length;
    { 
        u2 start_pc;
        u2 line_number;
    } line_number_table[line_number_table_length];
}
```
   
u2类型的 line_number_table_length，值是`00 03`（3）
3个 line_number_table 项，每项对应两个u2类型的值，start_pc 和 line_number，从上面对 code的分析结果看，

分别是
`00 00 00 0D` => (0, 13)
`00 05 00 0E` => (5, 14)
`00 10 00 0F` => (16, 15)
这个是将java字节码指令和源文件对应起来，start_pc 是字节码中的相对位置，line_number是java源文件的位置

![源代码中的行](https://i.vgy.me/SK8LQq.png)

第13行代码`super(0)` 对应于下面的字节码指令
```
aload_0
iconst_0
invokespecial #1
```

第十四行 `this.ref = new RefClass()` 则对应下面的字节码指令
```
aload_0
new #2
dup
invokespecial #3
putfield #4
```
`return` 指令则对应源代码的最后一行

第二个子属性：
    u2类型的 attribute_name_index 索引，值是`00 1E`（30），对应于常量池中的 `LocalVariableTable`
    u4类型的 attribute_length，值是`00 00 00 0C`（12）
    属性内容对应的数据是 `00 01 00 00 00 11 00 1F 00 20 00 00`
    
`LocalVariableTable` 结构定义：
```
LocalVariableTable_attribute{
  u2 attribute_name_index;
  u4 attribute_length;
  u2 local_variable_table_length;
  {
    u2 start_pc;
    u2 length;
    u2 name_index;
    u2 descriptor_index;
    u2 index;
  }local_variable_table[local_variable_table_length];
}
```

start_pc 是当前local_variable_info所对应的局部变量的作用域的起始字节码偏移量；
length 是当前local_variable_info所对应的局部变量的作用域的大小。 也就是从字节码偏移量start_pc 到start_pc+length就是当前局部变量的作用域范围；
name_index 指向常量池中的一个CONSTANT_Utf8_info， 该CONSTANT_Utf8_info描述了当前局部变量的变量名
descriptor_index 指向常量池中的一个CONSTANT_Utf8_info， 该CONSTANT_Utf8_info描述了当前局部变量的描述符
index 描述了在该方法被执行时，当前局部变量在栈帧中局部变量表中的位置

这个结构体描述了 局部变量信息，包含类型、名称、作用域等

u2类型的 local_variable_table_length 的值是 `00 01`（1）
1个local_variable_table 项，每个占10个字节

start_pc    是 `00 00`    => 0
length      是 `00 11`    => 17
name_index  是 `00 1F`    => 31
descriptor_index 是`00 20` => 32
index       是`00 00`      => 0

已这个方法为例，这是是无参构造函数，但局部变量表的长度是1，找到常量池中对于的索引，对应于`this`
在java中，非静态方法的第一个参数默认都是当前对象的引用，所以在方法内可以使用this，这个参数的作用域是整个方法，参数的index是0（第一个）


第十五、十六部分是类的属性数量和属性信息，这两部分的对应的数据如下
`00 01 00 32 00 00 00 02 00 33`
属性数量值是 `00 01`（1），属性表只有一项
attribute_name_index 是 `00 32`（50），对应于常量池中的 `SourceFile`
`SourceFile`的结构定义：
```
SourceFile_attribute {
     u2 attribute_name_index;
     u4 attribute_length;
     u2 sourcefile_index;
}
```
attribute_length 是 `00 00 00 02`（2）
sourcefile_index 是 `00 33`（51），对应于常量池中的 `SubClass.java`，说明这个class文件是由 `SubClass.java` 文件编译的

至此，一个完整的class文件的解析就完成了。
这个例子并没有包含所有的场景，例如`synchronized`关键字等，这里补充一个方法信息中的异常表

异常表的结构如下：
```
exception_table {
    u2 start_pc;
    u2 end_pc;
    u2 handler_pc;
    u2 catch_type;
}
```

它包含4个字段，
如果当字节码在第start_pc行到第end_pc行之间（不包含end_pc行）出现了类型为catch_type或者其子类的异常（catch_type为指向一个CONSTANT_Class_info型常量的索引），则转到第handler_pc行继续处理。
当catch_type的值为0时，代表任意异常情况都需要转向到handler_pc处进行处理。


在Java 6版本之后JVM在class文件中引入了栈图(StackMapTable)属性。作用是为了提高JVM在类型检查的验证过程的效率，以下简称StackMapTable为栈图。 
栈图结构位于Code属性（指Classfile的Code属性）的属性表（ attributes table）结构中。
在字节码的Code属性中最多包含一个StackMapTable属性。
在Java 7版本之后把栈图作为字节码文件中的强制部分。 本来程序员是不需要关心JVM中的JIT编译器的细节，也不用知道编译原理或者数据流、控制流的细节。
但栈图强制了，如果要生成bytecode，必须准确知道每个字节码指令对应的局部变量和操作数栈的类型。这是因为Java7在编译的时期做了一些验证期间要做的事情，那就是类型检查，也就是栈图包含的内容。

`StackMapTable_attribute`的结构定义如下：
```
StackMapTable_attribute {
    u2              attribute_name_index;
    u4              attribute_length;
    u2              number_of_entries;
    stack_map_frame entries[number_of_entries];
}
```
attribute_name_index：对应的是常量池表的一个有效索引。也即CONSTANT_Utf8_info结构中表示“StackMapTable”的索引。
attribute_length：属性的长度
number_of_entries:表示entries表的成员数量。entries表中的所有成员都是一个stack_map_frame结构。
entries[]:entries表中的每一项都表示本方法的一个stack map frame，并且表中每一项都是有序的。 


