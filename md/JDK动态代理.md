### JDK动态代理

JDK动态代理中所涉及到的几个主要类（或接口）

`sun.misc.ProxyGenerator`
用来动态生成组成类的字节数组

`java.lang.reflect.Proxy`
动态代理的入口

主要的方法
1. 生成代理类的Class对象
`public static Class<?> getProxyClass(ClassLoader loader, Class<?>... interfaces)`

这个方法接收一个`ClassLoader` 和 一个 Class数组，这里有几个问题需要注意
- `ClassLoader` 参数所指代的类加载器和 接口类的类加载器的命名空间问题
- interfaces 参数必须保证都是接口，并且接口的访问权限没有问题，接口不能重复

生成类的核心方法，见`ProxyClassFactory`类
```java
public Class<?> apply(ClassLoader loader, Class<?>[] interfaces) {

    Map<Class<?>, Boolean> interfaceSet = new IdentityHashMap<>(interfaces.length);
    for (Class<?> intf : interfaces) {
        //校验类加载器，保证类加载器能访问接口类的类加载器的类
        Class<?> interfaceClass = null;
        try {
            interfaceClass = Class.forName(intf.getName(), false, loader);
        } catch (ClassNotFoundException e) {
        }
        if (interfaceClass != intf) {
            throw new IllegalArgumentException(intf + " is not visible from class loader");
        }
        // 校验参数interfaces 中的类必须是接口类型
        if (!interfaceClass.isInterface()) {
            throw new IllegalArgumentException(interfaceClass.getName() + " is not an interface");
        }
        // 校验参数interfaces 中的参数不能重复
        if (interfaceSet.put(interfaceClass, Boolean.TRUE) != null) {
            throw new IllegalArgumentException("repeated interface: " + interfaceClass.getName());
        }
    }

    String proxyPkg = null;     // package to define proxy class in
    int accessFlags = Modifier.PUBLIC | Modifier.FINAL;
    // 检查包权限
    for (Class<?> intf : interfaces) {
        int flags = intf.getModifiers();
        if (!Modifier.isPublic(flags)) {
            accessFlags = Modifier.FINAL;
            String name = intf.getName();
            int n = name.lastIndexOf('.');
            String pkg = ((n == -1) ? "" : name.substring(0, n + 1));
            if (proxyPkg == null) {
                proxyPkg = pkg;
            } else if (!pkg.equals(proxyPkg)) {
                throw new IllegalArgumentException(
                    "non-public interfaces from different packages");
            }
        }
    }
    if (proxyPkg == null) {
        proxyPkg = ReflectUtil.PROXY_PACKAGE + ".";
    }
    // 生成代理类的名字
    long num = nextUniqueNumber.getAndIncrement();
    String proxyName = proxyPkg + proxyClassNamePrefix + num;
    
    // 生成代理类Class对象
    byte[] proxyClassFile = ProxyGenerator.generateProxyClass(
        proxyName, interfaces, accessFlags);
    try {
        //将字节数组转换成 Class对象
        return defineClass0(loader, proxyName, proxyClassFile, 0, proxyClassFile.length);
    } catch (ClassFormatError e) {
        throw new IllegalArgumentException(e.toString());
    }
}
```

2. 生成代理类实现
`public static Object newProxyInstance(ClassLoader loader,
                                           Class<?>[] interfaces,
                                           InvocationHandler h) throws IllegalArgumentException`

这个方法接受三个参数，前两个和 `getProxyClass` 方法一样，第三个参数是 `InvocationHandler`类型，这个类是用来定义我们对要代理的方法的包装逻辑


`java.lang.reflect.InvocationHandler`
这个接口只定义了一个方法
`public Object invoke(Object proxy, Method method, Object[] args) throws Throwable;`

方法接收三个参数，代理对象，方法反射，参数，可以看下生成的反射类的代码，假设接口中定义了test方法
生成的代理类的test方法如下(可以通过设置`sun.misc.ProxyGenerator.saveGeneratedFiles=true`参数让动态代理生成的类写到本地磁盘)：
m3 是反射得到的表示 test方法的对象，可以看到，代理类的方法最终是`InvocationHandler`的`invoke`方法
```java
public final void test() throws  {
    try {
        super.h.invoke(this, m3, (Object[])null);
    } catch (RuntimeException | Error var2) {
        throw var2;
    } catch (Throwable var3) {
        throw new UndeclaredThrowableException(var3);
    }
}
```

同时，可以看到，代理类继承了 `java.lang.reflect.Proxy`类（因为java是单继承，这也说明了jdk动态代理只能实现对接口的代理而不能对类代理），而`Proxy`类中有一个成员h，就是 `InvocationHandler` 类型，同时，
代理类生成了一个构造方法，接收一个`InvocationHandler`类型的参数，然后直接调用了父类的 构造方法，所以，代理类的test方法最终就是调用生成对象时的`InvocationHandler`
的`invoke`方法

我们只要自定义invoke方法实现逻辑就能完成对类的代理，由此可见，在JDK动态代理中，代理对象和被代理对象的类型没有任何关系，只是通过
实现代理对象所实现的接口（接口是由参数传入），这样就能保证代理对象和被代理对象具有某些相同的方法（这样反射调用拿到的
Method可以保证是同一个对象，但不在参数中的接口或类继承的方法是不能被代理的）


代理对象的接口是通过参数传递的，所以这个参数是可以由用户自定义，这个时候就可能出现几个问题
1.被代理对象没有实现接口
2.代理对象有接口被代理对象没有实现
3.接口中存在重复的方法定义

先看第三个问题：
接口中存在相同的方法定义（方法名，参数列表都相同），如果返回值不同，编译通不过，因为java的重载是不包括返回值的类型的
如果方法定义完全相同，对被代理类和代理类来说，只会存在一个方法实现，这种情况也没有问题

第二个问题，被代理对象没有实现接口，这时被代理类对象只能用它实现的接口类型接收（或者是Object类型），而不能用它没有实现的接口接收
第一个问题，被代理对象没有实现参数里面的接口，这时，调用代理类的方法（被代理类未实现的接口中的方法）就会抛出异常，通常异常信息如下
```
java.lang.IllegalArgumentException: object is not an instance of declaring class
```

         






