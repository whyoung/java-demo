package why.java.demo.limiter.limit;

import redis.clients.jedis.Jedis;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 令牌桶
 * 维持一个bucket，里面存放的是令牌，服务方定时放入一定数量的令牌，获取到令牌的请求才能继续，否则拒绝请求
 * 对比漏桶算法，这种方式比较适合做接口的限流（同步接口）
 * 也适合突发流量，可以通过令牌发放速度控制
 * @author apple
 */
public class TokenBucketLimiter extends AbstractLimiter {

    private static final String KEY = "token-bucket";
    private final ScheduledThreadPoolExecutor executor;
    private final String[] tokens;

    public TokenBucketLimiter(int period, int capacity) {
        super(period, capacity);
        executor = new ScheduledThreadPoolExecutor(1);
        tokens = new String[capacity];
        for (int i = 0; i < capacity; i++) {
            tokens[i] = "1";
        }
        Jedis jedis = new Jedis("127.0.0.1", 6379);
        jedis.del(KEY);
        //预先放入capacity个token，防止定时任务不能及时执行
        jedis.rpush(KEY, tokens);
        executor.scheduleAtFixedRate(() -> {
            //这里时间粒度可以更细一点，比如每多少毫秒发一个token，保证不会导致并发量瞬时提升
            jedis.rpush(KEY, tokens);
        }, period, period, TimeUnit.SECONDS);
    }

    @Override
    public boolean allow() {
        try (Jedis jedis = getJedis()) {
            return jedis.lpop(KEY) != null;
        }
    }
}
