package why.java.demo.limiter;

import why.java.demo.limiter.limit.Limiter;
import why.java.demo.limiter.limit.TokenBucketLimiter;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class RateLimiterTest {

    public static void main(String[] args) throws Exception {
        //每5秒中只允许
        int period = 5;
        int capacity = 10;

//        Limiter limiter = new SlideLimiter(period, capacity);
//        Limiter limiter = new CounterLimiter(period, capacity);
//        Limiter limiter = new BucketLimiter(period, capacity);
        Limiter limiter = new TokenBucketLimiter(period, capacity);
        AtomicInteger counter = new AtomicInteger();
        Thread[] threads = new Thread[capacity * 2];
        //启动 2 倍阈值的线程，同时发送请求，大概一半的请求会被阻塞
        for (int i = 0; i < capacity * 2; i++) {
            threads[i] = new Thread(() -> {
                for (int j = 0; j<5; j++) {
                    method(limiter, counter);
                    Limiter.sleep(TimeUnit.SECONDS.toMillis(period) / 2);
                }
            });
        }
        for (Thread t : threads) {
            t.start();
        }
        new CountDownLatch(1).await();
    }

    private static void method(Limiter limiter, AtomicInteger counter) {
        if (limiter.allow()) {
            limiter.invoke(() -> System.out.println("invoke method " + counter.incrementAndGet()));
        } else {
            System.out.println("blocking");
        }
    }
}
