package why.java.demo.limiter.limit;

import redis.clients.jedis.Jedis;

import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * 滑动窗口
 * 每次统计一个时间窗口内的请求数，如果请求数大于最大请求数，拒绝服务
 * 否则，在时间窗口内加一条请求记录
 *
 * 不太适合有突发流量的场景，实现比较简单
 * @author apple
 */
public class SlideLimiter extends AbstractLimiter {

    private static final String KEY = "slider";

    private final long milliseconds;

    public SlideLimiter(int period, int capacity) {
        super(period, capacity);
        this.milliseconds = TimeUnit.SECONDS.toMillis(period);
    }

    @Override
    public boolean allow() {
        try (Jedis jedis = getJedis()) {
            long current = System.currentTimeMillis();
            long start = current - milliseconds;
            //统计 从 start - current 这段时间内的请求数量，如果超出，就拒绝请求
            Set<String> set = jedis.zrangeByScore(KEY, start, current);
            if (set == null || set.size() < capacity) {
                //添加一条请求记录，member的值是任意的，尽量保证唯一，score是当前的时间戳
                jedis.zadd(KEY, current, UUID.randomUUID().toString());
                return true;
            }
            return false;
        }
    }
}
