package why.java.demo.limiter.limit;

import redis.clients.jedis.Jedis;

/**
 * 计数器
 * 维持一个计数器，请求之前检查计数器的数值，如果大于阈值，拒绝服务，否则计数器加1，在一段时间之后，将计数器清零
 *
 * 实现简单，不适合突发流量
 * @author apple
 */
public class CounterLimiter extends AbstractLimiter {

    private static final String KEY = "counter";

    public CounterLimiter(int period, int capacity) {
        super(period, capacity);
    }

    @Override
    public boolean allow() {
        try (Jedis jedis = getJedis()) {
            String val = jedis.get(KEY);
            //判断key是否超出最大限制
            if (val == null || Long.parseLong(val) < capacity) {
                jedis.incr(KEY);
                if (val == null) {
                    //设置过期时间
                    jedis.expire(KEY, period);
                }
                return true;
            } else {
                return false;
            }
        }
    }
}
