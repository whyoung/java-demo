package why.java.demo.limiter.limit;

import java.util.concurrent.TimeUnit;

public interface Limiter {

    /**
     * 是否允许请求进入
     * @return
     */
    boolean allow();

    /**
     * 执行实际的请求，这个方法只是为了模拟服务端处理
     * @param task 任务
     */
    void invoke(Runnable task);

    static void sleep(long milliseconds) {
        try {
            TimeUnit.MILLISECONDS.sleep(milliseconds);
        } catch (InterruptedException ignore) {
        }
    }
}
