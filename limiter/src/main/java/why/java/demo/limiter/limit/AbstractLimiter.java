package why.java.demo.limiter.limit;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * @author apple
 */
abstract class AbstractLimiter implements Limiter {

    /**
     * 时间段，单位，秒
     */
    protected final int period;

    /**
     * 最大限制
     */
    protected final int capacity;

    private final JedisPool pool;

    public AbstractLimiter(int period, int capacity) {
        if (period <= 0 || capacity <= 0) {
            throw new IllegalArgumentException("period or capacity can't be null");
        }
        this.period = period;
        this.capacity = capacity;
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        // 设置最大连接数，和最大阈值保持一致
        jedisPoolConfig.setMaxTotal(capacity);
        pool = new JedisPool(jedisPoolConfig, "127.0.0.1", 6379);
    }

    Jedis getJedis() {
        return pool.getResource();
    }

    @Override
    public void invoke(Runnable task) {
        if (task != null) {
            Limiter.sleep(50);
            task.run();
        }
    }
}
