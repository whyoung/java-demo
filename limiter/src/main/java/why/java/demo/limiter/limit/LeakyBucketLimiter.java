package why.java.demo.limiter.limit;


import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 桶漏算法（漏斗算法）
 * 维持一个固定大小的容器（bucket），每次请求，如果容器未满，将请求加入到容器中，服务方会从容器中每次消耗一定的数据
 * 可以对比线程池，定义一个固定大小的queue，每次将任务放进queue，后台线程将从queue获取任务并执行
 * <p>
 * 不适合突发流量，限制请求比较均匀
 *
 * @author apple
 */
public class LeakyBucketLimiter implements Limiter {

    /**
     * 时间间隔
     */
    protected final int period;

    /**
     * 最大限制
     */
    protected final int capacity;

    private final ExecutorService executor;

    private final BlockingQueue<Runnable> queue;

    public LeakyBucketLimiter(int period, int capacity) {
        this.period = period;
        this.capacity = capacity;
        queue = new ArrayBlockingQueue<>(capacity);

        //模拟后台实际处理程序
        executor = new ThreadPoolExecutor(1, 1,
                0L, TimeUnit.MILLISECONDS,
                queue,
                r -> new Thread(() -> {
                    Limiter.sleep(TimeUnit.SECONDS.toMillis(period) / capacity + 50);
                    r.run();
                }), new RejectedExecutionHandler() {
            @Override
            public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
                //如果超出queue的容量限制，则阻止请求
                System.out.println("blocking");
            }
        });
    }

    @Override
    public boolean allow() {
        //允许所有请求进入，在 invoke 方法中做实际的流量限制
        return true;
    }

    @Override
    public void invoke(Runnable task) {
        executor.submit(task);
    }
}
