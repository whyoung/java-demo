package why.demo.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 *
 * @author : apple
 * @date : 2021/3/30
 * @time : 12:28 下午
 */
public final class NumGenerator {

    private static final Random R = new Random();

    private NumGenerator() {
    }

    public static int randomInt(int bound) {
        return R.nextInt(bound);
    }

    public static List<Integer> randomInts(int bound, int count) {
        List<Integer> list = new ArrayList<>(count);
        for (int i = 0; i< count; i++) {
            list.add(R.nextInt(bound));
        }
        return list;
    }

    public static List<Integer> randomDistinctInts(int bound, int count) {
        if (count >= bound) {
            throw new IllegalArgumentException();
        }
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i< count; i++) {
            set.add(R.nextInt(bound));
        }
        return new ArrayList<>(set);
    }
}
