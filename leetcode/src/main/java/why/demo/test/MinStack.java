package why.demo.test;

import why.demo.datastruct.Stack;
import why.demo.utils.NumGenerator;

import java.util.List;

public class MinStack extends Stack<Integer> {

    private final Stack<Integer> dataStack;
    private final Stack<Integer> minStack;

    public MinStack() {
        this.dataStack = new Stack<>();
        this.minStack = new Stack<>();
    }

    @Override
    public void push(Integer o) {
        dataStack.push(o);
        if (minStack.isEmpty() || minStack.peek() >= o) {
            minStack.push(o);
        }
    }

    @Override
    public Integer pop() {
        Integer v = dataStack.pop();
        if (!minStack.isEmpty() && minStack.peek().equals(v)) {
            minStack.pop();
        }
        return v;
    }

    @Override
    public Integer peek() {
        return dataStack.peek();
    }

    @Override
    public boolean isEmpty() {
        return dataStack.isEmpty();
    }

    public Integer getMin() {
        return minStack.isEmpty() ? null : minStack.peek();
    }

    @Override
    public String toString() {
        return "MinStack{" +
                "dataStack=" + dataStack +
                ", minStack=" + minStack +
                '}';
    }

    public static void main(String[] args) {
        MinStack m = new MinStack();
        List<Integer> list = NumGenerator.randomInts(20, 40);
        list.forEach(i -> {
            m.push(i);
            System.out.println(m.dataStack);
            System.out.println(m.getMin());
        });
        while (!m.isEmpty()) {
            System.out.println(m.getMin());
            m.pop();
        }
    }
}
