package why.demo.test;

import why.demo.utils.NumGenerator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class StackMove {

    public static void main(String[] args) {

        List<Integer> list = NumGenerator.randomInts(30, 20);

        int[] arr = {18, 24, 1, 25, 27, 19, 19, 3, 7, 14, 24, 18, 1, 3, 18, 27, 19, 2, 4, 22};
//        for (int i = 0;i<20; i++) {
//            arr[i] = NumGenerator.randomInt(30);
//        }
        System.out.println(Arrays.toString(arr));
        Stack<Integer> maxStack = new Stack<>();

        int w = 3;
        int len = arr.length;
        List<Integer> ret = new ArrayList<>(len - w + 1);
        for (int i =0; i<w; i++) {
            if (maxStack.isEmpty() || maxStack.peek() <= arr[i]) {
                maxStack.push(arr[i]);
            }
        }
        System.out.println(maxStack);
        for (int i =1; i<len - w; i++) {
            if (!maxStack.isEmpty() && maxStack.peek() == arr[i-1]) {
                maxStack.pop();
            }
            if (maxStack.isEmpty() || maxStack.peek() <= arr[i + w]) {
                maxStack.push(arr[i + w]);
            }
            System.out.println(maxStack);
            ret.add(maxStack.peek());
        }
        System.out.println(ret);
    }
}
