package why.demo.test;

import why.demo.datastruct.Stack;
import why.demo.utils.NumGenerator;

public class StackSort {

    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        for (int i = 0;i<20; i++) {
            stack.push(NumGenerator.randomInt(100));
        }
        System.out.println(stack);

        Stack<Integer> extra = new Stack<>();
        while (!stack.isEmpty()) {
            int x = stack.pop();
            while(!extra.isEmpty() && extra.peek() > x) {
                stack.push(extra.pop());
            }
            extra.push(x);
        }
        while (!extra.isEmpty()) {
            stack.push(extra.pop());
        }

        System.out.println(stack);
    }
}
