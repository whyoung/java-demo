package why.demo.test;


import why.demo.utils.NumGenerator;

import java.util.Arrays;
import java.util.LinkedList;

public class Max {

    public static void main(String[] args) {
        int[] arr = new int[20];
        for (int i =0; i<20;i++) {
            arr[i]= NumGenerator.randomInt(50);
        }
        System.out.println(Arrays.toString(arr));
        int w = 3;
        //保存从i ~ i+w 的元素从大到小的顺序
        LinkedList<Integer> qmax = new LinkedList<>();
        int[] res = new int[arr.length - w + 1];
        int index = 0;
        for (int i =0 ;i<arr.length; i++) {
            while (!qmax.isEmpty() && arr[qmax.peekLast()] <= arr[i]) {
                qmax.pollLast();
            }
            qmax.addLast(i);
            if (qmax.peekFirst() == i - w) {
                qmax.pollFirst();
            }
            if (i >= w - 1) {
                res[index++] = arr[qmax.peekFirst()];
            }
        }
        System.out.println(Arrays.toString(res));
    }
}
