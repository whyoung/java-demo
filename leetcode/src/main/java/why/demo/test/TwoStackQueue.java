package why.demo.test;

import why.demo.datastruct.Queue;
import why.demo.datastruct.Stack;
import why.demo.utils.NumGenerator;

import java.util.List;
import java.util.Objects;

public class TwoStackQueue extends Queue<Integer> {

    private final Stack<Integer> stack1;
    private final Stack<Integer> stack2;

    public TwoStackQueue() {
        this.stack1 =  new Stack<>();
        this.stack2 = new Stack<>();
    }

    @Override
    public void add(Integer i) {
        stack1.push(i);
    }

    @Override
    public Integer poll() {
        if (stack2.isEmpty()) {
            if (stack1.isEmpty()) {
                return null;
            }
            while (!stack1.isEmpty()) {
                stack2.push(stack1.pop());
            }
        }
        return stack2.pop();
    }

    @Override
    public Integer peek() {
        if (stack2.isEmpty()) {
            if (stack1.isEmpty()) {
                return null;
            }
            while (!stack1.isEmpty()) {
                stack2.push(stack1.pop());
            }
        }
        return stack2.peek();
    }

    @Override
    public boolean isEmpty() {
        return stack1.isEmpty() && stack2.isEmpty();
    }

    public static void main(String[] args) {
        TwoStackQueue q = new TwoStackQueue();
        List<Integer> list = NumGenerator.randomInts(20, 40);
        list.forEach(q::add);
        for (int i=0;i<list.size();i++) {
            System.out.println(Objects.equals(list.get(i), q.poll()));
        }
    }
}
