package why.demo.datastruct;

public class Stack<T> {

    private Node<T> top;

    public void push(T t) {
        Node<T> node = new Node<>(t);
        if (top != null) {
            node.next = top;
        }
        top = node;
    }

    public T pop() {
        if (top == null) {
            return null;
        } else {
            T v = top.data;
            Node<T> node = top.next;
            top.next = null;
            top = node;
            return v;
        }
    }

    public T peek() {
        return top == null ? null : top.data;
    }

    public boolean isEmpty() {
        return top == null;
    }

    @Override
    public String toString() {
        if (top == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        Node<T> n = top;
        while (n != null) {
            sb.append(n.data).append('\t');
            n = n.next;
        }
        return sb.toString();
    }
}
