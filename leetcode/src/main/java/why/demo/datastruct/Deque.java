package why.demo.datastruct;

public class Deque<T> {

    private Node<T> head;
    private Node<T> tail;

    public void addFirst(T t) {
        Node<T> node = new Node<>(t);
        if (head == null) {
            tail = node;
        } else {
            node.next = head;
            head.prev = node;
        }
        head = node;
    }

    public void addLast(T t) {
        Node<T> node = new Node<>(t);
        if (head == null) {
            head = node;
        } else {
            tail.next = node;
        }
        tail = node;
    }

    public T pollFirst() {
        if (head == null) {
            return null;
        }
        Node<T> node = head;
        head = node.next;
        head.prev = null;
        node.next = null;
        return node.data;
    }

    public T pollLast() {
        if (tail == null) {
            return null;
        }
        Node<T> node = tail;
        tail = node.prev;
        tail.next = null;
        node.prev = null;
        return node.data;
    }

    public T peekFirst() {
        return head == null ? null : head.data;
    }

    public T peekLast() {
        return tail == null ? null : tail.data;
    }


    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public String toString() {
        if (head == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        Node<T> n = head;
        while (n != null) {
            sb.append(n.data).append('\t');
            n = n.next;
        }
        return sb.toString();
    }
}
