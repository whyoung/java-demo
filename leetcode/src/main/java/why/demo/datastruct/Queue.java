package why.demo.datastruct;

public class Queue<T> {

    private Node<T> head;
    private Node<T> tail;

    public void add(T t) {
        Node<T> node = new Node<>(t);
        if (head == null) {
            head = node;
        } else {
            tail.next = node;
        }
        tail = node;
    }

    public T poll() {
        if (head == null) {
            return null;
        }
        Node<T> node = head;
        head = node.next;
        node.next = null;
        return node.data;
    }

    public T peek() {
        return head == null ? null : head.data;
    }

    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public String toString() {
        if (head == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        Node<T> n = head;
        while (n != null) {
            sb.append(n.data).append('\t');
            n = n.next;
        }
        return sb.toString();
    }
}
