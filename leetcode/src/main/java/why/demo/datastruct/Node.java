package why.demo.datastruct;


class Node<T> {
    protected final T data;
    protected Node<T> next;
    protected Node<T> prev;

    public Node(T t) {
        this.data = t;
    }
}
