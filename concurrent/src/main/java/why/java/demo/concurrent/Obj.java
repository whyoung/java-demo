package why.java.demo.concurrent;

import java.util.concurrent.atomic.AtomicInteger;

public class Obj {

    private static final AtomicInteger COUNTER = new AtomicInteger(1);

    private final int id;

    public Obj() {
        this.id = COUNTER.getAndIncrement();
    }

    @Override
    public String toString() {
        return "Obj#" + id;
    }
}
