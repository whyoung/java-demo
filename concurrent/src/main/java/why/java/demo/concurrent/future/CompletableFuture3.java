/*
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

/*
 *
 *
 *
 *
 *
 * Written by Doug Lea with assistance from members of JCP JSR-166
 * Expert Group and released to the public domain, as explained at
 * http://creativecommons.org/publicdomain/zero/1.0/
 */

package why.java.demo.concurrent.future;

import java.util.concurrent.Executor;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;

/**
 * 删除注释和无关代码
 */
public class CompletableFuture3<T> {
    volatile Object result;       // Either the result or boxed AltResult
    volatile Completion stack;    // Top of Treiber stack of dependent actions

    final boolean internalComplete(Object r) { // CAS from null to r
        return UNSAFE.compareAndSwapObject(this, RESULT, null, r);
    }

    final boolean casStack(Completion cmp, Completion val) {
        return UNSAFE.compareAndSwapObject(this, STACK, cmp, val);
    }

    /** Returns true if successfully pushed c onto stack. */
    final boolean tryPushStack(Completion c) {
        Completion h = stack;
        lazySetNext(c, h);
        return UNSAFE.compareAndSwapObject(this, STACK, h, c);
    }

    /** Unconditionally pushes c onto stack, retrying if necessary. */
    final void pushStack(Completion c) {
        do {} while (!tryPushStack(c));
    }


    // Modes for Completion.tryFire. Signedness matters.
    static final int SYNC   =  0;
    static final int ASYNC  =  1;
    static final int NESTED = -1;

    /* ------------- Async task preliminaries -------------- */
    public static interface AsynchronousCompletionTask {
    }

    private static final boolean useCommonPool =
            (ForkJoinPool.getCommonPoolParallelism() > 1);

    private static final Executor asyncPool = useCommonPool ?
            ForkJoinPool.commonPool() : new ThreadPerTaskExecutor();

    /** Fallback if ForkJoinPool.commonPool() cannot support parallelism */
    static final class ThreadPerTaskExecutor implements Executor {
        @Override
        public void execute(Runnable r) { new Thread(r).start(); }
    }

    static Executor screenExecutor(Executor e) {
        if (!useCommonPool && e == ForkJoinPool.commonPool()) {
            return asyncPool;
        }
        if (e == null) {
            throw new NullPointerException();
        }
        return e;
    }

    static void lazySetNext(Completion c, Completion next) {
        UNSAFE.putOrderedObject(c, NEXT, next);
    }

    final void postComplete() {
        CompletableFuture3<?> f = this;
        Completion h;
        /*
        对于进入 while 代码块的两个条件：
        1.(h = f.stack) != null
        2.(f != this && (h = (f = this).stack) != null)，如果 f 不是 this对象，再将f设置成当前对象

        第一次循环时，条件2一定是 false，只能是 this.stack != null才能进入while代码

         */
        while ((h = f.stack) != null ||
                (f != this && (h = (f = this).stack) != null)) {
            CompletableFuture3<?> d;
            Completion t;
            //弹出栈顶元素
            if (f.casStack(h, t = h.next)) {
                //如果栈不为空
                if (t != null) {
                    if (f != this) {
                        //其他栈的栈顶元素压如当前栈的栈顶，再继续处理其他栈的元素
                        pushStack(h);
                        continue;
                    }
                    h.next = null;    // detach
                }
                f = (d = h.tryFire(NESTED)) == null ? this : d;
            }
        }
    }

    /** Traverses stack and unlinks dead Completions. */
    final void cleanStack() {
        for (Completion p = null, q = stack; q != null;) {
            Completion s = q.next;
            if (q.isLive()) {
                p = q;
                q = s; // q = q.next
            }
            else if (p == null) {
                // p == null 表示栈顶元素 isLive 是 false，将栈顶元素的next作为栈顶，即弹出旧的栈顶元素
                casStack(q, s);
                q = stack;
            }
            else {
                // 栈中间某个Completion的 isLive 返回 false
                /*
                 当前的关系：
                 p.next = q
                 q.next = s
                 p.next = s 表示将 q从stack链中移除
                 */
                p.next = s;
                if (p.isLive()) {
                    q = s;
                } else {
                    p = null;  // restart，存在并发操作，重新开始清理
                    q = stack;
                }
            }
        }
    }

    final CompletableFuture3<T> postFire(CompletableFuture3<?> a,
                                         CompletableFuture3<?> b, int mode) {
        if (b != null && b.stack != null) { // clean second source
            if (mode < 0 || b.result == null) {
                b.cleanStack();
            } else {
                b.postComplete();
            }
        }
        return postFire(a, mode);
    }

    final CompletableFuture3<T> postFire(CompletableFuture3<?> a, int mode) {
        if (a != null && a.stack != null) {
            if (mode < 0 || a.result == null) {
                a.cleanStack();
            } else {
                a.postComplete();
            }
        }
        if (result != null && stack != null) {
            if (mode < 0) {
                return this;
            } else {
                postComplete();
            }
        }
        return null;
    }

    /* ------------- Base Completion classes and operations -------------- */

    @SuppressWarnings("serial")
    abstract static class Completion extends ForkJoinTask<Void>
            implements Runnable, AsynchronousCompletionTask {
        volatile Completion next;      // Treiber stack link

        abstract CompletableFuture3<?> tryFire(int mode);

        abstract boolean isLive();

        @Override
        public final void run()                { tryFire(ASYNC); }
        @Override
        public final boolean exec()            { tryFire(ASYNC); return true; }
        @Override
        public final Void getRawResult()       { return null; }
        @Override
        public final void setRawResult(Void v) {}
    }


    @SuppressWarnings("serial")
    abstract static class UniCompletion<T,V> extends CompletableFuture4.Completion {
        Executor executor;                 // executor to use (null if none)
        CompletableFuture4<V> dep;          // 依赖的待完成任务，关联和CompletableFuture的关系
        CompletableFuture4<T> src;          // 动作的源（动作的发起者，dep的前置stage）

        UniCompletion(Executor executor, CompletableFuture4<V> dep,
                      CompletableFuture4<T> src) {
            this.executor = executor; this.dep = dep; this.src = src;
        }

        final boolean claim() {
            Executor e = executor;
            if (compareAndSetForkJoinTaskTag((short)0, (short)1)) {
                if (e == null) {
                    return true;
                }
                executor = null; // disable
                e.execute(this);
            }
            return false;
        }

        //如果没有依赖，表明当前任务已经结束
        @Override
        final boolean isLive() { return dep != null; }
    }

    /* ------------- public methods -------------- */

    public CompletableFuture3() {
    }

    private CompletableFuture3(Object r) {
        this.result = r;
    }


    // Unsafe mechanics
    private static final sun.misc.Unsafe UNSAFE;
    private static final long RESULT;
    private static final long STACK;
    private static final long NEXT;
    static {
        try {
            final sun.misc.Unsafe u;
            UNSAFE = u = sun.misc.Unsafe.getUnsafe();
            Class<?> k = CompletableFuture3.class;
            RESULT = u.objectFieldOffset(k.getDeclaredField("result"));
            STACK = u.objectFieldOffset(k.getDeclaredField("stack"));
            NEXT = u.objectFieldOffset
                (Completion.class.getDeclaredField("next"));
        } catch (Exception x) {
            throw new Error(x);
        }
    }
}