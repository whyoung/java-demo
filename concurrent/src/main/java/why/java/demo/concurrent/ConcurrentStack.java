package why.java.demo.concurrent;

import java.util.concurrent.atomic.AtomicReference;

/**
 * Treiber stack
 * @author apple
 * @param <T> T
 */
public class ConcurrentStack<T> {

    private final AtomicReference<Node<T>> top = new AtomicReference<>();

    public boolean push(T t) {
        Node<T> n = new Node<>(t);
        Node<T> old = top.get();
        if (top.compareAndSet(old, n)) {
            n.next = old;
            return true;
        }
        return false;
    }

    public void tryPush(T t) {
        Node<T> n = new Node<>(t);
        Node<T> old = top.get();
        while (!top.compareAndSet(old, n)) {
            old = top.get();
        }
        n.next = old;
    }

    public T pop() {
        Node<T> old = top.get();
        while (old != null && !top.compareAndSet(old, old.next)) {
            old = top.get();
        }
        if (old != null) {
            return old.data;
        }
        return null;
    }

    private static class Node<T> {
        Node(T data) {
            this.data = data;
        }

        private final T data;
        private Node<T> next;
    }
}
