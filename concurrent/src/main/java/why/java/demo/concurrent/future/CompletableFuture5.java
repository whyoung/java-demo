/*
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

/*
 *
 *
 *
 *
 *
 * Written by Doug Lea with assistance from members of JCP JSR-166
 * Expert Group and released to the public domain, as explained at
 * http://creativecommons.org/publicdomain/zero/1.0/
 */

package why.java.demo.concurrent.future;

import java.util.concurrent.CompletionStage;
import java.util.concurrent.Executor;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.locks.LockSupport;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class CompletableFuture5<T> {
    volatile Object result;       // Either the result or boxed AltResult
    volatile Completion stack;    // Top of Treiber stack of dependent actions

    /* ------------- Async task preliminaries -------------- */
    public static interface AsynchronousCompletionTask {
    }

    private static final boolean useCommonPool =
        (ForkJoinPool.getCommonPoolParallelism() > 1);

    private static final Executor asyncPool = useCommonPool ?
        ForkJoinPool.commonPool() : new ThreadPerTaskExecutor();

    /** Fallback if ForkJoinPool.commonPool() cannot support parallelism */
    static final class ThreadPerTaskExecutor implements Executor {
        @Override
        public void execute(Runnable r) { new Thread(r).start(); }
    }



    // Modes for Completion.tryFire. Signedness matters.
    static final int SYNC   =  0;
    static final int ASYNC  =  1;
    static final int NESTED = -1;

    /* ------------- Base Completion classes and operations -------------- */

    @SuppressWarnings("serial")
    abstract static class Completion extends ForkJoinTask<Void>
        implements Runnable, AsynchronousCompletionTask {
        volatile Completion next;      // Treiber stack link

        abstract CompletableFuture5<?> tryFire(int mode);

        abstract boolean isLive();

        @Override
        public final void run()                { tryFire(ASYNC); }
        @Override
        public final boolean exec()            { tryFire(ASYNC); return true; }
        @Override
        public final Void getRawResult()       { return null; }
        @Override
        public final void setRawResult(Void v) {}
    }

    /* ------------- One-input Completions -------------- */

    @SuppressWarnings("serial")
    abstract static class UniCompletion<T,V> extends Completion {
        Executor executor;                 // executor to use (null if none)
        CompletableFuture5<V> dep;          // the dependent to complete，关联和CompletableFuture的关系
        CompletableFuture5<T> src;          // 动作的源（动作的发起者，dep的前置stage）

        UniCompletion(Executor executor, CompletableFuture5<V> dep,
                      CompletableFuture5<T> src) {
            this.executor = executor; this.dep = dep; this.src = src;
        }

        final boolean claim() {
            Executor e = executor;
            if (compareAndSetForkJoinTaskTag((short)0, (short)1)) {
                if (e == null) {
                    return true;
                }
                executor = null; // disable
                e.execute(this);
            }
            return false;
        }

        @Override
        final boolean isLive() { return dep != null; }
    }

    @SuppressWarnings("serial")
    static final class UniApply<T,V> extends UniCompletion<T,V> {
        Function<? super T,? extends V> fn;
        UniApply(Executor executor, CompletableFuture5<V> dep,
                 CompletableFuture5<T> src,
                 Function<? super T,? extends V> fn) {
            super(executor, dep, src);
            this.fn = fn;
        }
        @Override
        final CompletableFuture5<V> tryFire(int mode) {
            CompletableFuture5<V> d;
            CompletableFuture5<T> a;
            //mode = 0表示同步执行，> 0 表示异步执行
            if ((d = dep) == null ||
                !d.uniApply(a = src, fn, mode > 0 ? null : this)) {
                return null;
            }
            dep = null; src = null; fn = null;
            return d.postFire(a, mode);
        }
    }

    @SuppressWarnings("serial")
    static final class UniAccept<T> extends UniCompletion<T,Void> {
        Consumer<? super T> fn;
        UniAccept(Executor executor, CompletableFuture5<Void> dep,
                  CompletableFuture5<T> src, Consumer<? super T> fn) {
            super(executor, dep, src); this.fn = fn;
        }
        @Override
        final CompletableFuture5<Void> tryFire(int mode) {
            CompletableFuture5<Void> d; CompletableFuture5<T> a;
            if ((d = dep) == null ||
                !d.uniAccept(a = src, fn, mode > 0 ? null : this)) {
                return null;
            }
            dep = null; src = null; fn = null;
            return d.postFire(a, mode);
        }
    }

    @SuppressWarnings("serial")
    static final class UniRun<T> extends UniCompletion<T,Void> {
        Runnable fn;
        UniRun(Executor executor, CompletableFuture5<Void> dep,
               CompletableFuture5<T> src, Runnable fn) {
            super(executor, dep, src); this.fn = fn;
        }
        @Override
        final CompletableFuture5<Void> tryFire(int mode) {
            CompletableFuture5<Void> d; CompletableFuture5<T> a;
            if ((d = dep) == null ||
                !d.uniRun(a = src, fn, mode > 0 ? null : this)) {
                return null;
            }
            dep = null; src = null; fn = null;
            return d.postFire(a, mode);
        }
    }

    @SuppressWarnings("serial")
    static final class UniWhenComplete<T> extends UniCompletion<T,T> {
        BiConsumer<? super T, ? super Throwable> fn;
        UniWhenComplete(Executor executor, CompletableFuture5<T> dep,
                        CompletableFuture5<T> src,
                        BiConsumer<? super T, ? super Throwable> fn) {
            super(executor, dep, src); this.fn = fn;
        }
        @Override
        final CompletableFuture5<T> tryFire(int mode) {
            CompletableFuture5<T> d; CompletableFuture5<T> a;
            if ((d = dep) == null ||
                !d.uniWhenComplete(a = src, fn, mode > 0 ? null : this)) {
                return null;
            }
            dep = null; src = null; fn = null;
            return d.postFire(a, mode);
        }
    }

    @SuppressWarnings("serial")
    static final class UniHandle<T,V> extends UniCompletion<T,V> {
        BiFunction<? super T, Throwable, ? extends V> fn;
        UniHandle(Executor executor, CompletableFuture5<V> dep,
                  CompletableFuture5<T> src,
                  BiFunction<? super T, Throwable, ? extends V> fn) {
            super(executor, dep, src); this.fn = fn;
        }
        @Override
        final CompletableFuture5<V> tryFire(int mode) {
            CompletableFuture5<V> d; CompletableFuture5<T> a;
            if ((d = dep) == null ||
                !d.uniHandle(a = src, fn, mode > 0 ? null : this)) {
                return null;
            }
            dep = null; src = null; fn = null;
            return d.postFire(a, mode);
        }
    }

    @SuppressWarnings("serial")
    static final class UniExceptionally<T> extends UniCompletion<T,T> {
        Function<? super Throwable, ? extends T> fn;
        UniExceptionally(CompletableFuture5<T> dep, CompletableFuture5<T> src,
                         Function<? super Throwable, ? extends T> fn) {
            super(null, dep, src); this.fn = fn;
        }
        @Override
        final CompletableFuture5<T> tryFire(int mode) { // never ASYNC
            // assert mode != ASYNC;
            CompletableFuture5<T> d; CompletableFuture5<T> a;
            if ((d = dep) == null || !d.uniExceptionally(a = src, fn, this)) {
                return null;
            }
            dep = null; src = null; fn = null;
            return d.postFire(a, mode);
        }
    }

    @SuppressWarnings("serial")
    static final class UniRelay<T> extends UniCompletion<T,T> { // for Compose
        UniRelay(CompletableFuture5<T> dep, CompletableFuture5<T> src) {
            super(null, dep, src);
        }
        @Override
        final CompletableFuture5<T> tryFire(int mode) {
            CompletableFuture5<T> d; CompletableFuture5<T> a;
            if ((d = dep) == null || !d.uniRelay(a = src)) {
                return null;
            }
            src = null; dep = null;
            return d.postFire(a, mode);
        }
    }

    @SuppressWarnings("serial")
    static final class UniCompose<T,V> extends UniCompletion<T,V> {
        Function<? super T, ? extends CompletionStage<V>> fn;
        UniCompose(Executor executor, CompletableFuture5<V> dep,
                   CompletableFuture5<T> src,
                   Function<? super T, ? extends CompletionStage<V>> fn) {
            super(executor, dep, src); this.fn = fn;
        }
        @Override
        final CompletableFuture5<V> tryFire(int mode) {
            CompletableFuture5<V> d; CompletableFuture5<T> a;
            if ((d = dep) == null ||
                !d.uniCompose(a = src, fn, mode > 0 ? null : this)) {
                return null;
            }
            dep = null; src = null; fn = null;
            return d.postFire(a, mode);
        }
    }

    /* ------------- 两个输入的 Completions -------------- */
    @SuppressWarnings("serial")
    abstract static class BiCompletion<T,U,V> extends UniCompletion<T,V> {
        CompletableFuture5<U> snd; // second source for action
        BiCompletion(Executor executor, CompletableFuture5<V> dep,
                     CompletableFuture5<T> src, CompletableFuture5<U> snd) {
            super(executor, dep, src); this.snd = snd;
        }
    }

    /** BiCompletion的一个代理 */
    @SuppressWarnings("serial")
    static final class CoCompletion extends Completion {
        BiCompletion<?,?,?> base;
        CoCompletion(BiCompletion<?,?,?> base) { this.base = base; }
        @Override
        final CompletableFuture5<?> tryFire(int mode) {
            BiCompletion<?,?,?> c; CompletableFuture5<?> d;
            if ((c = base) == null || (d = c.tryFire(mode)) == null) {
                return null;
            }
            base = null; // detach
            return d;
        }
        @Override
        final boolean isLive() {
            BiCompletion<?,?,?> c;
            return (c = base) != null && c.dep != null;
        }
    }

    @SuppressWarnings("serial")
    static final class BiApply<T,U,V> extends BiCompletion<T,U,V> {
        BiFunction<? super T,? super U,? extends V> fn;
        BiApply(Executor executor, CompletableFuture5<V> dep,
                CompletableFuture5<T> src, CompletableFuture5<U> snd,
                BiFunction<? super T,? super U,? extends V> fn) {
            super(executor, dep, src, snd); this.fn = fn;
        }
        @Override
        final CompletableFuture5<V> tryFire(int mode) {
            CompletableFuture5<V> d;
            CompletableFuture5<T> a;
            CompletableFuture5<U> b;
            if ((d = dep) == null ||
                !d.biApply(a = src, b = snd, fn, mode > 0 ? null : this)) {
                return null;
            }
            dep = null; src = null; snd = null; fn = null;
            return d.postFire(a, b, mode);
        }
    }

    @SuppressWarnings("serial")
    static final class BiAccept<T,U> extends BiCompletion<T,U,Void> {
        BiConsumer<? super T,? super U> fn;
        BiAccept(Executor executor, CompletableFuture5<Void> dep,
                 CompletableFuture5<T> src, CompletableFuture5<U> snd,
                 BiConsumer<? super T,? super U> fn) {
            super(executor, dep, src, snd); this.fn = fn;
        }
        @Override
        final CompletableFuture5<Void> tryFire(int mode) {
            CompletableFuture5<Void> d;
            CompletableFuture5<T> a;
            CompletableFuture5<U> b;
            if ((d = dep) == null ||
                !d.biAccept(a = src, b = snd, fn, mode > 0 ? null : this)) {
                return null;
            }
            dep = null; src = null; snd = null; fn = null;
            return d.postFire(a, b, mode);
        }
    }

    @SuppressWarnings("serial")
    static final class BiRun<T,U> extends BiCompletion<T,U,Void> {
        Runnable fn;
        BiRun(Executor executor, CompletableFuture5<Void> dep,
              CompletableFuture5<T> src,
              CompletableFuture5<U> snd,
              Runnable fn) {
            super(executor, dep, src, snd); this.fn = fn;
        }
        @Override
        final CompletableFuture5<Void> tryFire(int mode) {
            CompletableFuture5<Void> d;
            CompletableFuture5<T> a;
            CompletableFuture5<U> b;
            if ((d = dep) == null ||
                !d.biRun(a = src, b = snd, fn, mode > 0 ? null : this)) {
                return null;
            }
            dep = null; src = null; snd = null; fn = null;
            return d.postFire(a, b, mode);
        }
    }

    @SuppressWarnings("serial")
    static final class BiRelay<T,U> extends BiCompletion<T,U,Void> { // for And
        BiRelay(CompletableFuture5<Void> dep,
                CompletableFuture5<T> src,
                CompletableFuture5<U> snd) {
            super(null, dep, src, snd);
        }
        @Override
        final CompletableFuture5<Void> tryFire(int mode) {
            CompletableFuture5<Void> d;
            CompletableFuture5<T> a;
            CompletableFuture5<U> b;
            if ((d = dep) == null || !d.biRelay(a = src, b = snd)) {
                return null;
            }
            src = null; snd = null; dep = null;
            return d.postFire(a, b, mode);
        }
    }

    @SuppressWarnings("serial")
    static final class OrApply<T,U extends T,V> extends BiCompletion<T,U,V> {
        Function<? super T,? extends V> fn;
        OrApply(Executor executor, CompletableFuture5<V> dep,
                CompletableFuture5<T> src,
                CompletableFuture5<U> snd,
                Function<? super T,? extends V> fn) {
            super(executor, dep, src, snd); this.fn = fn;
        }
        @Override
        final CompletableFuture5<V> tryFire(int mode) {
            CompletableFuture5<V> d;
            CompletableFuture5<T> a;
            CompletableFuture5<U> b;
            if ((d = dep) == null ||
                !d.orApply(a = src, b = snd, fn, mode > 0 ? null : this)) {
                return null;
            }
            dep = null; src = null; snd = null; fn = null;
            return d.postFire(a, b, mode);
        }
    }

    @SuppressWarnings("serial")
    static final class OrAccept<T,U extends T> extends BiCompletion<T,U,Void> {
        Consumer<? super T> fn;
        OrAccept(Executor executor, CompletableFuture5<Void> dep,
                 CompletableFuture5<T> src,
                 CompletableFuture5<U> snd,
                 Consumer<? super T> fn) {
            super(executor, dep, src, snd); this.fn = fn;
        }
        @Override
        final CompletableFuture5<Void> tryFire(int mode) {
            CompletableFuture5<Void> d;
            CompletableFuture5<T> a;
            CompletableFuture5<U> b;
            if ((d = dep) == null ||
                !d.orAccept(a = src, b = snd, fn, mode > 0 ? null : this)) {
                return null;
            }
            dep = null; src = null; snd = null; fn = null;
            return d.postFire(a, b, mode);
        }
    }

    @SuppressWarnings("serial")
    static final class OrRun<T,U> extends BiCompletion<T,U,Void> {
        Runnable fn;
        OrRun(Executor executor, CompletableFuture5<Void> dep,
              CompletableFuture5<T> src,
              CompletableFuture5<U> snd,
              Runnable fn) {
            super(executor, dep, src, snd); this.fn = fn;
        }
        @Override
        final CompletableFuture5<Void> tryFire(int mode) {
            CompletableFuture5<Void> d;
            CompletableFuture5<T> a;
            CompletableFuture5<U> b;
            if ((d = dep) == null ||
                !d.orRun(a = src, b = snd, fn, mode > 0 ? null : this)) {
                return null;
            }
            dep = null; src = null; snd = null; fn = null;
            return d.postFire(a, b, mode);
        }
    }


    @SuppressWarnings("serial")
    static final class OrRelay<T,U> extends BiCompletion<T,U,Object> { // for Or
        OrRelay(CompletableFuture5<Object> dep, CompletableFuture5<T> src,
                CompletableFuture5<U> snd) {
            super(null, dep, src, snd);
        }
        @Override
        final CompletableFuture5<Object> tryFire(int mode) {
            CompletableFuture5<Object> d;
            CompletableFuture5<T> a;
            CompletableFuture5<U> b;
            if ((d = dep) == null || !d.orRelay(a = src, b = snd)) {
                return null;
            }
            src = null; snd = null; dep = null;
            return d.postFire(a, b, mode);
        }
    }

    /* ------------- Zero-input Async forms -------------- */

    @SuppressWarnings("serial")
    static final class AsyncSupply<T> extends ForkJoinTask<Void>
            implements Runnable, AsynchronousCompletionTask {

        CompletableFuture5<T> dep;
        Supplier<T> fn;
        AsyncSupply(CompletableFuture5<T> dep, Supplier<T> fn) {
            // 关联 Runnable 和 Future
            // Supplier 是构成 Runnable run的逻辑
            this.dep = dep;
            this.fn = fn;
        }

        @Override
        public final Void getRawResult() { return null; }
        @Override
        public final void setRawResult(Void v) {}
        @Override
        public final boolean exec() { run(); return true; }

        @Override
        public void run() {
            CompletableFuture5<T> d;
            Supplier<T> f;
            if ((d = dep) != null && (f = fn) != null) {
                dep = null;
                fn = null;
                if (d.result == null) {
                    try {
                        //设置执行完成
                        d.completeValue(f.get());
                    } catch (Throwable ex) {
                        d.completeThrowable(ex);
                    }
                }
                //通知后置的Stage，当前的 Stage 已经完成
                d.postComplete();
            }
        }
    }

    @SuppressWarnings("serial")
    static final class AsyncRun extends ForkJoinTask<Void>
            implements Runnable, AsynchronousCompletionTask {
        CompletableFuture5<Void> dep; Runnable fn;
        AsyncRun(CompletableFuture5<Void> dep, Runnable fn) {
            this.dep = dep; this.fn = fn;
        }

        @Override
        public final Void getRawResult() { return null; }
        @Override
        public final void setRawResult(Void v) {}
        @Override
        public final boolean exec() { run(); return true; }

        @Override
        public void run() {
            CompletableFuture5<Void> d; Runnable f;
            if ((d = dep) != null && (f = fn) != null) {
                dep = null; fn = null;
                if (d.result == null) {
                    try {
                        f.run();
                        d.completeNull();
                    } catch (Throwable ex) {
                        d.completeThrowable(ex);
                    }
                }
                d.postComplete();
            }
        }
    }

    /* ------------- Signallers -------------- */

    @SuppressWarnings("serial")
    static final class Signaller extends Completion
        implements ForkJoinPool.ManagedBlocker {
        long nanos;                    // wait time if timed
        final long deadline;           // non-zero if timed
        volatile int interruptControl; // > 0: interruptible, < 0: interrupted
        volatile Thread thread;

        Signaller(boolean interruptible, long nanos, long deadline) {
            this.thread = Thread.currentThread();
            this.interruptControl = interruptible ? 1 : 0;
            this.nanos = nanos;
            this.deadline = deadline;
        }
        @Override
        final CompletableFuture5<?> tryFire(int ignore) {
            Thread w; // no need to atomically claim
            if ((w = thread) != null) {
                thread = null;
                LockSupport.unpark(w);
            }
            return null;
        }
        @Override
        public boolean isReleasable() {
            if (thread == null) {
                return true;
            }
            if (Thread.interrupted()) {
                int i = interruptControl;
                interruptControl = -1;
                if (i > 0) {
                    return true;
                }
            }
            if (deadline != 0L &&
                (nanos <= 0L || (nanos = deadline - System.nanoTime()) <= 0L)) {
                thread = null;
                return true;
            }
            return false;
        }
        @Override
        public boolean block() {
            if (isReleasable()) {
                return true;
            } else if (deadline == 0L) {
                LockSupport.park(this);
            } else {
                if (nanos <= 0L) {
                    return isReleasable();
                }
                LockSupport.parkNanos(this, nanos);
            }
            return isReleasable();
        }
        @Override
        final boolean isLive() { return thread != null; }
    }


    private T completeNull() {
        return null;
    }

    private void completeThrowable(Throwable ex) {
    }

    private void postComplete() {
    }

    private void completeValue(T t) {
    }

    private boolean orRelay(Object var1, Object var2) {
        return false;
    }

    private <T, U> boolean orRun(Object var1, Object var2, Object var3, Object var4) {
        return false;
    }

    private <T, V> boolean uniApply(Object var1, Object var2, Object var3) {
        return false;
    }

    final CompletableFuture5<T> postFire(Object var1, Object var2, Object var3) {
        return null;
    }

    final CompletableFuture5<T> postFire(Object var1, Object var2) {
        return null;
    }

    private <T> boolean uniAccept(Object var1, Object var2, Object var3) {
        return false;
    }

    private <T, U extends T> boolean orAccept(Object var1, Object var2, Object var3, Object var4) {
        return false;
    }

    private <T, U> boolean biRelay(Object var1, Object var2) {
        return false;
    }

    private <T, U extends T> boolean orApply(Object var1, Object var2, Object var3, Object var4) {
        return false;
    }

    private <T, U> boolean biRun(Object var1, Object var2, Object var3) {
        return false;
    }

    private <T, U> boolean biAccept(Object var1, Object var2, Object var3, Object var4) {
        return false;
    }

    private <T, U> boolean biApply(Object var1, Object var2, Object var3, Object var4) {
        return false;
    }

    private <T, V> boolean uniCompose(Object var1, Object var2, Object var3) {
        return false;
    }

    private boolean uniRelay(Object var1) {
        return false;
    }

    private <T> boolean uniExceptionally(Object var1, Object var2, Object var3) {
        return false;
    }

    private <T, V> boolean uniHandle(Object var1, Object var2, Object var3) {
        return false;
    }

    private <T> boolean uniRun(Object var1, Object var2, Object var3) {
        return false;
    }

    private <T> boolean uniWhenComplete(Object var1, Object var2, Object var3) {
        return false;
    }

    private <T, U> boolean biRun(Object var1, Object var2, Object var3, Object var4) {
        return false;
    }
    /* ------------- public methods -------------- */

    // Unsafe mechanics
    private static final sun.misc.Unsafe UNSAFE;
    private static final long RESULT;
    private static final long STACK;
    private static final long NEXT;
    static {
        try {
            final sun.misc.Unsafe u;
            UNSAFE = u = sun.misc.Unsafe.getUnsafe();
            Class<?> k = CompletableFuture5.class;
            RESULT = u.objectFieldOffset(k.getDeclaredField("result"));
            STACK = u.objectFieldOffset(k.getDeclaredField("stack"));
            NEXT = u.objectFieldOffset
                (Completion.class.getDeclaredField("next"));
        } catch (Exception x) {
            throw new Error(x);
        }
    }
}