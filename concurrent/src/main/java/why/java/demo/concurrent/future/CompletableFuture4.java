/*
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

/*
 *
 *
 *
 *
 *
 * Written by Doug Lea with assistance from members of JCP JSR-166
 * Expert Group and released to the public domain, as explained at
 * http://creativecommons.org/publicdomain/zero/1.0/
 */

package why.java.demo.concurrent.future;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Executor;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.locks.LockSupport;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class CompletableFuture4<T> {
    volatile Object result;       // Either the result or boxed AltResult
    volatile Completion stack;    // Top of Treiber stack of dependent actions

    // Modes for Completion.tryFire. Signedness matters.
    static final int SYNC   =  0;
    static final int ASYNC  =  1;
    static final int NESTED = -1;

    /* ------------- Base Completion classes and operations -------------- */

    @SuppressWarnings("serial")
    abstract static class Completion extends ForkJoinTask<Void>
            implements Runnable {
        volatile Completion next;      // Treiber stack link

        abstract CompletableFuture4<?> tryFire(int mode);

        abstract boolean isLive();

        @Override
        public final void run()                { tryFire(ASYNC); }
        @Override
        public final boolean exec()            { tryFire(ASYNC); return true; }
        @Override
        public final Void getRawResult()       { return null; }
        @Override
        public final void setRawResult(Void v) {}
    }

    /* ------------- One-input Completions -------------- */

    @SuppressWarnings("serial")
    abstract static class UniCompletion<T,V> extends Completion {
        Executor executor;                 // executor to use (null if none)
        CompletableFuture4<V> dep;          // 依赖的待完成任务，关联和CompletableFuture的关系
        CompletableFuture4<T> src;          // 动作的源（动作的发起者，dep的前置stage）

        UniCompletion(Executor executor, CompletableFuture4<V> dep,
                      CompletableFuture4<T> src) {
            this.executor = executor; this.dep = dep; this.src = src;
        }

        final boolean claim() {
            Executor e = executor;
            if (compareAndSetForkJoinTaskTag((short)0, (short)1)) {
                if (e == null) {
                    return true;
                }
                executor = null; // disable
                e.execute(this);
            }
            return false;
        }

        //如果没有依赖，表明当前任务已经结束
        @Override
        final boolean isLive() { return dep != null; }
    }

    @SuppressWarnings("serial")
    static final class UniApply<T,V> extends UniCompletion<T,V> {
        Function<? super T,? extends V> fn;
        UniApply(Executor executor, CompletableFuture4<V> dep,
                 CompletableFuture4<T> src,
                 Function<? super T,? extends V> fn) {
            super(executor, dep, src);
            this.fn = fn;
        }
        @Override
        final CompletableFuture4<V> tryFire(int mode) {
            CompletableFuture4<V> d;
            CompletableFuture4<T> a;
            //mode = 0表示同步执行，> 0 表示异步执行
            if ((d = dep) == null ||
                    !d.uniApply(a = src, fn, mode > 0 ? null : this)) {
                return null;
            }
            dep = null; src = null; fn = null;
            return d.postFire(a, mode);
        }
    }

    @SuppressWarnings("serial")
    static final class UniAccept<T> extends UniCompletion<T,Void> {
        Consumer<? super T> fn;
        UniAccept(Executor executor, CompletableFuture4<Void> dep,
                  CompletableFuture4<T> src, Consumer<? super T> fn) {
            super(executor, dep, src); this.fn = fn;
        }
        @Override
        final CompletableFuture4<Void> tryFire(int mode) {
            CompletableFuture4<Void> d; CompletableFuture4<T> a;
            if ((d = dep) == null ||
                    !d.uniAccept(a = src, fn, mode > 0 ? null : this)) {
                return null;
            }
            dep = null; src = null; fn = null;
            return d.postFire(a, mode);
        }
    }

    @SuppressWarnings("serial")
    static final class UniRun<T> extends UniCompletion<T,Void> {
        Runnable fn;
        UniRun(Executor executor, CompletableFuture4<Void> dep,
               CompletableFuture4<T> src, Runnable fn) {
            super(executor, dep, src); this.fn = fn;
        }
        @Override
        final CompletableFuture4<Void> tryFire(int mode) {
            CompletableFuture4<Void> d;
            CompletableFuture4<T> a;
            if ((d = dep) == null ||
                    !d.uniRun(a = src, fn, mode > 0 ? null : this)) {
                //已同步方式执行，如果执行成功，就直接返回
                return null;
            }
            dep = null; src = null; fn = null;
            //调用 postFire 方法，
            return d.postFire(a, mode);
        }
    }

    @SuppressWarnings("serial")
    static final class UniWhenComplete<T> extends UniCompletion<T,T> {
        BiConsumer<? super T, ? super Throwable> fn;
        UniWhenComplete(Executor executor, CompletableFuture4<T> dep,
                        CompletableFuture4<T> src,
                        BiConsumer<? super T, ? super Throwable> fn) {
            super(executor, dep, src); this.fn = fn;
        }
        @Override
        final CompletableFuture4<T> tryFire(int mode) {
            CompletableFuture4<T> d; CompletableFuture4<T> a;
            if ((d = dep) == null ||
                    !d.uniWhenComplete(a = src, fn, mode > 0 ? null : this)) {
                return null;
            }
            dep = null; src = null; fn = null;
            return d.postFire(a, mode);
        }
    }

    @SuppressWarnings("serial")
    static final class UniHandle<T,V> extends UniCompletion<T,V> {
        BiFunction<? super T, Throwable, ? extends V> fn;
        UniHandle(Executor executor, CompletableFuture4<V> dep,
                  CompletableFuture4<T> src,
                  BiFunction<? super T, Throwable, ? extends V> fn) {
            super(executor, dep, src); this.fn = fn;
        }
        @Override
        final CompletableFuture4<V> tryFire(int mode) {
            CompletableFuture4<V> d; CompletableFuture4<T> a;
            if ((d = dep) == null ||
                    !d.uniHandle(a = src, fn, mode > 0 ? null : this)) {
                return null;
            }
            dep = null; src = null; fn = null;
            return d.postFire(a, mode);
        }
    }

    @SuppressWarnings("serial")
    static final class UniExceptionally<T> extends UniCompletion<T,T> {
        Function<? super Throwable, ? extends T> fn;
        UniExceptionally(CompletableFuture4<T> dep, CompletableFuture4<T> src,
                         Function<? super Throwable, ? extends T> fn) {
            super(null, dep, src); this.fn = fn;
        }
        @Override
        final CompletableFuture4<T> tryFire(int mode) { // never ASYNC
            // assert mode != ASYNC;
            CompletableFuture4<T> d; CompletableFuture4<T> a;
            if ((d = dep) == null || !d.uniExceptionally(a = src, fn, this)) {
                return null;
            }
            dep = null; src = null; fn = null;
            return d.postFire(a, mode);
        }
    }

    @SuppressWarnings("serial")
    static final class UniRelay<T> extends UniCompletion<T,T> { // for Compose
        UniRelay(CompletableFuture4<T> dep, CompletableFuture4<T> src) {
            super(null, dep, src);
        }
        @Override
        final CompletableFuture4<T> tryFire(int mode) {
            CompletableFuture4<T> d; CompletableFuture4<T> a;
            if ((d = dep) == null || !d.uniRelay(a = src)) {
                return null;
            }
            src = null; dep = null;
            return d.postFire(a, mode);
        }
    }

    @SuppressWarnings("serial")
    static final class UniCompose<T,V> extends UniCompletion<T,V> {
        Function<? super T, ? extends CompletionStage<V>> fn;
        UniCompose(Executor executor, CompletableFuture4<V> dep,
                   CompletableFuture4<T> src,
                   Function<? super T, ? extends CompletionStage<V>> fn) {
            super(executor, dep, src); this.fn = fn;
        }
        @Override
        final CompletableFuture4<V> tryFire(int mode) {
            CompletableFuture4<V> d; CompletableFuture4<T> a;
            if ((d = dep) == null ||
                    !d.uniCompose(a = src, fn, mode > 0 ? null : this)) {
                return null;
            }
            dep = null; src = null; fn = null;
            return d.postFire(a, mode);
        }
    }

    /* ------------- 两个输入的 Completions -------------- */
    @SuppressWarnings("serial")
    abstract static class BiCompletion<T,U,V> extends UniCompletion<T,V> {
        CompletableFuture4<U> snd; // second source for action
        BiCompletion(Executor executor, CompletableFuture4<V> dep,
                     CompletableFuture4<T> src, CompletableFuture4<U> snd) {
            super(executor, dep, src); this.snd = snd;
        }
    }

    @SuppressWarnings("serial")
    static final class BiApply<T,U,V> extends BiCompletion<T,U,V> {
        BiFunction<? super T,? super U,? extends V> fn;
        BiApply(Executor executor, CompletableFuture4<V> dep,
                CompletableFuture4<T> src, CompletableFuture4<U> snd,
                BiFunction<? super T,? super U,? extends V> fn) {
            super(executor, dep, src, snd); this.fn = fn;
        }
        @Override
        final CompletableFuture4<V> tryFire(int mode) {
            CompletableFuture4<V> d;
            CompletableFuture4<T> a;
            CompletableFuture4<U> b;
            if ((d = dep) == null ||
                    !d.biApply(a = src, b = snd, fn, mode > 0 ? null : this)) {
                return null;
            }
            dep = null; src = null; snd = null; fn = null;
            return d.postFire(a, b, mode);
        }
    }

    @SuppressWarnings("serial")
    static final class BiAccept<T,U> extends BiCompletion<T,U,Void> {
        BiConsumer<? super T,? super U> fn;
        BiAccept(Executor executor, CompletableFuture4<Void> dep,
                 CompletableFuture4<T> src, CompletableFuture4<U> snd,
                 BiConsumer<? super T,? super U> fn) {
            super(executor, dep, src, snd); this.fn = fn;
        }
        @Override
        final CompletableFuture4<Void> tryFire(int mode) {
            CompletableFuture4<Void> d;
            CompletableFuture4<T> a;
            CompletableFuture4<U> b;
            if ((d = dep) == null ||
                    !d.biAccept(a = src, b = snd, fn, mode > 0 ? null : this)) {
                return null;
            }
            dep = null; src = null; snd = null; fn = null;
            return d.postFire(a, b, mode);
        }
    }

    @SuppressWarnings("serial")
    static final class BiRun<T,U> extends BiCompletion<T,U,Void> {
        Runnable fn;
        BiRun(Executor executor, CompletableFuture4<Void> dep,
              CompletableFuture4<T> src,
              CompletableFuture4<U> snd,
              Runnable fn) {
            super(executor, dep, src, snd); this.fn = fn;
        }
        @Override
        final CompletableFuture4<Void> tryFire(int mode) {
            CompletableFuture4<Void> d;
            CompletableFuture4<T> a;
            CompletableFuture4<U> b;
            if ((d = dep) == null ||
                    !d.biRun(a = src, b = snd, fn, mode > 0 ? null : this)) {
                return null;
            }
            dep = null; src = null; snd = null; fn = null;
            return d.postFire(a, b, mode);
        }
    }

    @SuppressWarnings("serial")
    static final class BiRelay<T,U> extends BiCompletion<T,U,Void> { // for And
        BiRelay(CompletableFuture4<Void> dep,
                CompletableFuture4<T> src,
                CompletableFuture4<U> snd) {
            super(null, dep, src, snd);
        }
        @Override
        final CompletableFuture4<Void> tryFire(int mode) {
            CompletableFuture4<Void> d;
            CompletableFuture4<T> a;
            CompletableFuture4<U> b;
            if ((d = dep) == null || !d.biRelay(a = src, b = snd)) {
                return null;
            }
            src = null; snd = null; dep = null;
            return d.postFire(a, b, mode);
        }
    }

    @SuppressWarnings("serial")
    static final class OrApply<T,U extends T,V> extends BiCompletion<T,U,V> {
        Function<? super T,? extends V> fn;
        OrApply(Executor executor, CompletableFuture4<V> dep,
                CompletableFuture4<T> src,
                CompletableFuture4<U> snd,
                Function<? super T,? extends V> fn) {
            super(executor, dep, src, snd); this.fn = fn;
        }
        @Override
        final CompletableFuture4<V> tryFire(int mode) {
            CompletableFuture4<V> d;
            CompletableFuture4<T> a;
            CompletableFuture4<U> b;
            if ((d = dep) == null ||
                    !d.orApply(a = src, b = snd, fn, mode > 0 ? null : this)) {
                return null;
            }
            dep = null; src = null; snd = null; fn = null;
            return d.postFire(a, b, mode);
        }
    }

    @SuppressWarnings("serial")
    static final class OrAccept<T,U extends T> extends BiCompletion<T,U,Void> {
        Consumer<? super T> fn;
        OrAccept(Executor executor, CompletableFuture4<Void> dep,
                 CompletableFuture4<T> src,
                 CompletableFuture4<U> snd,
                 Consumer<? super T> fn) {
            super(executor, dep, src, snd); this.fn = fn;
        }
        @Override
        final CompletableFuture4<Void> tryFire(int mode) {
            CompletableFuture4<Void> d;
            CompletableFuture4<T> a;
            CompletableFuture4<U> b;
            if ((d = dep) == null ||
                    !d.orAccept(a = src, b = snd, fn, mode > 0 ? null : this)) {
                return null;
            }
            dep = null; src = null; snd = null; fn = null;
            return d.postFire(a, b, mode);
        }
    }

    @SuppressWarnings("serial")
    static final class OrRun<T,U> extends BiCompletion<T,U,Void> {
        Runnable fn;
        OrRun(Executor executor, CompletableFuture4<Void> dep,
              CompletableFuture4<T> src,
              CompletableFuture4<U> snd,
              Runnable fn) {
            super(executor, dep, src, snd); this.fn = fn;
        }
        @Override
        final CompletableFuture4<Void> tryFire(int mode) {
            CompletableFuture4<Void> d;
            CompletableFuture4<T> a;
            CompletableFuture4<U> b;
            if ((d = dep) == null ||
                    !d.orRun(a = src, b = snd, fn, mode > 0 ? null : this)) {
                return null;
            }
            dep = null; src = null; snd = null; fn = null;
            return d.postFire(a, b, mode);
        }
    }


    @SuppressWarnings("serial")
    static final class OrRelay<T,U> extends BiCompletion<T,U,Object> { // for Or
        OrRelay(CompletableFuture4<Object> dep, CompletableFuture4<T> src,
                CompletableFuture4<U> snd) {
            super(null, dep, src, snd);
        }
        @Override
        final CompletableFuture4<Object> tryFire(int mode) {
            CompletableFuture4<Object> d;
            CompletableFuture4<T> a;
            CompletableFuture4<U> b;
            if ((d = dep) == null || !d.orRelay(a = src, b = snd)) {
                return null;
            }
            src = null; snd = null; dep = null;
            return d.postFire(a, b, mode);
        }
    }

    /* ------------- Signallers -------------- */
    //除了 Signaller 和 CoCompletion 之外，所有的 可实例化的 Completion 都是继承自 UniCompletion
    @SuppressWarnings("serial")
    static final class Signaller extends Completion
            implements ForkJoinPool.ManagedBlocker {
        long nanos;                    // wait time if timed
        final long deadline;           // non-zero if timed
        volatile int interruptControl; // > 0: interruptible, < 0: interrupted
        volatile Thread thread;

        Signaller(boolean interruptible, long nanos, long deadline) {
            this.thread = Thread.currentThread();
            this.interruptControl = interruptible ? 1 : 0;
            this.nanos = nanos;
            this.deadline = deadline;
        }
        @Override
        final CompletableFuture4<?> tryFire(int ignore) {
            Thread w; // no need to atomically claim
            if ((w = thread) != null) {
                thread = null;
                LockSupport.unpark(w);
            }
            return null;
        }
        @Override
        public boolean isReleasable() {
            if (thread == null) {
                return true;
            }
            if (Thread.interrupted()) {
                int i = interruptControl;
                interruptControl = -1;
                if (i > 0) {
                    return true;
                }
            }
            if (deadline != 0L &&
                    (nanos <= 0L || (nanos = deadline - System.nanoTime()) <= 0L)) {
                thread = null;
                return true;
            }
            return false;
        }
        @Override
        public boolean block() {
            if (isReleasable()) {
                return true;
            } else if (deadline == 0L) {
                LockSupport.park(this);
            } else {
                if (nanos <= 0L) {
                    return isReleasable();
                }
                LockSupport.parkNanos(this, nanos);
            }
            return isReleasable();
        }
        @Override
        final boolean isLive() { return thread != null; }
    }

    /** BiCompletion的一个代理 */
    @SuppressWarnings("serial")
    static final class CoCompletion extends Completion {
        BiCompletion<?,?,?> base;
        CoCompletion(BiCompletion<?,?,?> base) { this.base = base; }
        @Override
        final CompletableFuture4<?> tryFire(int mode) {
            BiCompletion<?,?,?> c; CompletableFuture4<?> d;
            if ((c = base) == null || (d = c.tryFire(mode)) == null) {
                return null;
            }
            base = null; // detach
            return d;
        }
        @Override
        final boolean isLive() {
            BiCompletion<?,?,?> c;
            return (c = base) != null && c.dep != null;
        }
    }

    /* ------------- Zero-input Async forms -------------- */

    @SuppressWarnings("serial")
    static final class AsyncSupply<T> extends ForkJoinTask<Void>
            implements Runnable {

        CompletableFuture4<T> dep;
        Supplier<T> fn;
        AsyncSupply(CompletableFuture4<T> dep, Supplier<T> fn) {
            // 关联 Runnable 和 Future
            // Supplier 是构成 Runnable run的逻辑
            this.dep = dep;
            this.fn = fn;
        }

        @Override
        public final Void getRawResult() { return null; }
        @Override
        public final void setRawResult(Void v) {}
        @Override
        public final boolean exec() { run(); return true; }

        @Override
        public void run() {
            CompletableFuture4<T> d;
            Supplier<T> f;
            if ((d = dep) != null && (f = fn) != null) {
                dep = null;
                fn = null;
                if (d.result == null) {
                    try {
                        //设置执行完成
                        d.completeValue(f.get());
                    } catch (Throwable ex) {
                        d.completeThrowable(ex);
                    }
                }
                //通知后置的Stage，当前的 Stage 已经完成
                d.postComplete();
            }
        }
    }

    @SuppressWarnings("serial")
    static final class AsyncRun extends ForkJoinTask<Void>
            implements Runnable {
        CompletableFuture4<Void> dep; Runnable fn;
        AsyncRun(CompletableFuture4<Void> dep, Runnable fn) {
            this.dep = dep; this.fn = fn;
        }

        @Override
        public final Void getRawResult() { return null; }
        @Override
        public final void setRawResult(Void v) {}
        @Override
        public final boolean exec() { run(); return true; }

        @Override
        public void run() {
            CompletableFuture4<Void> d; Runnable f;
            if ((d = dep) != null && (f = fn) != null) {
                dep = null; fn = null;
                if (d.result == null) {
                    try {
                        f.run();
                        d.completeNull();
                    } catch (Throwable ex) {
                        d.completeThrowable(ex);
                    }
                }
                d.postComplete();
            }
        }
    }

    //==============================================================================================
      /*
    Signaller 和 CoCompletion 都只在内部方法流程中使用
    因此可以理解，外部的访问入口都是使用的 UniCompletion
    通过 UniCompletion 的定义可以看到，这里定义了一个事件源，一个依赖
    以 thenRun 方法为例，UniRun对象的src参数是当前对象，
     */

    private CompletableFuture4<Void> uniRunStage(Executor e, Runnable f) {
        if (f == null) {
            throw new NullPointerException();
        }
        CompletableFuture4<Void> d = new CompletableFuture4<>();
        if (e != null || !d.uniRun(this, f, null)) {
            //这里将 CompletableFuture 和 Completion 关联起来
            UniRun<T> c = new UniRun<T>(e, d, this, f);
            //将Completion 压入堆栈
            push(c);
            c.tryFire(SYNC);
        }
        return d;
    }

    final boolean uniRun(CompletableFuture4<?> a, Runnable f, UniRun<?> c) {
        Object r; Throwable x;
        //尝试在当前线程中执行 依赖的任务
        if (a == null || (r = a.result) == null || f == null) {
            return false;
        }
        if (result == null) {
            if (r instanceof Throwable) {
                completeThrowable((Throwable) r);
            } else {
                try {
                    //如果是异步执行，需要做下面的判断
                    if (c != null && !c.claim()) {
                        return false;
                    }
                    //直接调用run方法，同步方式执行
                    f.run();
                    completeNull();
                } catch (Throwable ex) {
                    completeThrowable(ex);
                }
            }
        }
        return true;
    }

    public CompletableFuture4<Void> thenRun(Runnable action) {
        return uniRunStage(null, action);
    }





    private void completeNull() {
    }

    private void push(Object var1) {
    }
    private void completeThrowable(Throwable ex) {
    }

    private void postComplete() {
    }

    private void completeValue(T t) {
    }

    private boolean orRelay(Object var1, Object var2) {
        return false;
    }

    private <T, U> boolean orRun(Object var1, Object var2, Object var3, Object var4) {
        return false;
    }

    private <T, V> boolean uniApply(Object var1, Object var2, Object var3) {
        return false;
    }

    final CompletableFuture4<T> postFire(Object var1, Object var2, Object var3) {
        return null;
    }

    final CompletableFuture4<T> postFire(Object var1, Object var2) {
        return null;
    }

    private <T> boolean uniAccept(Object var1, Object var2, Object var3) {
        return false;
    }

    private <T, U extends T> boolean orAccept(Object var1, Object var2, Object var3, Object var4) {
        return false;
    }

    private <T, U> boolean biRelay(Object var1, Object var2) {
        return false;
    }

    private <T, U extends T> boolean orApply(Object var1, Object var2, Object var3, Object var4) {
        return false;
    }

    private <T, U> boolean biRun(Object var1, Object var2, Object var3) {
        return false;
    }

    private <T, U> boolean biAccept(Object var1, Object var2, Object var3, Object var4) {
        return false;
    }

    private <T, U> boolean biApply(Object var1, Object var2, Object var3, Object var4) {
        return false;
    }

    private <T, V> boolean uniCompose(Object var1, Object var2, Object var3) {
        return false;
    }

    private boolean uniRelay(Object var1) {
        return false;
    }

    private <T> boolean uniExceptionally(Object var1, Object var2, Object var3) {
        return false;
    }

    private <T, V> boolean uniHandle(Object var1, Object var2, Object var3) {
        return false;
    }

    private <T> boolean uniWhenComplete(Object var1, Object var2, Object var3) {
        return false;
    }

    private <T, U> boolean biRun(Object var1, Object var2, Object var3, Object var4) {
        return false;
    }
}