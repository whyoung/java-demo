package why.java.demo.concurrent;

import java.util.concurrent.CountDownLatch;

import static why.java.demo.concurrent.ThreadUtils.sleep;

public class VolatileTest {

    static volatile int x = 9;
    public static void main(String[] args) throws InterruptedException {

        System.out.println(x);

        new Thread(() -> x = 3).start();

        sleep(1);
        System.out.println(x);

        new CountDownLatch(1).await();

    }
}
