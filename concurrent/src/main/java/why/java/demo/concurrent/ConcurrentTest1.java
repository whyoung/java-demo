package why.java.demo.concurrent;

import com.sun.xml.internal.ws.util.CompletedFuture;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import static why.java.demo.concurrent.ThreadUtils.MAX_THREAD;
import static why.java.demo.concurrent.ThreadUtils.newThreadFactory;
import static why.java.demo.concurrent.ThreadUtils.sleep;

public class ConcurrentTest1 {

    public static void main(String[] args) throws Exception {
        staticMethodTest();
        thenOne();
        handleException();
        thenBoth();
        thenEither();
        multi();
    }

    private static void multi() {
        //allOf，所有的都完成才返回，runAfterBoth 可以看做 allOf的一个特例
        //anyOf，任意一个完成即返回
    }

    public static void thenOne() {
        ExecutorService executor = new ThreadPoolExecutor(MAX_THREAD, MAX_THREAD,
                0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(), newThreadFactory("CompletableFuture", false));

        //提交一个异步任务
        CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
            System.out.println("start at " + System.currentTimeMillis());
            sleep(5);
        }, executor);

        //下面的方法都是在当前任务结束之后执行
        /*
        已Async 表示要异步执行这个任务，没有则表示在当线程中继续执行
        run 表示继续提交一个任务
        accept 表示接受上一个任务（即future）的执行结果并处理
        apply  表示接受上一个任务的执行结果，并返回新的结果
         */
        future.thenRun(() -> {
            System.out.println("then run at " + System.currentTimeMillis());
            sleep(1);
        }).thenAccept(unused -> {
            System.out.println("then accept at " + System.currentTimeMillis());
            sleep(1);
        }).thenApply(unused -> {
            System.out.println("then apply at " + System.currentTimeMillis());
            sleep(1);
            return "apply return";
        }).thenAccept(i -> {
            System.out.println(i);
            executor.shutdown();
        });
    }

    public static void thenCompose() {
        ExecutorService executor = new ThreadPoolExecutor(MAX_THREAD, MAX_THREAD,
                0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(), newThreadFactory("CompletableFuture", false));

        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
            sleep(5);
            return 3;
        }, executor);

        //如果有这样的方法，返回值也是 CompletableFuture 类型
        //假设这个任务依赖 future的运行结果
        //如果我们直接使用thenApply，泛型就变成CompletableFuture<Integer> 类型，
        CompletableFuture<CompletableFuture<Integer>> future1 = future.thenApplyAsync((x) -> {
            sleep(1);
            return calc(x, executor);
        });

        future1.thenAccept((x) -> {
            //如果要获取calc 方法的运行结果，只能通过 future1的运行结果，再去获取 calc的结果
            int ret = x.join();
            assert ret == 6;
        });


        // 为了避免嵌套,可以使用thenCompose
        CompletableFuture<Integer> future2 = CompletableFuture.supplyAsync(()->{
            sleep(5);
            return 5;
        }).thenComposeAsync((x) -> calc(x, executor));
        future2.thenAccept(System.out::println).thenRun(executor::shutdown);
    }

    private static CompletableFuture<Integer> calc(int x, Executor executor) {
        return CompletableFuture.supplyAsync(() -> x * 2, executor);
    }

    public static void thenBoth() {
        ExecutorService executor = new ThreadPoolExecutor(MAX_THREAD, MAX_THREAD,
                0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(), newThreadFactory("CompletableFuture", false));

        //提交一个异步任务
        CompletableFuture<Integer> future1 = CompletableFuture.supplyAsync(() -> {
            System.out.println("task 1 start at " + System.currentTimeMillis());
            sleep(5);
            return 1;
        }, executor);

        CompletableFuture<Integer> future2 = CompletableFuture.supplyAsync(() -> {
            System.out.println("task 2 start at " + System.currentTimeMillis());
            sleep(8);
            return 2;
        }, executor);

        /*
        then accept both 表示在前面两个任务结束之后，拿到两个任务的执行结果，再执行第三个任务
        有Async表示异步执行
         */
        CompletableFuture<Void> future3 = future1.thenAcceptBoth(future2, (i1, i2) -> {
            System.out.println("all task finish at " + System.currentTimeMillis());
            System.out.println("task 1 return " + i1 + ", task 2 return " +i2 +", sum is " + (i1 + i2));
        });

        //表示接受两个任务的执行结果，并返回一个新的结果
        future1.thenCombine(future2, new BiFunction<Integer, Integer, Object>() {
            @Override
            public Object apply(Integer i1, Integer i2) {
                return i1 + i2;
            }
        }).thenAccept((i) -> System.out.println("combine return " + i));
    }

    public static void thenEither() {
        ExecutorService executor = new ThreadPoolExecutor(MAX_THREAD, MAX_THREAD,
                0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(), newThreadFactory("CompletableFuture", false));

        //提交一个异步任务
        CompletableFuture<Integer> future1 = CompletableFuture.supplyAsync(() -> {
            System.out.println("task 1 start at " + System.currentTimeMillis());
            sleep(3);
            return 1/0;
        }, executor);

        CompletableFuture<String> future2 = CompletableFuture.supplyAsync(() -> {
            System.out.println("task 2 start at " + System.currentTimeMillis());
            sleep(5);
            return "2";
        }, executor);

        future1.thenAcceptBoth(future2, new BiConsumer<Integer, String>() {
            @Override
            public void accept(Integer integer, String s) {
                System.out.println(integer);
                System.out.println(s);
            }
        });


        future1.runAfterEither(future2, new Runnable() {
            @Override
            public void run() {
                System.out.println("task 3 start at " + System.currentTimeMillis());
            }
        });

        future1.runAfterBoth(future1, new Runnable() {
            @Override
            public void run() {
                System.out.println("task 4 start at " + System.currentTimeMillis());
            }
        });

        CompletableFuture<Integer> future_x = CompletableFuture.supplyAsync(() -> {
            System.out.println("task x start at " + System.currentTimeMillis());
            sleep(10);
            return 1;
        }, executor);
        future1.acceptEither(future_x, new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) {
                System.out.println("task 5 start at " + System.currentTimeMillis());
                System.out.println(integer);
            }
        });

        future1.acceptEitherAsync(future_x, new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) {
                System.out.println("task 6 start at " + System.currentTimeMillis());
                System.out.println(integer);
            }
        });

        future1.applyToEither(future_x, new Function<Integer, Object>() {
            @Override
            public Object apply(Integer integer) {
                System.out.println("task 7 start at " + System.currentTimeMillis());
                return integer;
            }
        });
    }

    public static void handleException() {
        ExecutorService executor = new ThreadPoolExecutor(MAX_THREAD, MAX_THREAD,
                0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(), newThreadFactory("CompletableFuture", false));

        //提交一个异步任务
        CompletableFuture<Integer> future1 = CompletableFuture.supplyAsync(() -> {
            sleep(3);
            return 1;
        }, executor);

        future1.whenComplete(new BiConsumer<Integer, Throwable>() {
            @Override
            public void accept(Integer ret, Throwable ex) {
                if (ex != null) {
                    ex.printStackTrace();
                } else {
                    System.out.println(ret);
                }
            }
        }).thenRun(() -> {
            //使用when如果抛出异常后面的不会再执行不会执行到这一步
            System.out.println("then close executor");
        });

        CompletableFuture<Integer> future2 = CompletableFuture.supplyAsync(() -> {
            sleep(3);
            return 1/0;
        }, executor);

        future2.handle(new BiFunction<Integer, Throwable, Object>() {
            @Override
            public Object apply(Integer ret, Throwable ex) {
                if (ex != null) {
                    ex.printStackTrace();
                    return new Object();
                } else {
                    System.out.println(ret);
                    return ret;
                }
            }
        }).thenRun(() -> {
            //handle发生异常会继续往下执行
            System.out.println("then close executor");
        });

        CompletableFuture<Integer> future3 = CompletableFuture.supplyAsync(() -> {
            sleep(3);
            return 1/0;
        }, executor);
        future3.exceptionally(new Function<Throwable, Integer>() {
            @Override
            public Integer apply(Throwable ex) {
                ex.printStackTrace();
                //如果抛出异常，不会继续往下执行，否则，将使用 apply方法的返回值作为future3的get值
//                throw new RuntimeException(ex);
                return 3;
            }
        }).thenAccept((x) -> {
            System.out.println("-----");
            System.out.println(x);
//            try {
//                //注意 future3.get() 还是 抛出的异常，只是后续的stage 如果需要接收future3的结果，将返回3
//                System.out.println(Objects.equals(future3.get(), x));
//            } catch (InterruptedException | ExecutionException ignore) {
//            }
            executor.shutdown();
        });


        CompletableFuture<Integer> future4 = CompletableFuture.supplyAsync(() -> {
            sleep(3);
            return 1/0;
        }, executor);
    }


    private static void staticMethodTest() throws Exception {
        ExecutorService executor = new ThreadPoolExecutor(MAX_THREAD, MAX_THREAD,
                0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(), newThreadFactory("CompletableFuture", false));

        Random r = new Random();

        //提交一个无返回值的任务
        CompletableFuture<Void> future1 = CompletableFuture.runAsync(() -> System.out.println(Thread.currentThread().getName() + " run this task"), executor);

        //提交一个有返回值的异步任务
        CompletableFuture<Integer> future2 = CompletableFuture.supplyAsync(() -> 5, executor);
        System.out.println(future2.get());
        List<CompletableFuture<Integer>> futureTasks = new ArrayList<>();
        for (int i = 1; i<=10; i++) {
            final int x = i;
            futureTasks.add(CompletableFuture.supplyAsync(() -> {
                int second = r.nextInt(10) + 1;
                System.out.println("task " + x + " sleep " + second);
                sleep(second);
                return x;
            }));
        }

        CompletableFuture<?>[] futureTaskArray = futureTasks.toArray(new CompletableFuture[0]);

        //提交一个任务列表，任意任务结束之后，就表示done
        CompletableFuture<Object> future4 = CompletableFuture.anyOf(futureTaskArray);
        while (!future4.isDone()) {
        }
        for (int i = 1; i<=10; i++) {
            if (futureTaskArray[i - 1].isDone()) {
                System.out.println("task " + i + " is done");
            } else {
                System.out.println("task " + i + " is not done yet");
            }
        }
        futureTasks.forEach(t -> System.out.println(t.isDone()));

        //提交一个任务列表，所有任务都结束才算完成
        CompletableFuture<Void> future3 = CompletableFuture.allOf(futureTaskArray);
        while (!future3.isDone()) {
        }
        for (int i = 1; i<= 10; i++) {
            if (futureTaskArray[i - 1].isDone()) {
                System.out.println("task " + i + " is done");
            } else {
                System.out.println("task " + i + " is not done yet");
            }
        }
        executor.shutdown();
    }
}
