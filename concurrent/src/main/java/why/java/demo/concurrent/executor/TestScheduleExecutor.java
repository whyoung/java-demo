package why.java.demo.concurrent.executor;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class TestScheduleExecutor {

    public static void main(String[] args) {

        ScheduledExecutorService service = Executors.newScheduledThreadPool(1);

        service.schedule(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                TimeUnit.SECONDS.sleep(30);
                return null;
            }
        }, 100, TimeUnit.SECONDS);
    }
}
