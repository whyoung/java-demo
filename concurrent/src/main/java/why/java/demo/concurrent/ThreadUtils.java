package why.java.demo.concurrent;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class ThreadUtils {

    private static final int DEFAULT_THREAD = 4;
    public static final int MAX_THREAD;
    static {
        int processors = Runtime.getRuntime().availableProcessors();
        if (processors <= 0 || processors > DEFAULT_THREAD) {
            MAX_THREAD = DEFAULT_THREAD;
        } else {
            MAX_THREAD = processors;
        }
    }

    public static ThreadFactory newThreadFactory(String name, boolean daemon) {
        return new DefaultThreadFactory(name, daemon);
    }

    static class DefaultThreadFactory implements ThreadFactory {
        private final ThreadGroup group;
        private final AtomicInteger threadNumber = new AtomicInteger(1);
        private final String namePrefix;
        private final boolean daemon;

        DefaultThreadFactory(String name, boolean daemon) {
            this.daemon = daemon;
            group = Thread.currentThread().getThreadGroup();
            namePrefix = name +
                    "-thread-";
        }

        @Override
        public Thread newThread(Runnable r) {
            Thread t = new Thread(group, r,
                    namePrefix + threadNumber.getAndIncrement(),
                    0);
            t.setPriority(Thread.NORM_PRIORITY);
            t.setDaemon(daemon);
            return t;
        }
    }

    public static void sleep(int seconds) {
        if (seconds < 0) {
            throw new IllegalArgumentException();
        }
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException ignore) {
        }
    }
}
