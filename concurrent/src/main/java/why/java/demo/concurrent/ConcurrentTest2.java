package why.java.demo.concurrent;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static why.java.demo.concurrent.ThreadUtils.MAX_THREAD;
import static why.java.demo.concurrent.ThreadUtils.newThreadFactory;
import static why.java.demo.concurrent.ThreadUtils.sleep;

public class ConcurrentTest2 {


    public static void main(String[] args) {
        testRacing();
    }

    private static void testSum() {
        Random r = new Random();
        ExecutorService executor = new ThreadPoolExecutor(MAX_THREAD, MAX_THREAD,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(), newThreadFactory("CompletionServiceTask", false));

        CompletionService<Integer> service = new ExecutorCompletionService<>(executor);

        List<Callable<Integer>> tasks = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            final int x = i;
            tasks.add(() -> {
                int second = r.nextInt(10) + 1;
                System.out.println("task " + x + " sleep " + second);
                sleep(second);
                return x;
            });
        }
        tasks.forEach(service::submit);

        int sum = 0;
        int count = tasks.size();
        while (count > 0) {
            try {
                Future<Integer> f = service.take();
                sum += f.get();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                count--;
            }
        }
        System.out.println("sum = " + sum);
        executor.shutdown();
    }


    private static void testRacing() {
        Random r = new Random();

        ExecutorService executor = new ThreadPoolExecutor(MAX_THREAD, MAX_THREAD,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(), newThreadFactory("CompletionServiceTask", false));

        CompletionService<Integer> service = new ExecutorCompletionService<>(executor);

        List<Future<Integer>> tasks = new ArrayList<>();
        for (int i = 1; i <= MAX_THREAD; i++) {
            final int x = i;
            tasks.add(service.submit(() -> {
                int second = r.nextInt(10) + 1;
                System.out.println("task " + x + " sleep " + second);
                sleep(second);
                return x;
            }));
        }

        Integer ret = null;
        while (ret == null) {
            try {
                Future<Integer> f = service.take();
                ret = f.get();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        tasks.forEach(t -> t.cancel(true));
        System.out.println("the first finished is " + ret);
        executor.shutdown();
    }
}
