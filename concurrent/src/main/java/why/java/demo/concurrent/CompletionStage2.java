package why.java.demo.concurrent;


import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

public interface CompletionStage2<T> {

    //-------------------------------针对一个 CompletionStage -----------------------------------
    /*
    apply get前一个stage的执行结果，并返回新的结果
     */
    <U> CompletionStage2<U> thenApply(Function<? super T, ? extends U> fn);
    <U> CompletionStage2<U> thenApplyAsync(Function<? super T, ? extends U> fn);
    <U> CompletionStage2<U> thenApplyAsync(Function<? super T, ? extends U> fn, Executor executor);

    /*
    accept 消费前一个stage的结果
     */
    CompletionStage2<Void> thenAccept(Consumer<? super T> action);
    CompletionStage2<Void> thenAcceptAsync(Consumer<? super T> action);
    CompletionStage2<Void> thenAcceptAsync(Consumer<? super T> action, Executor executor);

    /*
    前一个执行完成之后，执行新的动作
     */
    CompletionStage2<Void> thenRun(Runnable action);
    CompletionStage2<Void> thenRunAsync(Runnable action);
    CompletionStage2<Void> thenRunAsync(Runnable action, Executor executor);

    /*
    类似 thenApply，只是fn返回的CompletionStage，相当于在当前stage后面插入一个stage，返回的stage是fn生成的stage的下一个stage
     */
    <U> CompletionStage2<U> thenCompose(Function<? super T, ? extends CompletionStage2<U>> fn);
    <U> CompletionStage2<U> thenComposeAsync(Function<? super T, ? extends CompletionStage2<U>> fn);
    <U> CompletionStage2<U> thenComposeAsync(Function<? super T, ? extends CompletionStage2<U>> fn, Executor executor);

    //-------------------------------针对两个 CompletionStage -----------------------------------
    <U, V> CompletionStage2<V> thenCombine(CompletionStage2<? extends U> other, BiFunction<? super T, ? super U, ? extends V> fn);
    <U, V> CompletionStage2<V> thenCombineAsync(CompletionStage2<? extends U> other, BiFunction<? super T, ? super U, ? extends V> fn);
    <U, V> CompletionStage2<V> thenCombineAsync(CompletionStage2<? extends U> other, BiFunction<? super T, ? super U, ? extends V> fn, Executor executor);

    <U> CompletionStage2<Void> thenAcceptBoth(CompletionStage2<? extends U> other, BiConsumer<? super T, ? super U> action);
    <U> CompletionStage2<Void> thenAcceptBothAsync(CompletionStage2<? extends U> other, BiConsumer<? super T, ? super U> action);
    <U> CompletionStage2<Void> thenAcceptBothAsync(CompletionStage2<? extends U> other, BiConsumer<? super T, ? super U> action, Executor executor);

    CompletionStage2<Void> runAfterBoth(CompletionStage2<?> other, Runnable action);
    CompletionStage2<Void> runAfterBothAsync(CompletionStage2<?> other, Runnable action);
    CompletionStage2<Void> runAfterBothAsync(CompletionStage2<?> other, Runnable action, Executor executor);

    <U> CompletionStage2<U> applyToEither(CompletionStage2<? extends T> other, Function<? super T, U> fn);
    <U> CompletionStage2<U> applyToEitherAsync(CompletionStage2<? extends T> other, Function<? super T, U> fn);
    <U> CompletionStage2<U> applyToEitherAsync(CompletionStage2<? extends T> other, Function<? super T, U> fn, Executor executor);

    CompletionStage2<Void> acceptEither(CompletionStage2<? extends T> other, Consumer<? super T> action);
    CompletionStage2<Void> acceptEitherAsync(CompletionStage2<? extends T> other, Consumer<? super T> action);
    CompletionStage2<Void> acceptEitherAsync(CompletionStage2<? extends T> other, Consumer<? super T> action, Executor executor);

    CompletionStage2<Void> runAfterEither(CompletionStage2<?> other, Runnable action);
    CompletionStage2<Void> runAfterEitherAsync(CompletionStage2<?> other, Runnable action);
    CompletionStage2<Void> runAfterEitherAsync(CompletionStage2<?> other, Runnable action, Executor executor);

    CompletableFuture<T> toCompletableFuture();
    /*
    可以处理异常，其他的方法要求必须是前一个stage正常返回
     */
    CompletionStage2<T> exceptionally(Function<Throwable, ? extends T> fn);

    CompletionStage2<T> whenComplete(BiConsumer<? super T, ? super Throwable> action);
    CompletionStage2<T> whenCompleteAsync(BiConsumer<? super T, ? super Throwable> action);
    CompletionStage2<T> whenCompleteAsync(BiConsumer<? super T, ? super Throwable> action, Executor executor);

    <U> CompletionStage2<U> handle(BiFunction<? super T, Throwable, ? extends U> fn);
    <U> CompletionStage2<U> handleAsync(BiFunction<? super T, Throwable, ? extends U> fn);
    <U> CompletionStage2<U> handleAsync(BiFunction<? super T, Throwable, ? extends U> fn, Executor executor);
}

