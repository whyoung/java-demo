package why.java.demo.concurrent;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;

import static why.java.demo.concurrent.ThreadUtils.sleep;

public class ConcurrentTest3 {

    public static void main(String[] args) throws InterruptedException {
        final CountDownLatch latch = new CountDownLatch(1);
        final Random r = new Random();
        List<CompletableFuture<Integer>> tasks = new ArrayList<>();
        for (int i =1; i<=10; i++) {
            final int x = i;
            tasks.add(CompletableFuture.supplyAsync(() -> {
                sleep(r.nextInt(10) + 1);
                if (x == 5) {
                    return x / 0;
                } else if (x == 7) {
                    sleep(10);
                    Integer s = null;
                    return s + 2;
                }
                System.out.println("task " + x + " is done");
                return x;
            }));
        }

        CompletableFuture<Void> future = CompletableFuture.allOf(tasks.toArray(new CompletableFuture[0]));

        future.thenRun(() -> {
            System.out.println("finish");
            latch.countDown();
        });

        while(!future.isDone()) {
        }
        future.join();
        System.out.println(future.isDone());
        System.out.println(future.isCompletedExceptionally());

//        future.handle((unused, ex) -> {
//            if (ex != null) {
//                System.out.println("error");
//                ex.printStackTrace();
//            }
//            return null;
//        }).thenRun(latch::countDown);

        latch.await();
    }
}
