package why.java.demo.concurrent;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static why.java.demo.concurrent.ThreadUtils.MAX_THREAD;
import static why.java.demo.concurrent.ThreadUtils.newThreadFactory;
import static why.java.demo.concurrent.ThreadUtils.sleep;

public class ConcurrentTest4 {

    public static void main(String[] args) {
        ExecutorService executor = new ThreadPoolExecutor(MAX_THREAD, MAX_THREAD,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(), newThreadFactory("CompletionServiceTask", false));

        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
            sleep(5);
            return 1;
        }, executor);

        future.thenRunAsync(() -> {
            sleep(3);
            System.out.println("then run 1");
        }, executor);

        future.thenRunAsync(() -> {
            sleep(6);
            System.out.println("then run 2");
        }, executor);
    }
}
