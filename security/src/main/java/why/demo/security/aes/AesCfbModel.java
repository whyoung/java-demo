package why.demo.security.aes;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;

/**
 * CFB模式又称密文反馈模式（Cipher feedback)：
 * 与ECB和CBC模式只能够加密块数据不同，CFB能够将块密文（Block Cipher）转换为流密文（Stream Cipher）
 *
 * CFB的加密工作分为两部分：
 * 1、将一前段加密得到的密文再加密；
 * 2、将第1步加密得到的数据与当前段的明文异或。
 *
 * 由于加密流程和解密流程中被块加密器加密的数据是前一段密文，因此即使明文数据的长度不是加密块大小的整数倍也是不需要填充的，这保证了数据长度在加密前后是相同的。
 * 以128位为例，128位的CFB是对前一段数据的密文用块加密器加密后保存在IV中，之后用这128位数据与后面到来的128位数据异或，num用来记录自上次调用加密器后已经处理的数据长度（字节），
 * 当num重新变为0的时候就会再调用加密器，即每处理128位调用一次加密器。
 *
 * CFB128是每处理128位数据调用一次加密器，此外还有两种常用的CFB是CFB8和CFB1，前者每处理8位调用一次加密器，后者每处理1位调用1次加密器，就运算量来讲CFB1是CFB8的8倍，是CFB128的128倍。对于CFB8和CFB1需要将IV作为移位寄存器。
 */
public class AesCfbModel extends AbstractAesMode {

    public AesCfbModel() {
        super("AES/CFB/PKCS5Padding");
    }

    @Override
    public byte[] encrypt(byte[] content, String secretKey) throws Exception {
        byte[] keyByte = secretKey.getBytes();
        SecretKeySpec keySpec = new SecretKeySpec(keyByte, AES_ALGORITHM);
        Cipher cipher = Cipher.getInstance(algorithm);
        //需要设置初始化向量，向量的长度需要和块长度保持一致
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(new byte[cipher.getBlockSize()]));
        return cipher.doFinal(content);
    }

    @Override
    public byte[] decrypt(byte[] content, String secretKey) throws Exception {
        byte[] keyByte = secretKey.getBytes(StandardCharsets.UTF_8);
        SecretKeySpec keySpec = new SecretKeySpec(keyByte, AES_ALGORITHM);
        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(new byte[cipher.getBlockSize()]));
        return cipher.doFinal(content);
    }
}
