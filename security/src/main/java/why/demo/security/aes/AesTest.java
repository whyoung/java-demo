package why.demo.security.aes;

import com.sun.tools.javac.util.Assert;
import why.demo.security.EnDecrypt;

import java.nio.charset.StandardCharsets;
import java.util.Objects;

/**
 * Advanced Encryption Standard
 * 高级加密标准 (AES) 密钥是对称密钥，它可使用三种不同的密钥长度：128 位、192 位或 256 位
 *
 * 关于填充方式
 * 1.NoPadding
 * 不填充，只能加密长为128bits倍数的信息，一般不会使用
 *
 * 2.PKCS#5 和 PKCS#7
 * 缺几个字节就填几个缺的字节数
 * 对于AES来说PKCS5Padding和PKCS7Padding是完全一样的，AES加密当中严格来说是不能使用PKCS5的，因为AES的块大小是16bytes而PKCS5只能用于8bytes，通常我们在AES加密中所说的PKCS5指的就是PKCS7
 *
 * 3.ZerosPadding
 * 全部填充0x00，无论缺多少全部填充0x00，已经是128bits倍数仍要填充
 *
 * 4.ISO 10126
 * 最后一个字节是填充的字节数（包括最后一字节），其他全部填随机数
 *
 * 5.NSI X9.23
 * 类似ISO 10126填充，只不过ANSI X9.23其他字节填的都是0而不是随机数
 *
 * 消息的完整性校验与GCM
 *
 */
public class AesTest {

    public static void main(String[] args) {
        String secretKey128 = "waWAj2faiv8M6rhH";
        String secretKey192 = "xGrn4t7urtD0bIY+ubi7lYQ6";
        String secretKey256 = "t+F5p88uHGS+WgksUQ45hFHEwpJRhvT8";
        String data = "🌶④√赛博朋克-_。%2077-hello-😃👌emoji👿";
        testAes(secretKey128, secretKey192, secretKey256, data);
//        testNoPadding(secretKey256);
    }

    private static void testAes(String secretKey128, String secretKey192, String secretKey256, String data) {
        //都是使用PACS7填充方式
        EnDecrypt ecb = new AesEcbModel();
        testAes(ecb, secretKey128, data);
        testAes(ecb, secretKey192, data);
        testAes(ecb, secretKey256, data);

        EnDecrypt cbc = new AesCbcModel();
        testAes(cbc, secretKey128, data);

        EnDecrypt ctr = new AesCtrModel();
        testAes(ctr, secretKey128, data);

        EnDecrypt cfb = new AesCfbModel();
        testAes(cfb, secretKey128, data);

        EnDecrypt ofb = new AesOfbModel();
        testAes(ofb, secretKey128, data);
    }

    private static void testNoPadding(String secretKey) {
        //16byte
        String source = "A01B23C45D67E89F";
        EnDecrypt ecbNoPadding = new AesEcbNoPaddingModel();
        String target = null;
        try {
            byte[] content = source.getBytes(StandardCharsets.UTF_8);
            byte[] encrypt = ecbNoPadding.encrypt(content, secretKey);
            System.out.println(encrypt.length);
            System.out.println(new String(encrypt));
            byte[] decrypt = ecbNoPadding.decrypt(encrypt, secretKey);
            target = new String(decrypt, StandardCharsets.UTF_8);
        } catch (Exception ignore) {
        }
        Assert.check(Objects.equals(source, target), "加密和解密数据不一致");
    }

    private static void testAes(EnDecrypt aes, String secretKey, String source) {
        String target = null;
        try {
            byte[] content = source.getBytes(StandardCharsets.UTF_8);
            byte[] encrypt = aes.encrypt(content, secretKey);
            byte[] decrypt = aes.decrypt(encrypt, secretKey);
            target = new String(decrypt, StandardCharsets.UTF_8);
        } catch (Exception ignore) {
        }
        Assert.check(Objects.equals(source, target), "加密和解密数据不一致");
    }
}
