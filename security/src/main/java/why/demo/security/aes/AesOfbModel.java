package why.demo.security.aes;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;

/**
 * OFB模式又称输出反馈模式（Output feedback）：
 * OFB是先用块加密器生成密钥流（Key stream），然后再将密钥流与明文流异或得到密文流，解密是先用块加密器生成密钥流，再将密钥流与密文流异或得到明文，
 * 由于异或操作的对称性所以加密和解密的流程是完全一样的。
 *
 * OFB与CFB一样都非常适合对流数据的加密，OFB由于加密和解密都依赖与前一段数据，所以加密和解密都不能并行。
 */
public class AesOfbModel extends AbstractAesMode {

    public AesOfbModel() {
        super("AES/OFB/PKCS5Padding");
    }

    @Override
    public byte[] encrypt(byte[] content, String secretKey) throws Exception {
        byte[] keyByte = secretKey.getBytes();
        SecretKeySpec keySpec = new SecretKeySpec(keyByte, AES_ALGORITHM);
        Cipher cipher = Cipher.getInstance(algorithm);
        //需要设置初始化向量，向量的长度需要和块长度保持一致
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(new byte[cipher.getBlockSize()]));
        return cipher.doFinal(content);
    }

    @Override
    public byte[] decrypt(byte[] content, String secretKey) throws Exception {
        byte[] keyByte = secretKey.getBytes(StandardCharsets.UTF_8);
        SecretKeySpec keySpec = new SecretKeySpec(keyByte, AES_ALGORITHM);
        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(new byte[cipher.getBlockSize()]));
        return cipher.doFinal(content);
    }
}
