package why.demo.security.aes;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;

/**
 * ECB（Electronic Codebook Book）模式
 * 将整个明文分成若干段相同的小段，然后对每一小段进行加密。
 *
 * ECB是最简单的块密码加密模式，加密前根据加密块大小（AES为128位）分成若干块，每块使用相同的密钥单独加密，解密同理
 * ECB模式由于每块数据的加密是独立的因此加密和解密都可以并行计算，ECB模式最大的缺点是相同的明文块会被加密成相同的密文块，这种方法在某些环境下不能提供严格的数据保密性
 */
public class AesEcbModel extends AbstractAesMode {

    public AesEcbModel() {
        super("AES/ECB/PKCS5Padding");
    }

    @Override
    public byte[] encrypt(byte[] content, String secretKey) throws Exception {
        byte[] keyByte = secretKey.getBytes();
        SecretKeySpec keySpec = new SecretKeySpec(keyByte, AES_ALGORITHM);
        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.ENCRYPT_MODE, keySpec);
        return cipher.doFinal(content);
    }

    @Override
    public byte[] decrypt(byte[] content, String secretKey) throws Exception {
        byte[] keyByte = secretKey.getBytes(StandardCharsets.UTF_8);
        SecretKeySpec keySpec = new SecretKeySpec(keyByte, AES_ALGORITHM);
        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.DECRYPT_MODE, keySpec);
        return cipher.doFinal(content);
    }
}
