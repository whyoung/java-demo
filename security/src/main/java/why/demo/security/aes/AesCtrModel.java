package why.demo.security.aes;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;

/**
 * CTR(Counter)
 * CTR模式中， 有一个自增的算子，这个算子用密钥加密之后的输出和明文异或的结果得到密文，相当于一次一密。
 * 这种加密方式简单快速，安全可靠，而且可以并行加密，但是在计算器不能维持很长的情况下，密钥只能使用一次。
 *
 * CTR 的另两个优点是：1）支持加解密并行计算，可事先进行加解密准备；
 *                  2）错误密文中的对应比特只会影响明文中的对应比特(加密数据块独立，不依赖前面的数据块)
 */
public class AesCtrModel extends AbstractAesMode {

    public AesCtrModel() {
        super("AES/CTR/PKCS5Padding");
    }

    @Override
    public byte[] encrypt(byte[] content, String secretKey) throws Exception {
        byte[] keyByte = secretKey.getBytes();
        SecretKeySpec keySpec = new SecretKeySpec(keyByte, AES_ALGORITHM);
        Cipher cipher = Cipher.getInstance(algorithm);
        //需要设置初始化向量，向量的长度需要和块长度保持一致
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(new byte[cipher.getBlockSize()]));
        return cipher.doFinal(content);
    }

    @Override
    public byte[] decrypt(byte[] content, String secretKey) throws Exception {
        byte[] keyByte = secretKey.getBytes(StandardCharsets.UTF_8);
        SecretKeySpec keySpec = new SecretKeySpec(keyByte, AES_ALGORITHM);
        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(new byte[cipher.getBlockSize()]));
        return cipher.doFinal(content);
    }
}
