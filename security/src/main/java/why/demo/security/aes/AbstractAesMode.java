package why.demo.security.aes;

import why.demo.security.EnDecrypt;

public abstract class AbstractAesMode implements EnDecrypt {

    static final String AES_ALGORITHM = "AES";

    protected String algorithm;

    public AbstractAesMode(String algorithm) {
        this.algorithm = algorithm;
    }
}
