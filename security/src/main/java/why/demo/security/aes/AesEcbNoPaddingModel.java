package why.demo.security.aes;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;

public class AesEcbNoPaddingModel extends AbstractAesMode {

    public AesEcbNoPaddingModel() {
        super("AES/ECB/NoPadding");
    }

    @Override
    public byte[] encrypt(byte[] content, String secretKey) throws Exception {
        byte[] keyByte = secretKey.getBytes();
        SecretKeySpec keySpec = new SecretKeySpec(keyByte, AES_ALGORITHM);
        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.ENCRYPT_MODE, keySpec);
        return cipher.doFinal(content);
    }

    @Override
    public byte[] decrypt(byte[] content, String secretKey) throws Exception {
        byte[] keyByte = secretKey.getBytes(StandardCharsets.UTF_8);
        SecretKeySpec keySpec = new SecretKeySpec(keyByte, AES_ALGORITHM);
        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.DECRYPT_MODE, keySpec);
        return cipher.doFinal(content);
    }
}
