package why.demo.security.rsa;

import com.sun.tools.javac.util.Assert;
import org.apache.commons.lang3.tuple.Pair;
import why.demo.security.EnDecrypt;

import java.nio.charset.StandardCharsets;
import java.util.Objects;

public class RsaTest {

    public static void main(String[] args) throws Exception {
        Pair<String, String> secretKey = RsaEnDecrypt.generateKeyPair();

        EnDecrypt crypt = new RsaEnDecrypt();
        String data = "🌶④√赛博朋克-_。%2077-hello-😃👌emoji👿";

        byte[] encrypt = crypt.encrypt(data.getBytes(StandardCharsets.UTF_8), secretKey.getLeft());
        byte[] decrypt = crypt.decrypt(encrypt, secretKey.getRight());
        String target = new String(decrypt, StandardCharsets.UTF_8);
        Assert.check(Objects.equals(data, target), "加密和解密数据不一致");
    }
}
