package why.demo.security;

public interface Decrypt {

    byte[] decrypt(byte[] content, String secretKey) throws Exception;
}
