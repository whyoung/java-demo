package why.demo.security.des;

import com.sun.tools.javac.util.Assert;
import why.demo.security.EnDecrypt;

import java.nio.charset.StandardCharsets;
import java.util.Objects;

public class DesTest {

    public static void main(String[] args) throws Exception {

        String secretKey = "eWWDX69o";
        EnDecrypt crypt = new DesEnDecrypt();
        String data = "🌶④√赛博朋克-_。%2077-hello-😃👌emoji👿";

        byte[] encrypt = crypt.encrypt(data.getBytes(StandardCharsets.UTF_8), secretKey);
        byte[] decrypt = crypt.decrypt(encrypt, secretKey);
        String target = new String(decrypt, StandardCharsets.UTF_8);
        Assert.check(Objects.equals(data, target), "加密和解密数据不一致");
    }
}
