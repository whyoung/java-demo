package why.demo.security.des;

import why.demo.security.EnDecrypt;

import javax.crypto.Cipher;
import java.security.Key;

/**
 * DES：Data Encryption Standard
 * 明文和密文为 64 位分组，密钥的长度为 64 位，但是密钥的每个第八位设置为奇偶校验位，因此密钥的实际长度为56位。
 * 加密过程：
 * （1）初始置换，根据IP置换表将明文顺序打乱
 * （2）生成子密钥
 * （3）迭代过程
 * （4）逆置换
 *
 * 解密过程：
 *
 * 3DES：
 * 3DES相当于对统一数据块采用3次DES,3次DES使用的密钥如果完全不同，则密钥长度可以达到168位
 *
 */
public class DesEnDecrypt implements EnDecrypt {

    private static final String DES_ALGORITHM = "DES";

    @Override
    public byte[] decrypt(byte[] content, String secretKey) throws Exception {
        Cipher cipher = Cipher.getInstance(DES_ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, getKey(secretKey.getBytes()));
        return cipher.doFinal(content);
    }

    @Override
    public byte[] encrypt(byte[] content, String secretKey) throws Exception {
        Cipher cipher = Cipher.getInstance(DES_ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, getKey(secretKey.getBytes()));
        return cipher.doFinal(content);
    }

    private Key getKey(byte[] secretKey) throws Exception {
        // 创建一个空的8位字节数组（默认值为0），将原始字节数组转换为8位
//        byte[] tmp = new byte[8];
//        for (int i = 0; i < tmp.length && i < secretKey.length; i++) {
//            tmp[i] = secretKey[i];
//        }
        // 生成密钥
        return new javax.crypto.spec.SecretKeySpec(secretKey, DES_ALGORITHM);
    }
}
