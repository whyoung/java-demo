package why.demo.security;

public interface Encrypt {

    byte[] encrypt(byte[] content, String secretKey) throws Exception;
}
