package why.demo.optimize;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class ArthasTraceTest {

    public static void main(String[] args) throws Exception {
        run();
        new CountDownLatch(1).await();
    }

    public static void run() {
        m1();
    }

    private static void m1() {
        sleep(1);
        m2();
    }

    private static void m2() {
        sleep(1);
        m3();
    }

    private static void m3() {
        sleep(1);
        m4();
    }

    private static void m4() {
        sleep(1);
        System.out.println("over");
    }


    private static void sleep(int seconds) {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException ignore) {
        }
    }
}
